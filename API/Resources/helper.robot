*** Settings ***
# Library     SeleniumLibrary
# Library     RequestsLibrary

# Library     Collections
# Library     OperatingSystem    
 

*** Keywords ***
Nova Aba
    [arguments]     ${url}
    Execute Javascript          window.open()    
    Switch Window               locator=NEW
    Go To                       ${url}

Gera Token
    [Arguments]     ${cpf}
    Abrir conexao   https://api.qa.bancobari.com.br
    ${PARAMS}       Create Dictionary       username=${cpf}  password=123Abc  grant_type=password  client_id=mobile  client_secret=mH8341Kop  content-Type=text/plain
    ${HEADERS}      Create Dictionary       content-type=application/x-www-form-urlencoded       
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      auth/connect/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}    
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}

Gera Token Salesforce
    Abrir conexao   https://api.qa.bancobari.com.br
    ${PARAMS}       Create Dictionary       grant_type=client_credentials  client_id=salesforce  client_secret=s4l1f43E  content-Type=text/plain
    ${HEADERS}      Create Dictionary       content-type=application/x-www-form-urlencoded       
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      auth/connect/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}    
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}

Gera AccountID
    [Arguments]     ${token}
    Abrir conexao   https://api.dev.bancobari.com.br
    ${PARAMS}       Create Dictionary       content-Type=text/plain
    ${HEADERS}      Create Dictionary       accept=application/json  Authorization=${token}     
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Get Request     newsession      cards/v2/accounts
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    log  ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}    
    ${accountID}  Set Variable  ${json_value[0]['accountId']}
    Set Test Variable  ${accountID}
    [Return]        ${accountID}

Gera CrediCardId
    [Arguments]     ${token}  ${accounId}
    Abrir conexao   https://api.dev.bancobari.com.br
    ${PARAMS}       Create Dictionary       content-Type=text/plain
    ${HEADERS}      Create Dictionary       accept=application/json  Authorization=${token}     
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Get Request     newsession      cards/v2/creditCards?accountId=${accountID}
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    log  ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}    
    ${crediCardId}  Set Variable  ${json_value[0]['id']}
    Set Test Variable  ${crediCardId}
    [Return]        ${crediCardId}

New Token DynamoDB
    Abrir Navegador  https://bancobari.awsapps.com/start#/
    Wait Until Element Is Visible         id=mainFrame                120 second
    Input Text                            id=wdc_username             daffyne.dias
    Input Text                            id=wdc_password             Patty55inc@    
    Click Element                         id=wdc_login_button
    ${bodyreturn}  Email Verification                    no-reply@login.awsapps.com        Verification Code:
    Log  ${bodyreturn}
    ${teste}  Fetch From Right        ${bodyreturn}     40px;">
    ${teste1}  Fetch From Left        ${teste}          </div><div
    log  ${teste1}
    Wait Until Element Is Visible         xpath=//*[@id="wdc_mfacode"]         20 second 
    Input Text                            xpath=//*[@id="wdc_mfacode"]         ${teste1}
    Click Element                         xpath=//*[@id="wdc_login_button"]/span/span/button
    Wait Until Element Is Visible         xpath=//*[@id="app-03e8643328913682"]        20 second
    Discard Email  no-reply@login.awsapps.com  Verification Code:
    Click Element                         xpath=//*[@id="app-03e8643328913682"]
    Click Element                         xpath=//*[@id="ins-07f2eb6bec3f867f"]/div/div
    Wait Until Element Is Visible         xpath=//a[@title='DynamoDBRW']/../span[2]/a      20 second
    Click Element                         xpath=//a[@title='DynamoDBRW']/../span[2]/a
    Wait Until Element Is Visible         xpath=//div[@class='dialog-body']
    Wait Until Element Is Visible         xpath=//div[4][@class='ng-tns-c18-10']      60 second
    Wait Until Element Is Visible  xpath=//*[@id="cli-cred-file-code"]/div[2]  60 second
    ${resultextract2}  Get Text            xpath=//*[@id="cli-cred-file-code"]/div[2]
    Wait Until Element Is Visible  xpath=//*[@id="cli-cred-file-code"]/div[3]  60 second
    ${resultextract4}  Get Text            xpath=//*[@id="cli-cred-file-code"]/div[3]
    Wait Until Element Is Visible  xpath=//*[@id="cli-cred-file-code"]/div[4]  60 second
    ${resultextract6}  Get Text            xpath=//*[@id="cli-cred-file-code"]/div[4]
   
    Log  ${resultextract2}
    ${stripped2}=	Strip String  ${resultextract2}
    Log  ${resultextract4}
    ${stripped4}=	Strip String  ${resultextract4}
    Log  ${resultextract6}
    ${stripped6}=	Strip String  ${resultextract6}
    ${defaultText}  Set Variable  [default]
    ${string}  catenate  ${defaultText}${\n}${stripped2}${\n}${stripped4}${\n}${stripped6}${\n}
    Remove File  C:\\Users\\User\\.aws\\credentials
    Append To File  C:\\Users\\User\\.aws\\credentials  ${string}
    Close Browser

Email Verification
    [Arguments]  ${sender}  ${content}
    Open Mailbox    host=outlook.office365.com    user=daffyne.dias@primecontrol.com.br  password=Daffy2107  port=993 
    ${LATEST}    Wait For Email    sender=${sender}    timeout=12000
    ${body}    Get Email Body  ${LATEST}
    Should Contain    ${body}    ${content}  
    Close Mailbox
    [Return]  ${body}

Discard Email
    [Arguments]  ${sender}  ${content}
    Open Mailbox    host=outlook.office365.com    user=daffyne.dias@primecontrol.com.br  password=Daffy2107  port=993 
    ${LATEST}    Wait For Email    sender=${sender}    timeout=12000
    Delete Email  ${LATEST}

#412    #437    #448    #449
consultar numero da conta
    [Arguments]     ${token}     ${cpf}
    #${PARAMS}       Create Dictionary      
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=d25cf88d-468c-45b1-b529-8958f2793207    Authorization=${token}        content-type=application/x-www-form-urlencoded  
    ${RESPOSTAREQUEST}     Get Request     newsession      account/v1/accounts?customerId=${cpf}        headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text} 
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()} 
    ${numConta}  Catenate      ${json_value['accountId']}
    ${ag}  Catenate     ${json_value['agency']}
    [Return]        ${numConta}      ${ag}

# Consultar Numero Da Conta
#     [Arguments]     ${token}     ${cpf}
#     #${PARAMS}       Create Dictionary      
#     ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=d25cf88d-468c-45b1-b529-8958f2793207    Authorization=${token}        content-type=application/x-www-form-urlencoded  
#     ${RESPOSTAREQUEST}     Get Request     newsession      account/v1/accounts?customerId=${cpf}        headers=${HEADERS}
#     Log           ${RESPOSTAREQUEST.text} 
#     ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()} 
#     ${numConta}  Catenate      ${json_value['accountId']}
#     ${ag}  Catenate     ${json_value['agency']}
#     [Return]        ${numConta}      ${ag}

#444    #448    #449
consultar subconta de um usuario
    [Arguments]     ${token}     ${cpf}     ${numeroConta}
    Set Test Variable       @{nomeSubconta}       @{EMPTY}
    Set Test Variable       @{numeroSubconta}       @{EMPTY}
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=280422cf-25b9-4328-8a4c-795c8fc51c3f    Authorization=${token}
    ${RESPOSTAREQUEST}     Get Request     newsession      account/v1/accounts/${numeroConta}/financialgoals      headers=${HEADERS}
    ${resultadoGet}  Set Variable  ${RESPOSTAREQUEST.json()}
    Log           ${resultadoGet}
    :FOR  ${index}  IN  @{resultadoGet}
    \   ${nome}  Catenate      ${index['name']}
    \   ${numero}  Catenate      ${index['id']}
    \   Append To List          ${nomeSubconta}           ${nome}
    \   Append To List          ${numeroSubconta}            ${numero}
    \   Log  ${nomeSubconta}
    \   Log  ${numeroSubconta}

#492    #513    #519    #573    
consultar saldo
    [Arguments]     ${token}     ${cpf}
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=9b74bac8-1481-4db1-ad8d-6f05a9af8e94    Authorization=${token}
    ${RESPOSTAREQUEST}     Get Request     newsession      account/v1/accounts?customerId=${cpf}        headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text} 
    ${retornoSaldo}  Set Variable  ${RESPOSTAREQUEST.json()} 
    ${saldo}  Catenate      ${retornoSaldo['balance']}
    Set Test Variable  ${retornoSaldo}
    [Return]        ${saldo}

Realiza Transferencia
    [Arguments]     ${token}  ${cpf}  ${nome}  ${data}  ${valorTrans}  ${codBanco}  ${agenciaBanco}  ${numConta}  ${tipoConta}  
    ${PARAMS}       catenate        {\"value\":${valorTrans},\"toName\":\"${nome}\",
    ...  \"toDocument\":\"${cpf}\",\"toBank\":${codBanco},\"toAgency\":${agenciaBanco},
    ...  \"toAccount\":${numConta},\"tpAccount\":\"${tipoConta}\",\"description\":\"Pagamento\",
    ...  \"favoriteAccount\":true,\"transactionDate\":\"${data}\"}
	${HEADERS}      Create Dictionary       accept=application/json  User-Agent=0b61cd0b-1672-4613-9ba2-03f55251ff2f    Authorization=${token}
	...		Content-Type=application/json-patch+json  
    ${RESPOSTAREQUEST}     Post Request     newsession      account/v1/transactions		data=${PARAMS}        headers=${HEADERS}
    ${retornoResposta}  Set Variable  ${RESPOSTAREQUEST.json()} 
	Log           ${RESPOSTAREQUEST.text}
    Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200
    [Return]        ${retornoResposta}

Ver Todos Extratos Conta
    [Arguments]     ${token}  ${numConta}
	${HEADERS}      Create Dictionary       accept=application/json  User-Agent=49934d3f-20d3-4e08-97f1-ae06d375e304    Authorization=${token}
    ${RESPOSTAREQUEST}     Get Request     newsession      account/v1/transactions?accountId=${numConta}      headers=${HEADERS}
    ${retornoHist}  Set Variable  ${RESPOSTAREQUEST.json()} 
	Log           ${RESPOSTAREQUEST.text}
    Set Test Variable  ${retornoHist}
    [Return]        ${retornoHist}

Consultar Saldo Subconta
    [Arguments]     ${token}        ${numConta}     ${numSubconta}
	${HEADERS}      Create Dictionary       accept=application/json  User-Agent=49934d3f-20d3-4e08-97f1-ae06d375e304    Authorization=${token}
    ${RESPOSTAREQUEST}     Get Request     newsession      account/v1/accounts/${numConta}/financialgoals/${numSubconta}      headers=${HEADERS}
    ${saldoSubconta}  Set Variable  ${RESPOSTAREQUEST.json()} 
    Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200
	Log           ${RESPOSTAREQUEST.text}
    Set Test Variable  ${saldoSubconta}
    [Return]        ${saldoSubconta}

Consulta Extrato Subconta
    [Arguments]     ${token}        ${numConta}     ${numSubconta}      ${qntdExtrato}
    ${PARAMS}       Create Dictionary       accountId=${numConta}        financialGoalId=${idSubconta}       itemsPerPag=${qntdExtrato}
	${HEADERS}      Create Dictionary       accept=application/json  User-Agent=d30611b4-dca1-446d-b16b-470ddc057dae    Authorization=${token}
    ${RESPOSTAREQUEST}     Get Request     newsession      account/v1/accounts/${numConta}/financialgoals/${idSubconta}/transactions?itemsPerPage=${qntdExtrato}		
	...		data=${PARAMS}        headers=${HEADERS}
    ${retornoExtrato}  Set Variable  ${RESPOSTAREQUEST.json()} 
    Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200
    Log           ${RESPOSTAREQUEST.text}
	Set Test Variable  ${retornoExtrato}
    [Return]        ${retornoExtrato}

Consultar Contatos Transferencia
    [Arguments]     ${token}        ${cpf}
    Set Test Variable       @{nomeContato}       @{EMPTY}
    Set Test Variable       @{idContato}       @{EMPTY}
	${HEADERS}      Create Dictionary       accept=application/json  User-Agent=3605a563-f2ec-443d-b3b3-bbe8a89b4d6a    Authorization=${token}
    ${RESPOSTAREQUEST}     Get Request     newsession      account/v1/accounts/${cpf}/contacts		
	...		headers=${HEADERS}
    ${retornoContato}  Set Variable  ${RESPOSTAREQUEST.json()} 
    Log  ${retornoContato}
    Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200
    :FOR  ${index}  IN  @{retornoContato}
    \   ${id}  Catenate      ${index['id']}
    \   ${nome}  Catenate      ${index['name']}
    \   Append To List          ${nomeContato}           ${nome}
    \   Append To List          ${idContato}            ${id}
    \   Log  ${nomeContato}
    \   Log  ${idContato}

#827
Verificar Dados Do Boleto
    [Arguments]     ${token}      ${numBoleto}
    ${PARAMS}       catenate        {\"digitable\":\"${numBoleto}\"}
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=660d1d1a-e847-47f5-9960-76ba52938bcd    
    ...  Authorization=${token}        
    ...  Content-Type=application/json  
    ${RESPOSTAREQUEST}     Post Request     newsession      /payment/v1/billpayments/authorize       data=${PARAMS}      headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text} 
    Set Test Variable       ${retornoDadosBoleto}        ${RESPOSTAREQUEST.json()}
    Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200
    [Return]        ${retornoDadosBoleto}

#446
Ativacao Transferencia Recorrente
    [Arguments]     ${token}  ${accountId}  ${financialGoalId}  ${ativarTrans}  ${valorTransf}  ${tipoRecor}  ${valorRecor}
    ${PARAMS}       catenate        {\"active\":${ativarTrans},\"value\":${valorTransf},
    ...  \"repetition\":{\"type\":\"${tipoRecor}\",\"value\":\"${valorRecor}\"}}
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=deb527f7-2579-45b4-8940-7112b6b62fbf    Authorization=${token}   content-type=application/json  
    ${RESPOSTAREQUEST}     Put Request     newsession      account/v1/accounts/${accountId}/financialgoals/${financialGoalId}/recurrentdeposit        headers=${HEADERS}       data=${PARAMS}
    Set Test Variable           ${returnActiveRec}      ${RESPOSTAREQUEST.json()}
    Log     ${returnActiveRec}
    Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200

Valida Teste
    [Arguments]     ${logErro}
    Log  ${logErro}
    Run Keyword Unless '${logErro}'=='None'    Set Test Variable  ${aux}  FAIL
