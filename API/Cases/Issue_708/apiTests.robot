*** Settings ***
Documentation
...  As cliente de uma conta e subconta cartão ativa no Banco Bari
...  Want visualizar minhas últimas movimentações
Default Tags	automacao       Sprint35
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.stage.bancobari.com.br

*** Test Cases ***

Extrato subconta cartão
	[Tags]	backend	regression	automation
	Given que_informa_os_dados_para_realizar_a_chamada_de_extrato
	When chama_o_endpoint_de_extrato_para_subconta_cartao
	Then valida_os_dados_do_extrato_para_subconta_conta

*** Keywords ***

que_informa_os_dados_para_realizar_a_chamada_de_extrato
    Set Test Variable       ${cpf}      09478206982
	Set Test Variable		${valorTrans}		100
	Set Test Variable		${qntdExtrato}		90 

	${token}  Gera Token  ${cpf}
	Set Test Variable  ${token}

	${numConta}		${numAgen}		Consultar Numero Da Conta		${token}		${cpf}
    Set Test Variable  ${numConta}
	
    ${retorno}      Consulta Dynamo Three Attributes Two Int And Bool       
    ...  Barigui.Services.Account_FinancialGoals     AccountId		${numConta}      Type       2
    ...  Active		true
    Log  ${retorno}
	${idSubconta}=		Convert To String		${retorno[0]['Id']}
	Set Test Variable  ${idSubconta}

chama_o_endpoint_de_extrato_para_subconta_cartao
    ${retornoExtrato}		Consulta Extrato Subconta		${token}        ${numConta}     ${idSubconta}      ${qntdExtrato}

valida_os_dados_do_extrato_para_subconta_conta
    Should Be True    '${retornoExtrato['itemsPerPage']}'=='90'
	${ultimoExt}  Convert To Integer  ${retornoExtrato['totalItems']}
	Log  ${retornoExtrato}
    Log  ${ultimoExt}
	#Esse é para validar o primeiro extrato
	#Esses passos são apenas para validar se os itens existem no retorno
	Should Be True    '${retornoExtrato['items'][0]['type']}'=='${retornoExtrato['items'][0]['type']}'
	Should Be True    '${retornoExtrato['items'][0]['item']['value']}'=='${retornoExtrato['items'][0]['item']['value']}'
	Should Be True    '${retornoExtrato['items'][0]['item']['category']}'=='${retornoExtrato['items'][0]['item']['category']}'
	Should Be True    '${retornoExtrato['items'][0]['item']['description']}'=='${retornoExtrato['items'][0]['item']['description']}'

	#Esse é para validar o último extrato
	Should Be True    '${retornoExtrato['items'][${ultimoExt}-1]['type']}'=='${retornoExtrato['items'][${ultimoExt}-1]['type']}'
	Should Be True    '${retornoExtrato['items'][${ultimoExt}-1]['item']['value']}'=='${retornoExtrato['items'][${ultimoExt}-1]['item']['value']}'
	Should Be True    '${retornoExtrato['items'][${ultimoExt}-1]['item']['category']}'=='${retornoExtrato['items'][${ultimoExt}-1]['item']['category']}'
	Should Be True    '${retornoExtrato['items'][${ultimoExt}-1]['item']['description']}'=='${retornoExtrato['items'][${ultimoExt}-1]['item']['description']}'
    Should Be True    '${retornoExtrato['items'][${ultimoExt}-1]['item']['isCardTransaction']}'=='${retornoExtrato['items'][${ultimoExt}-1]['item']['isCardTransaction']}'
    Should Be True    '${retornoExtrato['items'][${ultimoExt}-1]['item']['investment']}'=='None'