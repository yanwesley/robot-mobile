*** Settings ***
Documentation
...  As usuário ativo no APP do Banco Bari,
...  Want deseja excluir sua subconta objetivo cartão
Default Tags	automacao       Sprint34
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***

Solicitando a exclusao da subconta objetivo cartao
	[Tags]	backend	regression
	Given endpoint_para_excluir_uma_subconta
	When realizado_o_delete_request_para_excluir_a_subconta_cartao
	Then ira_retornar_sucesso

*** Keywords ***

endpoint_para_excluir_uma_subconta
    Set Test Variable       ${cpf}      09478206982
    ${token}     Gera token       ${cpf}  
    Set Test Variable  ${token}
	${numeroConta}  ${agencia}  Consultar Numero Da Conta  ${token}  ${cpf}
    Set Test Variable  ${numeroConta}
    Set Test Variable  ${agencia}

realizado_o_delete_request_para_excluir_a_subconta_cartao
    ${retorno}      Consulta Dynamo Three Attributes Two Int And Bool       
    ...  Barigui.Services.Account_FinancialGoals     AccountId		${numeroConta}      Type       2
    ...  Active		true
    Log  ${retorno}
    ${numeroSubconta}       Set Variable      ${retorno[0]['Id']}
    Set Test Variable        ${numeroSubconta}
    #MUDAR
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=a5b12449-5783-4134-96b9-1db3a0fdf9b5    Authorization=${token}        content-type=application/x-www-form-urlencoded  
    ${RESPOSTAREQUEST}     Delete Request     newsession      account/v1/accounts/${numeroConta}/financialgoals/${numeroSubconta}      headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text} 
    Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		202

ira_retornar_sucesso
    ${retorno}      Consulta Dynamo Int     Barigui.Services.Account_FinancialGoals     Id       ${numeroSubconta}
    ${statusAtivo}  Set Variable  ${retorno[0]['Active']}
    Should Be True    'False' == '${statusAtivo}'
