*** Settings ***
Documentation
...    Como cliente do banco Bari
Default Tags	automacao       Sprint29
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***

Excluir uma subconta objetivo meta
    [Tags]	backend	regression
    Given endpoint_para_excluir_uma_subconta_objetivo_meta
	When realizado_o_delete_request_com_o_id_da_subconta_objetivo_meta
	Then sera_marcado_como_inativo_no_banco_referente_a_subconta_objetivo_meta_escolhida

*** Keywords ***

endpoint_para_excluir_uma_subconta_objetivo_meta
    Set Test Variable       ${cpf}      09478206982
    ${token}     Gera token       ${cpf}  
    Set Test Variable  ${token}
	${numeroConta}  ${agencia}  consultar numero da conta  ${token}  ${cpf}
    Set Test Variable  ${numeroConta}
    Set Test Variable  ${agencia}

realizado_o_delete_request_com_o_id_da_subconta_objetivo_meta
    consultar subconta de um usuario        ${token}     ${cpf}     ${numeroConta}
    ${auxNum}       Set Variable      ${numeroSubconta}
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=a5b12449-5783-4134-96b9-1db3a0fdf9b5    Authorization=${token}        content-type=application/x-www-form-urlencoded  
    ${RESPOSTAREQUEST}     Delete Request     newsession      account/v1/accounts/${numeroConta}/financialgoals/${auxNum[0]}      headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text} 
    Log           ${auxNum[0]}
    Set Test Variable  ${subcontaEscolhida}     ${auxNum[0]} 

sera_marcado_como_inativo_no_banco_referente_a_subconta_objetivo_meta_escolhida
    ${retorno}      Consulta Dynamo Int    Barigui.Services.Account_FinancialGoals     Id       ${subcontaEscolhida}
    ${statusAtivo}  Set Variable  ${retorno[0]['Active']}
    Should Be True    'False' == '${statusAtivo}'