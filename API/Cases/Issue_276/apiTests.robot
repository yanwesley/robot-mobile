*** Settings ***
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao   

*** Test Case ***
Confirmar se o usuario consta na Black List
	[Tags]	backend	regression	automation
	Given que_ocorreu_a_consulta_na_black_list
	And o_usuario_atingiu_o_limite_de_tentativas_de_login_com_o_cpf
	When a_consulta_retornar_o_numero_de_tentativas
	Then sera_recebido_a_confirmacao_do_usuario_na_black_list

Confirmar se o usuario nao consta na Black List
	[Tags]	backend	regression	automation
	Given que_ocorreu_a_consulta_na_black_list
	And o_usuario_nao_atingiu_o_limite_de_tentativas_de_login_com_o_cpf
	When a_consulta_nao_retornar_o_numero_de_tentativas
	Then nao_sera_recebido_a_confirmacao_do_usuario_na_black_list

Confirmar se o usuario consta na Black List mas passou 30 dias
	[Tags]	backend	regression	automation
	Given que_ocorreu_a_consulta_na_black_list
	And o_usuario_esta_a_mais_de_30_dias_na_black_list
	When receber_o_evento_de_tentativa_de_login
	Then ira_apagar_as_tentativas_anteriores
	And sera_registrado_essa_tentativa_no_dynamo_db

Confirmar se a tentativa e cadastrada na Black List
	[Tags]	backend	regression	automation
	Given que_o_usuario_possui_conta_cadastrada_no_aplicativo
	When receber_o_evento_de_tentativa_de_login
	Then sera_registrado_essa_tentativa_no_dynamo_db

*** Keywords ***

#######

# Confirmar se o usuário consta na Black List
# 	[Tags]	backend	regression	automation
# 	${limiteTentativas}		Set Variable		5
# 	@{cpf}		Create List		02535200961		07980962923		06166556900		07783740928		68097221063
# 	#Given o_usuario_possui_conta_no_bari
# 	When o_usuario_atingiu_o_limite_de_tentativas_de_login_com_o_cpf		@{cpf}
# 	Then sera_recebido_a_confirmacao_do_usuario_na_black_list		@{cpf}

que_eu_defino_o_endpoint_para_consulta_de_cpf
	[Arguments]	${endpoint}
    Set Test Variable  ${endpoint}

eu_defino_o_request_header
    Create Dictionary    content-type=application/json
    
faco_uma_chamada_get
    [Arguments]     ${cpf}
    ${RESPOSTAREQUEST}     Get Request     customer     /v1/customers/${cpf}/checkUserInvited
    Log                           ${RESPOSTAREQUEST.text}
    Set Test Variable             ${RESPOSTAREQUEST}

o_usuario_atingiu_o_limite_de_tentativas_de_login_com_o_cpf
	[Arguments]     @{cpf}

    ${validarDesc}       Set Variable        Bearer The username and password do not match

    :FOR  ${index}  IN  @{cpf}
	\	Gera Token  ${index}
    \   ${PARAMS}       Create Dictionary       username=${index}  password=123Abd  grant_type=password  client_id=mobile  client_secret=mH8341Kop  content-Type=text/plain
    \   ${HEADERS}      Create Dictionary       content-type=application/x-www-form-urlencoded  
    \   ${RESPOSTAREQUEST}     Post Request     newsession      auth/connect/token      data=${PARAMS}      headers=${HEADERS}
    \   ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}    
    \   ${descricao}  Catenate  Bearer  ${json_value['error_description']}
    \   Should Be True    '${descricao}' == '${validarDesc}'

recebo_a_resposta_referente_ao_cpf
    [Arguments]     ${resposta}
    Log             ${resposta}
    Dictionary Should Contain Item    ${RESPOSTAREQUEST.json()}    status             ${resposta}

sera_recebido_a_confirmacao_do_usuario_na_black_list
    [Arguments]     @{cpf}
    #Consulta Dynamo     Barigui.Services.IdentityServer_LoginTries     

sera_marcado_como_inativo_o_registro_no_banco_referente_a_subconta_objetivo_meta_escolhida
    [Arguments]     ${numeroSubconta}
    ${numeroSubconta}=      Convert To Number  ${numeroSubconta}  
    ${retorno}      Consulta Dynamo     Barigui.Services.Account_FinancialGoals        CustomerId      ${numeroSubconta}
    ${retorno}  Set Variable  ${retorno[0]}
    # Contas inativas sempre terão número ...
    Should Be True    0 == ${retorno['AgencyId']}





