*** Settings ***
Documentation
...    Como cliente do banco Bari
Default Tags	automacao		Sprint29
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***

Usuario copia os dados da conta usando o icone no card
	[Tags]	backend	regression
	Given o_usuario_tem_uma_conta_ativa_no_banco_bari
	When consultar_no_endpoint_o_numero_da_conta
	Then verificar_se_os_dados_estao_corretos

*** Keywords ***

o_usuario_tem_uma_conta_ativa_no_banco_bari
    ${token}    Gera Token  09478206982
    Set Test Variable  ${token}

consultar_no_endpoint_o_numero_da_conta
	Set Test Variable       ${cpf}      09478206982
	${numeroConta}  ${agencia}  consultar numero da conta  ${token}  ${cpf}
	Set Test Variable  ${numeroConta}
    Set Test Variable  ${agencia}

verificar_se_os_dados_estao_corretos
    ${retorno}      Consulta Dynamo     Barigui.Services.Account_CheckingAccount        CustomerId      ${cpf}
    ${retorno}  Set Variable  ${retorno[0]}
    Should Be True    ${numeroConta} == ${retorno['Id']}
    # POR ENQUANTO TODAS AS AGENCIAS SERÃO NÚM 0 CASO ISSO ALTERE SÓ USAR A VAR ${agencia}
    Should Be True    0 == ${retorno['AgencyId']}