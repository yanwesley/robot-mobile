*** Settings ***
Documentation
...    As usuário do salesforce
...    Want visualizo os históricos de transferência dos clientes
Default Tags	automacao       Sprint30
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***

O Salesforce acessando o historico de transferencia
	[Tags]	backend	regression
	Given que_o_salesforce_consulta_o_historico_de_transferencia
	When ocorrer_a_chamada_get_de_historico_de_transferencia
	Then retorna_os_dados_requisitados_pelo_salesforce

*** Keywords ***

que_o_salesforce_consulta_o_historico_de_transferencia
	Set Test Variable       ${cpf}      09478206982
	${token}  Gera Token Salesforce
	Set Test Variable  ${token}

ocorrer_a_chamada_get_de_historico_de_transferencia
	${numConta}  ${ag}  consultar numero da conta  ${token}  ${cpf}
	${retornoHist}  Ver Todos Extratos Conta  ${token}  ${numConta}

retorna_os_dados_requisitados_pelo_salesforce
	Should Be True		'${retornoHist['items'][0]['dateTime']}'!=''
	Should Be True		'${retornoHist['items'][0]['item']['category']}'!=''
	Should Be True		'${retornoHist['items'][0]['item']['accountId']}'!=''
	Should Be True		'${retornoHist['items'][0]['item']['description']}'!=''
	Should Be True		'${retornoHist['items'][0]['item']['value']}'!=''
