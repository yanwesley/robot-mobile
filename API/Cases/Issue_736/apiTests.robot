*** Settings ***
Documentation
...  As 
...  Want 
Default Tags	automacao       Sprint33
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		http://172.16.128.153/SicredAPI/api

*** Test Cases ***

Enviando dados Sicredi
	[Tags]	backend	regression
	Given endpoint_para_enviar_os_dados_para_Sicredi
	When realizado_o_post_request_para_enviar
	Then sera_enviado_os_dados_para_Sicredi

*** Keywords ***

endpoint_para_enviar_os_dados_para_Sicredi
    Set Test Variable       ${cpf}      07783740928
    Set Test Variable       ${formulario}      ClientesPF
    Set Test Variable       ${campo}      Cliente
      

realizado_o_post_request_para_enviar
    ${PARAMS}=      catenate       [ { \"campo\": \"Cliente\", \"valor\": \"07783740928\" }]

    #${PARAMS}       Create Dictionary       ${enviar}
	${HEADERS}      Create Dictionary       accept=text/plain  User-Agent=901f78d0-062a-4ec9-b232-63629b8baad9      Content-Type=application/json-patch+json
    ${RESPOSTAREQUEST}     Post Request     newsession      /cadastro-dinamico/layout-update/${formulario}		
	...		data=${PARAMS}        headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text}
    Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200
    Set Test Variable       ${retorno}      ${RESPOSTAREQUEST.json()}

sera_enviado_os_dados_para_Sicredi
    ${enderecoEmail}  Catenate      ${retorno['subForm'][0]['body'][70]['valor']}
    ${email}  Catenate      ${retorno['subForm'][0]['body'][70]['referencia']}
    Should Be True    'EMAIL' == '${email}'
