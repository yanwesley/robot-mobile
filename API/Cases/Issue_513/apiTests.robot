*** Settings ***
Documentation
...    As usuário com conta ativa no banco Bari
...    Want solicito uma transferência para contas Bari
Default Tags	automacao       Sprint30
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***
#Tem que refatorar
# Listar contas de bancos terceiros do usuario
# 	[Tags]	backend	regression
# 	Given digita_o_cpf_do_mesmo_usuario
# 	When chama_o_endpoint_de_listar_bancos
# 	Then valida_se_na_tabela_do_banco_possui_as_contas_bancarias

Usuario transfere para outro usuario
	[Tags]	backend	regression
	Given digita_o_cpf_de_um_cliente_bari
	When chama_o_endpoint_de_transferencia
	Then valida_no_endpoint_de_saldo_se_foi_transferido

Usuario transfere para ele em outro banco
	[Tags]	backend	regression
	Given digita_o_seu_cpf
	When chama_o_endpoint_para_transferencia_em_outro_banco
	Then valida_no_saldo_se_foi_transferido_corretamente

*** Keywords ***

digita_o_cpf_do_mesmo_usuario
	Set Test Variable       ${cpf}      09478206982
	Set Test Variable       ${nome}      Daffyne Alves Dias
	${token}  Gera Token  ${cpf}
	Set Test Variable  ${token}
	${contasRelac}      Consulta Dynamo     Barigui.Services.Account_RelatedAccounts     CustomerId       ${cpf}
	Set Test Variable  ${contasRelac}

chama_o_endpoint_de_listar_bancos
	Set Test Variable       @{numAgencia}       @{EMPTY}
    Set Test Variable       @{numConta}       @{EMPTY}
	Set Test Variable       @{codBanco}       @{EMPTY}
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=cbdb57ba-7110-49e3-bd9b-63721ffeed66    Authorization=${token}  
    ${RESPOSTAREQUEST}     Get Request     newsession      account/v1/accounts/09478206982/relatedaccounts       headers=${HEADERS}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()} 
	Log           ${RESPOSTAREQUEST.text}
	${length} =     Get Length      ${json_value}
	:FOR  ${index}  IN RANGE  ${length}	
	\	${auxAgencia}  Catenate		${json_value[${index}]['agencyNumber']}
    \	${auxConta}  Catenate      ${json_value[${index}]['accountNumber']}	
	\	${auxBanco}  Catenate      ${json_value[${index}]['bank']['code']}	
    \   Append To List          ${numAgencia}           ${auxAgencia}
    \   Append To List          ${numConta}            ${auxConta}
	\   Append To List          ${codBanco}           ${auxBanco}
    \   Log  ${numAgencia}
    \   Log  ${numConta}
	\   Log  ${codBanco}

#TESTE ANTIGO
valida_se_na_tabela_do_banco_possui_as_contas_bancarias
	Set Test Variable       ${aux}		0
	${auxAgencia}       Set Variable      ${numAgencia}
    ${auxConta}       Set Variable      ${numConta}
	${auxBanco}       Set Variable      ${codBanco}
	${retorno}      Consulta Dynamo     Barigui.Services.Account_RelatedAccounts     CustomerId       ${cpf}
    ${tamRetorno} =     Get Length      ${retorno}
	Log  ${retorno}
	:FOR        ${index}      IN RANGE        ${tamRetorno}
	\	Log  ${index}
	\	Continue For Loop If	'${retorno[${index}]['RelatedAccounts'][0]['Name']}' != '${nome}'
	\	${retornoAgencia}  Set Variable  ${retorno[${index}]['RelatedAccounts'][0]['AgencyNumber']}
	\	${retornoConta}  Set Variable  ${retorno[${index}]['RelatedAccounts'][0]['AccountNumber']}
	\	${retornoBanco}  Set Variable  ${retorno[${index}]['RelatedAccounts'][0]['CompeCode']}
	\	Should Be True    '${auxAgencia[${aux}]}' == '${retornoAgencia}'
	\	Should Be True    '${auxConta[${aux}]}' == '${retornoConta}'
	\	Should Be True    '${auxBanco[${aux}]}' == '${retornoBanco}'
	\	${aux}=  Set Variable  ${aux} +1

#SEGUNDO TESTE

digita_o_cpf_de_um_cliente_bari
	Set Test Variable       ${cpf}      09478206982
	${token}  Gera Token  ${cpf}
	Set Test Variable  ${token}

chama_o_endpoint_de_transferencia
	Set Test Variable       ${nome}      Luis Eduardo Gritz
	Set Test Variable       ${cpfTransf}      07337619928
	Set Test Variable       ${data}      2020-07-14T16:40:24.504Z
	Set Test Variable       ${valorTrans}      100
	Set Test Variable       ${codBanco}		330
	Set Test Variable       ${agenciaBanco}		1
	Set Test Variable       ${numConta}		21001
	Set Test Variable       ${tipoConta}		CC
	${saldoAntes}		consultar saldo		${token}     ${cpf}
	Set Test Variable  ${saldoAntes}
	${transferencia}		realiza transferencia  ${token}  ${cpfTransf}  ${nome}  ${data}  ${valorTrans}
	...  ${codBanco}  ${agenciaBanco}  ${numConta}  ${tipoConta}  

valida_no_endpoint_de_saldo_se_foi_transferido
	${saldoDepois}		consultar saldo		${token}     ${cpf}
	Set Test Variable  ${saldoDepois}
	Should Be True    ${saldoDepois} == ${saldoAntes}-${valorTrans}

#TERCEIRO TESTE

digita_o_seu_cpf
	Set Test Variable       ${cpf}      09478206982
	${token}  Gera Token  ${cpf}
	Set Test Variable  ${token}

chama_o_endpoint_para_transferencia_em_outro_banco
	Set Test Variable       ${nome}      Daffyne Alves Dias
	Set Test Variable       ${cpfTransf}      09478206982
	Set Test Variable       ${data}      2020-07-14T16:40:24.504Z
	Set Test Variable       ${valorTrans}      100
	Set Test Variable       ${codBanco}		237
	Set Test Variable       ${agenciaBanco}		1234
	Set Test Variable       ${numConta}		1472583
	Set Test Variable       ${tipoConta}		CC
	${saldoAntes}		consultar saldo		${token}     ${cpf}
	Set Test Variable  ${saldoAntes}
	${transferencia}		realiza transferencia  ${token}  ${cpfTransf}  ${nome}  ${data}  ${valorTrans}
	...  ${codBanco}  ${agenciaBanco}  ${numConta}  ${tipoConta}  

valida_no_saldo_se_foi_transferido_corretamente
	${saldoDepois}		consultar saldo		${token}     ${cpf}
	Set Test Variable  ${saldoDepois}
	Should Be True    ${saldoDepois} == ${saldoAntes}-${valorTrans}