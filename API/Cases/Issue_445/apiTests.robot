*** Settings ***
Documentation
...  As cliente ativo do Banco Bari com uma subconta cartão
...  Want receber SMS no número cadastrado sempre que houver movimentações com o cartão
Default Tags	automacao       Sprint36
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***

Solicitar comprovante SMS da subconta cartao
	[Tags]	backend	regression
	Given endpoint_para_solicitar_comprovante_sms_da_subconta_objetivo_cartao
	When realizado_o_put_request_com_as_informacoes_do_comprovante_da_subconta_objetivo_cartao
	Then sera_alterado_o_registro_no_banco_referente_a_subconta_objetivo_cartao_escolhida

Tira solicitacao de SMS da subconta cartao
	[Tags]	backend	regression
	Given endpoint_para_tirar_solicitacao_de_sms_da_subconta_objetivo_cartao
	When realizado_o_put_request_com_as_informacoes_para_tirar_solicitacao_de_sms
	Then sera_colocado_como_falso_no_registro_do_banco_referente_a_subconta

*** Keywords ***

endpoint_para_solicitar_comprovante_sms_da_subconta_objetivo_cartao
    Set Test Variable       ${cpf}      09478206982
    Set Test Variable       ${numTel}      5541999210787

	${token}  Gera Token  ${cpf}
	Set Test Variable  ${token}

	${numConta}		${numAgen}		Consultar Numero Da Conta		${token}		${cpf}
    Set Test Variable  ${numConta}
	
    ${retorno}      consulta_dynamo_three_attributes_two_int_and_bool    
	...  Barigui.Services.Account_FinancialGoals		AccountId		${numConta}
	...  Type       2
    ...  Active		true
	Log  ${retorno}
	${idSubconta}=		Convert To String		${retorno[0]['Id']}
	Set Test Variable  ${idSubconta}

realizado_o_put_request_com_as_informacoes_do_comprovante_da_subconta_objetivo_cartao
    ${PARAMS}       catenate       { \"Active\":true, \"PhoneNumber\":\"${numTel}\"}
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=bb9ae94e-021b-4a6a-ad5a-829df3c9730e    
    ...  Authorization=${token}        
    ...  Content-Type=application/json-patch+json  
    ${RESPOSTAREQUEST}     Put Request     newsession      account/v1/accounts/${numConta}/financialgoals/${idSubconta}/smsconfig       data=${PARAMS}      headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text} 
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200

sera_alterado_o_registro_no_banco_referente_a_subconta_objetivo_cartao_escolhida
    ${retorno}      Consulta Dynamo Int     Barigui.Services.Account_FinancialGoals     Id       ${idSubconta}
    ${retornoActive}  Set Variable  ${retorno[0]['SmsConfig']['Active']}
    ${retornoIdSubconta}  Set Variable  ${retorno[0]['SmsConfig']['FinancialGoalId']}
    ${retornoNumTel}  Set Variable  ${retorno[0]['SmsConfig']['PhoneNumber']}
    Should Be True    'True' == '${retornoActive}'
    Should Be True    '${idSubconta}' == '${retornoIdSubconta}'
    Should Be True    '${numTel}' == '${retornoNumTel}'

#OUTRO TESTE

endpoint_para_tirar_solicitacao_de_sms_da_subconta_objetivo_cartao
    Set Test Variable       ${cpf}      09478206982
    Set Test Variable       ${numTel}      5541999210787

	${token}  Gera Token  ${cpf}
	Set Test Variable  ${token}

	${numConta}		${numAgen}		Consultar Numero Da Conta		${token}		${cpf}
    Set Test Variable  ${numConta}
	
    ${retorno}      consulta_dynamo_three_attributes_two_int_and_bool    
	...  Barigui.Services.Account_FinancialGoals		AccountId		${numConta}
	...  Type       2
    ...  Active		true
	Log  ${retorno}
	${idSubconta}=		Convert To String		${retorno[0]['Id']}
	Set Test Variable  ${idSubconta}

realizado_o_put_request_com_as_informacoes_para_tirar_solicitacao_de_sms
    ${PARAMS}       catenate       { \"Active\":false, \"PhoneNumber\":\"${numTel}\"}
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=bb9ae94e-021b-4a6a-ad5a-829df3c9730e    
    ...  Authorization=${token}        
    ...  Content-Type=application/json-patch+json  
    ${RESPOSTAREQUEST}     Put Request     newsession      account/v1/accounts/${numConta}/financialgoals/${idSubconta}/smsconfig       data=${PARAMS}      headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text} 
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200

sera_colocado_como_falso_no_registro_do_banco_referente_a_subconta
    ${retorno}      Consulta Dynamo Int     Barigui.Services.Account_FinancialGoals     Id       ${idSubconta}
    ${retornoActive}  Set Variable  ${retorno[0]['SmsConfig']['Active']}
    ${retornoIdSubconta}  Set Variable  ${retorno[0]['SmsConfig']['FinancialGoalId']}
    ${retornoNumTel}  Set Variable  ${retorno[0]['SmsConfig']['PhoneNumber']}
    Should Be True    'False' == '${retornoActive}'
    Should Be True    '${idSubconta}' == '${retornoIdSubconta}'
    Should Be True    '${numTel}' == '${retornoNumTel}'
