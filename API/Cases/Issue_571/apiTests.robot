*** Settings ***
Documentation
...  As cliente de uma conta e subconta meta ativa no Banco Bari
...  Want visualizar os rendimentos dos meus resgate
Default Tags	automacao       Sprint34
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.stage.bancobari.com.br

*** Test Cases ***

Visualizando o rendimento detalhado por resgate
	[Tags]	backend	regression
	Given endpoint_para_realizar_uma_transferencia_na_subconta
	When realizado_o_put_request_para_retirar_o_dinheiro
	Then ira_trazer_o_rendimento_detalhado_daquele_resgate

*** Keywords ***

endpoint_para_realizar_uma_transferencia_na_subconta
    Set Test Variable       ${cpf}      83886710904
	${token}  Gera Token  ${cpf}
	Set Test Variable  ${token}
	${numConta}  ${agencia}  Consultar Numero Da Conta  ${token}  ${cpf}
	Set Test Variable  ${numConta}
    Set Test Variable  ${agencia}
	# ${retorno}      Consulta Dynamo Two Attributes Int And Bool    
	# ...  Barigui.Services.Account_FinancialGoals		AccountId		${numConta}
	# ...  Active		true
	# Log  ${retorno}
	${retorno}      Consulta Dynamo Three Attributes Two Int And Bool       
    ...  Barigui.Services.Account_FinancialGoals     AccountId		${numConta}      Type       1
    ...  Active		true
    Log  ${retorno}
	${idSubconta}=		Convert To String		${retorno[1]['Id']}
	Set Test Variable  ${idSubconta}

realizado_o_put_request_para_retirar_o_dinheiro
	${HEADERS}      Create Dictionary       accept=application/json  User-Agent=cd8fc073-7ffc-4387-9c17-6b94146ddd5f    Authorization=${token}
    ${RESPOSTAREQUEST}     Get Request     newsession      account/v1/accounts/${numConta}/financialgoals/${idSubconta}/transactions?tpTransaction=2&page=0&itemsPerPage=10        headers=${HEADERS}
    Set Test Variable		${retornoResposta}		${RESPOSTAREQUEST.json()}
	Log           ${RESPOSTAREQUEST.text}
	Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200

ira_trazer_o_rendimento_detalhado_daquele_resgate
    #Validar apenas se existe esses campos
    Log    ${retornoResposta['items'][0]['item']['investment']} 
    Should Be True    ${retornoResposta['items'][0]['item']['investment']['value']} == ${retornoResposta['items'][0]['item']['investment']['value']}
    Should Be True    ${retornoResposta['items'][0]['item']['investment']['income']} == ${retornoResposta['items'][0]['item']['investment']['income']}
    Should Be True    ${retornoResposta['items'][0]['item']['investment']['incomeTax']} == ${retornoResposta['items'][0]['item']['investment']['incomeTax']}
    Should Be True    ${retornoResposta['items'][0]['item']['investment']['iof']} == ${retornoResposta['items'][0]['item']['investment']['iof']}