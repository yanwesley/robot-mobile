*** Settings ***
Library             AppiumLibrary  timeout=120

*** Test Cases ***

Transferencia
    ${orig timeout} =    Set Appium Timeout    180 seconds
    Open Application  remote_url=http://localhost:4723/wd/hub  udid=8e256a11  platformName=Android  deviceName=RedmiDaffy  appPackage=br.com.bancobari.qa  appActivity=br.com.bancobari.OnboardingActivity
    Wait Until Element Is Visible  br.com.bancobari.qa:id/accessAccount  300
    Element Should Be Visible  br.com.bancobari.qa:id/accessAccount  ${orig timeout}
    Click Element       br.com.bancobari.qa:id/accessAccount
    Wait Until Element Is Visible  br.com.bancobari.qa:id/cpfEditText  300
    Input Text          br.com.bancobari.qa:id/cpfEditText  09478206982
    Click Element       br.com.bancobari.qa:id/continueButton
    Wait Until Element Is Visible  br.com.bancobari.qa:id/passwordEditText  300
    Input Text          br.com.bancobari.qa:id/passwordEditText  123Abc
    Click Element       br.com.bancobari.qa:id/continueButton
    Wait Until Element Is Visible  br.com.bancobari.qa:id/cancelButton  300
    Click Element       br.com.bancobari.qa:id/cancelButton