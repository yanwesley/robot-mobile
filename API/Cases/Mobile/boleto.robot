*** Settings ***
Library             AppiumLibrary  timeout=300
Library             Collections
Resource            ${EXECDIR}/Resources/helper.robot

*** Test Cases ***
Boleto
    Open Application  remote_url=http://localhost:4723/wd/hub  udid=8e256a11  platformName=Android  deviceName=RedmiDaffy  appPackage=br.com.bancobari.qa  appActivity=br.com.bancobari.OnboardingActivity
    
    #Start Screen Recording
    Wait Until Element Is Visible  br.com.bancobari.qa:id/accessAccount  300
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/accessAccount
    Wait Until Element Is Visible  br.com.bancobari.qa:id/cpfEditText  300
    Input Text          br.com.bancobari.qa:id/cpfEditText  09478206982
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/continueButton
    Wait Until Element Is Visible  br.com.bancobari.qa:id/passwordEditText  300
    Input Text          br.com.bancobari.qa:id/passwordEditText  123Abc
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/continueButton
    Wait Until Element Is Visible  br.com.bancobari.qa:id/cancelButton  300
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/cancelButton
    Wait Until Element Is Visible  br.com.bancobari.qa:id/showExtractButton  300
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/drawerToggleButton
    Wait Until Element Is Visible  br.com.bancobari.qa:id/navItem2  300
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/navItem3
    Wait Until Element Is Visible  br.com.bancobari.qa:id/toolbarTitleTextView  300
    Capture Page Screenshot
    Set Test Variable       @{progress}       @{EMPTY}
    ${msg}  Run Keyword And Ignore Error  Element Text Should Be  br.com.bancobari.qa:id/toolbarTitleTextView  Pagamento
    Append To List          ${progress}           ${msg[1]}
    ${msg}  Run Keyword And Ignore Error  Element Text Should Be  br.com.bancobari.qa:id/barCodeEditText  Código de barras
    Append To List          ${progress}           ${msg[1]}
    ${msg}  Run Keyword And Ignore Error  Element Text Should Be  br.com.bancobari.qa:id/descriptionEditText  Descrição
    Append To List          ${progress}           ${msg[1]}
    Input Text          br.com.bancobari.qa:id/barCodeEditText  42297030060000269528643818598625883460000000759
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/descriptionEditText
    Wait Until Element Is Visible  br.com.bancobari.qa:id/descriptionEditText  300
    Input Text          br.com.bancobari.qa:id/descriptionEditText  Boleto Teste
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/continueButton
    Wait Until Element Is Visible  	br.com.bancobari.qa:id/iconImageView  300
    ${msg}  Run Keyword And Ignore Error  Element Text Should Be  br.com.bancobari.qa:id/titleTextView  Pagamento não autorizado
    Append To List          ${progress}           ${msg[1]}
    ${msg}  Run Keyword And Ignore Error  Element Text Should Be  br.com.bancobari.qa:id/descriptionTextView  Parece que este boleto já foi pago!
    Append To List          ${progress}           ${msg[1]}
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/icCloseImageView

    Capture Page Screenshot
    #VALIDA SE TEVE ERROS ACIMA
    Set Test Variable       ${aux}      ${EMPTY}
    :FOR  ${erro}  IN  @{progress}
    ${vai}  Evaluate  "None"=="""${erro}"""
    Run Keyword Unless   ${vai}  
    ...  ${aux}  FAIL
    END    
    Should Be Equal As Strings  ${aux}  ${EMPTY}
    