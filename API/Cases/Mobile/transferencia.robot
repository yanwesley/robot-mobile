*** Settings ***
Library             AppiumLibrary  timeout=300

*** Test Cases ***
Transferencia
    Open Application  remote_url=http://localhost:4723/wd/hub  udid=8e256a11  platformName=Android  deviceName=RedmiDaffy  appPackage=br.com.bancobari.qa  appActivity=br.com.bancobari.OnboardingActivity

    # Start Screen Recording
    Wait Until Element Is Visible  br.com.bancobari.qa:id/accessAccount  300
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/accessAccount
    Wait Until Element Is Visible  br.com.bancobari.qa:id/cpfEditText  300
    Input Text          br.com.bancobari.qa:id/cpfEditText  09478206982
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/continueButton
    Wait Until Element Is Visible  br.com.bancobari.qa:id/passwordEditText  300
    Input Text          br.com.bancobari.qa:id/passwordEditText  123Abc
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/continueButton
    Wait Until Element Is Visible  br.com.bancobari.qa:id/cancelButton  300
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/cancelButton
    Wait Until Element Is Visible  br.com.bancobari.qa:id/showExtractButton  300
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/showExtractButton
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/showExtractButton
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/showExtractButton
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/drawerToggleButton
    Wait Until Element Is Visible  br.com.bancobari.qa:id/navItem2  300
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/navItem2
    Wait Until Element Is Visible  br.com.bancobari.qa:id/toolbarTitleTextView  300
    Capture Page Screenshot
    Element Text Should Be  br.com.bancobari.qa:id/toolbarTitleTextView  Transferência
    Input Text          br.com.bancobari.qa:id/contactSearchEditText  Luis Eduardo Gritz
    Element Text Should Be  br.com.bancobari.qa:id/nameTextView  Luis Eduardo Gritz
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/nameTextView
    Wait Until Element Is Visible  br.com.bancobari.qa:id/bankNameTextView  300
    Element Text Should Be  br.com.bancobari.qa:id/bankNameTextView  Bco Barigui Inv Fin S/A
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/bankNameTextView
    Wait Until Element Is Visible  br.com.bancobari.qa:id/textView2  300
    Element Text Should Be  br.com.bancobari.qa:id/textView2  Qual valor você quer transferir para esta conta?
    Capture Page Screenshot
    Clear Text          br.com.bancobari.qa:id/amountToTransferET
    Input Text          br.com.bancobari.qa:id/amountToTransferET  100
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/continueButton
    Wait Until Element Is Visible  br.com.bancobari.qa:id/textView2  300
    Element Text Should Be  br.com.bancobari.qa:id/textView2  Você pode adicionar uma descrição a esta transferência, caso queira.
    Capture Page Screenshot
    Input Text          br.com.bancobari.qa:id/transferenceObsET  Automação Bari
    Click Element       br.com.bancobari.qa:id/continueButton
    Element Should Be Visible  //android.widget.TextView[@text='Banco']
    Element Should Be Visible  //android.widget.TextView[@text='Agência']
    Element Should Be Visible  //android.widget.TextView[@text='Conta']
    Element Should Be Visible  //android.widget.TextView[@text='Favorecido']
    Element Should Be Visible  //android.widget.TextView[@text='CPF']
    Element Should Be Visible  //android.widget.TextView[@text='Tipo de Conta']
    Element Should Be Visible  //android.widget.TextView[@text='Hoje']
    Element Should Be Visible  //android.widget.TextView[@text='Descrição']
    Element Text Should Be  br.com.bancobari.qa:id/tvBankName  330 Bco Barigui Inv Fin S/A
    Element Text Should Be  br.com.bancobari.qa:id/tvAgency  0001
    Element Text Should Be  br.com.bancobari.qa:id/tvAccount  	2100-1
    Element Text Should Be  br.com.bancobari.qa:id/tvDestName  Luis Eduardo Gritz	
    Element Text Should Be  br.com.bancobari.qa:id/tvDestCpf  	073.376.199-28
    Element Text Should Be  br.com.bancobari.qa:id/typeAccountDescription  	Conta Corrente
    Element Text Should Be  br.com.bancobari.qa:id/tvObs  Automação Bari
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/continueButton
    Wait Until Element Is Visible  //android.widget.TextView[@text='Senha de transação']  300
    Input Text          br.com.bancobari.qa:id/passwordEditText  1470
    Capture Page Screenshot
    Click Element       br.com.bancobari.qa:id/confirmButton
    Wait Until Element Is Visible  //android.widget.TextView[@text='Transferência realizada com sucesso!']  300
    Capture Page Screenshot
    Element Text Should Be  br.com.bancobari.qa:id/tvAmount  	R$ 1,00
    Element Text Should Be  br.com.bancobari.qa:id/tvDestName  Luis Eduardo Gritz	
    Element Text Should Be  br.com.bancobari.qa:id/tvBankName  330 Bco Barigui Inv Fin S/A
    Element Text Should Be  br.com.bancobari.qa:id/tvAgency  0001
    Element Text Should Be  br.com.bancobari.qa:id/tvAccount  	2100-1
    Element Text Should Be  br.com.bancobari.qa:id/tvObs  Automação Bari
    Element Should Be Visible  br.com.bancobari.qa:id/tvAuthCode
    # Stop Screen Recording