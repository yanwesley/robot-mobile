*** Settings ***
Documentation
...    Como cliente do banco Bari
Default Tags	automacao       Sprint29
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***
Alterar uma subconta objetivo meta
	[Tags]	backend	regression
	Given endpoint_para_alterar_uma_subconta_objetivo_meta
	When realizado_o_put_request_com_as_informacoes_da_subconta_objetivo_meta
	Then sera_alterado_o_registro_no_banco_referente_a_subconta_objetivo_meta_escolhida

*** Keywords ***

endpoint_para_alterar_uma_subconta_objetivo_meta
    Set Test Variable       ${cpf}      09478206982
    ${valorMeta}	Convert To Integer      0
    Set Test Variable       ${valorMeta}
    ${token}     Gera token       ${cpf}  
    Set Test Variable  ${token}

	${numeroConta}  ${agencia}  consultar numero da conta  ${token}  ${cpf}
    Set Test Variable  ${numeroConta}
    Set Test Variable  ${agencia}

realizado_o_put_request_com_as_informacoes_da_subconta_objetivo_meta
    # consultar subconta de um usuario        ${token}     ${cpf}     ${numeroConta}
    
    New Token DynamoDB

    ${retorno}      Consulta Dynamo Three Attributes Two Int And Bool       
    ...  Barigui.Services.Account_FinancialGoals     AccountId		${numeroConta}      Type       1
    ...  Active		true
    Log  ${retorno}
    ${idSubconta}       Set Variable      ${retorno[0]['Id']}
    
    #${auxNum}       Set Variable      ${numeroSubconta}
    ${PARAMS}       Create Dictionary       name=Aposentadoria       image=CAR      goal=${valorMeta}       deadLine=2020-03-10T19:31:02.175Z
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=83334e79-ba7b-47a3-8188-2f7b0b8dcb0a    Authorization=${token}        Content-Type=application/json-patch+json  
    ${RESPOSTAREQUEST}     Put Request     newsession      account/v1/accounts/${numeroConta}/financialgoals/${idSubconta}       data=${PARAMS}      headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text} 
    Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()} 
    ${nomeSubconta}  Catenate      ${json_value['name']}
    ${imagemSubconta}  Catenate      ${json_value['image']}
    Set Test Variable  ${subcontaEscolhida}     ${idSubconta} 
    Set Test Variable        ${nomeSubconta}
    Set Test Variable        ${imagemSubconta}

sera_alterado_o_registro_no_banco_referente_a_subconta_objetivo_meta_escolhida
    ${retorno}      Consulta Dynamo Int     Barigui.Services.Account_FinancialGoals     Id       ${subcontaEscolhida}
    ${retornoNome}  Set Variable  ${retorno[0]['Name']}
    ${retornoImagem}  Set Variable  ${retorno[0]['Image']}
    Should Be True    '${nomeSubconta}' == '${retornoNome}'
    Should Be True    '${imagemSubconta}' == '${retornoImagem}'