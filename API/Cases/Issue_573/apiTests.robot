*** Settings ***
Documentation
...    As cliente de uma conta e subconta meta ativa no Banco Bari
...    Want resgatar o dinheiro aplicado na subconta meta
Default Tags	automacao       Sprint31
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***

Usuario resgata valor aplicado
	[Tags]	backend	regression
	Given tenha_saldo_na_subconta_meta_desejada
	When chama_o_endpoint_de_resgate_da_conta
	Then valida_se_o_saldo_possui_o_desconto_igual_ao_retirado

# Validar se valor resgatado foi passado para a conta principal
# 	[Tags]	backend	regression
# 	Given guardado_o_valor_que_existe_no_saldo_da_conta_principal
# 	And tenha_chamado_o_endpoint_de_resgate_da_conta
# 	When chama_o_endpoint_de_saque
# 	Then valida_se_o_valor_resgatado_foi_adicionado_ao_saldo_da_conta_principal

*** Keywords ***

tenha_saldo_na_subconta_meta_desejada
	Set Test Variable       ${cpf}      09478206982
	${valorTrans}     Convert To Integer      1
	Set Test Variable		${valorTrans}
	${token}  Gera Token  ${cpf}
	Set Test Variable  ${token}
	${numConta}  ${agencia}  Consultar Numero Da Conta  ${token}  ${cpf}
	Set Test Variable  ${numConta}
    Set Test Variable  ${agencia}
	${saldoAntesConta}  Consultar Saldo  ${token}  ${cpf}
	Set Test Variable  ${saldoAntesConta}
	${retorno}      Consulta Dynamo Two Attributes Int And Bool    
	...  Barigui.Services.Account_FinancialGoals		AccountId		${numConta}
	...  Active		true
	Log  ${retorno}
	${idSubconta2}=		Convert To String		${retorno[0]['Id']}
	${idSubconta}     Convert To Integer      ${idSubconta2}
	Set Test Variable  ${idSubconta}
	${saldoSubAntes}  Consultar Saldo Subconta  ${token}  ${numConta}  ${idSubconta}
	Set Test Variable		${saldoSubAntes}		${saldoSubAntes['investmentTotalBalance']}

chama_o_endpoint_de_resgate_da_conta
	${PARAMS}       Create Dictionary       FinancialGoalId=${idSubconta}  Value=${valorTrans}
	${HEADERS}      Create Dictionary       accept=application/json  Content-Type=application/json-patch+json  User-Agent=cd8fc073-7ffc-4387-9c17-6b94146ddd5f    Authorization=${token}
    ${RESPOSTAREQUEST}     Post Request     newsession      account/v1/accounts/${numConta}/financialgoals/withdrawal		data=${PARAMS}        headers=${HEADERS}
    Set Test Variable		${retornoResposta}		${RESPOSTAREQUEST.json()}
	Log           ${RESPOSTAREQUEST.text}
	Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200

valida_se_o_saldo_possui_o_desconto_igual_ao_retirado
	${saldoDepoisConta}  Consultar Saldo  ${token}  ${cpf}
	${retorno}      Consulta Dynamo Two Attributes Int And Bool    
	...  Barigui.Services.Account_FinancialGoals		AccountId		${numConta}
	...  Active		true
	Log  ${retorno}
	${idSubconta}=		Convert To String		${retorno[0]['Id']}
	${saldoSubDepois}  Consultar Saldo Subconta  ${token}  ${numConta}  ${idSubconta}
	${saldoSubDepois}		Set Variable		${saldoSubDepois['investmentTotalBalance']}
	Should Be True    ${saldoDepoisConta} == ${saldoAntesConta}+${valorTrans}
	Should Be True    ${saldoSubDepois} == ${saldoSubAntes}-${valorTrans}

#OUTRO TESTE

# guardado_o_valor_que_existe_no_saldo_da_conta_principal
# 	Set Test Variable       ${cpf}      09478206982
# 	${token}  Gera Token  ${cpf}
# 	${saldoAntes}  consultar saldo  ${token}  ${cpf}
# 	Set Test Variable		${saldoAntes}

# tenha_chamado_o_endpoint_de_resgate_da_conta
# 	Set Test Variable       ${cpf}      09478206982
# 	Set Test Variable		${numConta}		19503
# 	Set Test Variable		${valorTrans}		1
# 	${token}  Gera Token  ${cpf}
# 	Set Test Variable  ${token}

# 	${PARAMS}       Create Dictionary       financialGoalId=${idSubconta}  value=${valorTrans}
# 	${HEADERS}      Create Dictionary       accept=application/json  Content-Type=application/json-patch+json  User-Agent=cd8fc073-7ffc-4387-9c17-6b94146ddd5f    Authorization=${token}
#     ${RESPOSTAREQUEST}     Post Request     newsession      account/v1/accounts/${numConta}/financialgoals/withdrawal		data=${PARAMS}        headers=${HEADERS}
#     Set Test Variable		${retornoResposta}		${RESPOSTAREQUEST.json()}
# 	Log           ${RESPOSTAREQUEST.text}
# 	Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200

# chama_o_endpoint_de_saque
# 	${PARAMS}       Create Dictionary       financialGoalId=${idSubconta}  value=${valorTrans}
# 	${HEADERS}      Create Dictionary       accept=application/json  Content-Type=application/json-patch+json  User-Agent=cd8fc073-7ffc-4387-9c17-6b94146ddd5f    Authorization=${token}
#     ${RESPOSTAREQUEST}     Post Request     newsession      account/v1/accounts/${numConta}/financialgoals/withdrawal		data=${PARAMS}        headers=${HEADERS}
#     Set Test Variable		${retornoResposta}		${RESPOSTAREQUEST.json()}
# 	Log           ${RESPOSTAREQUEST.text}

# valida_se_o_valor_resgatado_foi_adicionado_ao_saldo_da_conta_principal
# 	${saldoDepois}		consultar saldo		${token}     ${cpf}
# 	Set Test Variable  ${saldoDepois}
# 	Should Be True    ${saldoDepois} == ${saldoAntes}+${valorTrans}