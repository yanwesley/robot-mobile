*** Settings ***
Documentation
...  As usuário ativo no APP do Banco Bari,
...  Want alterar o nome e imagem do objetivo cartão
Default Tags	automacao       Sprint34
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***

Alterar uma subconta objetivo cartao
	[Tags]	backend	regression
	Given endpoint_para_alterar_uma_subconta_objetivo_cartao
	When realizado_o_put_request_com_as_informacoes_da_subconta_objetivo_cartao
	Then sera_alterado_o_registro_no_banco_referente_a_subconta_objetivo_cartao_escolhida

Alterar apenas imagem de uma subconta objetivo cartao
	[Tags]	backend	regression
	Given endpoint_para_alterar_apenas_a_imagem_da_subconta_objetivo_cartao
	When realizado_o_put_request_com_a_imagem_da_subconta_objetivo_cartao
	Then sera_alterado_o_registro_no_banco_alterando_apenas_a_imagem

Alterar apenas nome de uma subconta objetivo cartao
	[Tags]	backend	regression
	Given endpoint_para_alterar_apenas_o_nome_da_subconta_objetivo_cartao
	When realizado_o_put_request_com_o_nome_da_subconta_objetivo_cartao
	Then sera_alterado_o_registro_no_banco_alterando_apenas_o_nome

*** Keywords ***

endpoint_para_alterar_uma_subconta_objetivo_cartao
    Set Test Variable       ${cpf}      09478206982
    ${token}        Gera token       ${cpf} 
    Set Test Variable  ${token}
	${numeroConta}  ${agencia}  Consultar Numero Da Conta  ${token}  ${cpf}
	Set Test Variable  ${numeroConta}
    Set Test Variable  ${agencia}

realizado_o_put_request_com_as_informacoes_da_subconta_objetivo_cartao
    ${retorno}      Consulta Dynamo Three Attributes Two Int And Bool       
    ...  Barigui.Services.Account_FinancialGoals     AccountId		${numeroConta}      Type       2
    ...  Active		true
    Log  ${retorno}
    ${numeroSubconta}       Set Variable      ${retorno[0]['Id']}
    ${PARAMS}       Create Dictionary       name=Cartaozinho       image=EDUCATION
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=83334e79-ba7b-47a3-8188-2f7b0b8dcb0a    Authorization=${token}        Content-Type=application/json-patch+json  
    ${RESPOSTAREQUEST}     Put Request     newsession      account/v1/accounts/${numeroConta}/financialgoals/${numeroSubconta}       data=${PARAMS}      headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text} 
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()} 
    ${nomeSubCartao}  Catenate      ${json_value['name']}
    ${imagemSubCartao}  Catenate      ${json_value['image']}
    Set Test Variable        ${numeroSubconta}
    Set Test Variable        ${nomeSubCartao}
    Set Test Variable        ${imagemSubCartao}

sera_alterado_o_registro_no_banco_referente_a_subconta_objetivo_cartao_escolhida
    ${retorno}      Consulta Dynamo Int     Barigui.Services.Account_FinancialGoals     Id       ${numeroSubconta}
    ${retornoNome}  Set Variable  ${retorno[0]['Name']}
    ${retornoImagem}  Set Variable  ${retorno[0]['Image']}
    Should Be True    '${nomeSubCartao}' == '${retornoNome}'
    Should Be True    '${imagemSubCartao}' == '${retornoImagem}'

#OUTRO TESTE

endpoint_para_alterar_apenas_a_imagem_da_subconta_objetivo_cartao
    Set Test Variable       ${cpf}      09478206982
    ${token}        Gera token       ${cpf} 
    Set Test Variable  ${token}
	${numeroConta}  ${agencia}  Consultar Numero Da Conta  ${token}  ${cpf}
	Set Test Variable  ${numeroConta}
    Set Test Variable  ${agencia}
    ${retorno}      Consulta Dynamo Three Attributes Two Int And Bool       
    ...  Barigui.Services.Account_FinancialGoals     AccountId		${numeroConta}      Type       2
    ...  Active		true
    Log  ${retorno}
    Set Test Variable      ${numeroSubconta}       ${retorno[0]['Id']}

realizado_o_put_request_com_a_imagem_da_subconta_objetivo_cartao
    ${PARAMS}       Create Dictionary       image=TARGET
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=83334e79-ba7b-47a3-8188-2f7b0b8dcb0a    Authorization=${token}        Content-Type=application/json-patch+json  
    ${RESPOSTAREQUEST}     Put Request     newsession      account/v1/accounts/${numeroConta}/financialgoals/${numeroSubconta}       data=${PARAMS}      headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text} 
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()} 
    ${imagemSubCartao}  Catenate      ${json_value['image']}
    Set Test Variable        ${imagemSubCartao}

sera_alterado_o_registro_no_banco_alterando_apenas_a_imagem
    ${retorno}      Consulta Dynamo Int     Barigui.Services.Account_FinancialGoals     Id       ${numeroSubconta}
    ${retornoImagem}  Set Variable  ${retorno[0]['Image']}
    Should Be True    '${imagemSubCartao}' == '${retornoImagem}'


#OUTRO TESTE

endpoint_para_alterar_apenas_o_nome_da_subconta_objetivo_cartao
    Set Test Variable       ${cpf}      09478206982
    ${token}        Gera token       ${cpf} 
    Set Test Variable  ${token}
	${numeroConta}  ${agencia}  Consultar Numero Da Conta  ${token}  ${cpf}
	Set Test Variable  ${numeroConta}
    Set Test Variable  ${agencia}
    ${retorno}      Consulta Dynamo Three Attributes Two Int And Bool       
    ...  Barigui.Services.Account_FinancialGoals     AccountId		${numeroConta}      Type       2
    ...  Active		true
    Log  ${retorno}
    Set Test Variable      ${numeroSubconta}       ${retorno[0]['Id']}

realizado_o_put_request_com_o_nome_da_subconta_objetivo_cartao
    ${PARAMS}       Create Dictionary       name=Conta Cartao       
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=83334e79-ba7b-47a3-8188-2f7b0b8dcb0a    Authorization=${token}        Content-Type=application/json-patch+json  
    ${RESPOSTAREQUEST}     Put Request     newsession      account/v1/accounts/${numeroConta}/financialgoals/${numeroSubconta}       data=${PARAMS}      headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text} 
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()} 
    ${nomeSubCartao}  Catenate      ${json_value['name']}
    Set Test Variable        ${nomeSubCartao}

sera_alterado_o_registro_no_banco_alterando_apenas_o_nome
    ${retorno}      Consulta Dynamo Int     Barigui.Services.Account_FinancialGoals     Id       ${numeroSubconta}
    ${retornoNome}  Set Variable  ${retorno[0]['Name']}
    Should Be True    '${nomeSubCartao}' == '${retornoNome}'