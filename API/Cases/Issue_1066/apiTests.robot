*** Settings ***
Documentation
...  As cliente do Banco Bari com uma conta Ativa
...  Want excluir minha subconta controle junto com o cartão de débito
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***

cliente excluindo a subconta controle junto com o cartao de debito
	Given que_o_cliente_tenha_uma_subconta_controle_cartao
	When solicita_a_exclusao_da_subconta_controle_cartao
	Then e_disparado_um_evento_para_solicitar_o_cancelamento_do_cartao


*** Keywords ***

que_o_cliente_tenha_uma_subconta_controle_cartao

	${token}  Gera Token  09699387998
	Set Test Variable  ${token}
	${HEADERS}      Create Dictionary       accept=application/json   Content-Type=application/json  User-Agent=8d31a8d5-f4a5-429b-943f-04a6d15bda46    Authorization=${token}  
    ${PARAMS}       Create Dictionary       name=Cancela  image=CAR  goal=0  deadLine=2020-03-05T21:16:59.450Z  type=2

	${conta}  consultar numero da conta   ${token}   09699387998
	Log   ${conta}

    Create Session    session    https://api.qa.bancobari.com.br/account/v1/accounts
    ${RESPOSTA}     Post Request    session   ${conta[0]}/financialgoals    headers=${HEADERS}    data=${PARAMS}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
	${json_response}   Set Variable   ${RESPOSTA.json()}
	${id_subconta}   Set Variable   ${json_response['id']}
	Set Test Variable  ${id_subconta}

	# Solicita a geração do cartão
	Create Session    session    https://api.qa.bancobari.com.br/account
	${PARAMS}       Create Dictionary       cardPrintedName=teste cancela
    ${RESPOSTA}     Post Request    session   v1/accounts/${conta[0]}/subaccounts/${id_subconta}/cards    headers=${HEADERS}    data=${PARAMS}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
	${json_response}   Set Variable   ${RESPOSTA.json()}
	${id_cartao}   Set Variable   ${json_response['id']}
	Set Test Variable  ${id_cartao}


solicita_a_exclusao_da_subconta_controle_cartao

    ${HEADERS}      Create Dictionary       accept=application/json   Content-Type=application/json  User-Agent=8d31a8d5-f4a5-429b-943f-04a6d15bda46    Authorization=${token}     
    Create Session    session    https://api.qa.bancobari.com.br/account
	${RESPOSTA}     Delete Request    session   v1/accounts/${conta[0]}/financialgoals/${id_subconta}   headers=${HEADERS}    
	Should Be Equal As Strings      ${RESPOSTA.status_code}    202 


e_disparado_um_evento_para_solicitar_o_cancelamento_do_cartao
	Sleep   10s
	${HEADERS}      Create Dictionary       accept=application/json   Content-Type=application/json  User-Agent=8d31a8d5-f4a5-429b-943f-04a6d15bda46    Authorization=${token}     
    Create Session    session    https://api.qa.bancobari.com.br/account
	${RESPOSTA}     Get Request    session   v1/accounts/${conta[0]}/financialgoals/${id_subconta}   headers=${HEADERS}    
	Should Be Equal As Strings      ${RESPOSTA.status_code}    200 
	${json_response}   Set Variable   ${RESPOSTA.json()}
	Log    ${json_response}
	${status}   Set Variable   ${json_response['card']['status']}
	Should Be Equal As Strings      ${status}    error 
	Log     Se tivesse finalizado a solicitação do cartão o status seria cancelled