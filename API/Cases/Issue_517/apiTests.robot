*** Settings ***
Documentation
...    As usuário do salesforce
...    Want recebo a informação do CPF vicnulado a conta
Default Tags	automacao       Sprint30
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://barigui--qabari.my.salesforce.com/

*** Test Cases ***

Enviando CPF do usuario para o Salesforce
	[Tags]	backend	regression
	 Given o_usuario_terminou_o_onboarding
	 And foi_criado_sua_conta_na_lydians
	 When a_lydians_retornar_o_vinculo_do_cpf_com_o_numero_da_conta_daquele_usuario
	 Then sera_enviado_o_cpf_do_novo_cliente_para_o_salesforce

*** Keywords ***

o_usuario_terminou_o_onboarding
	# Usuário para realizar o teste já passou 
	Log  pula

foi_criado_sua_conta_na_lydians
	# Usuário para realizar o teste já criou 
	Log  pula

a_lydians_retornar_o_vinculo_do_cpf_com_o_numero_da_conta_daquele_usuario
	# Não temos o retorno da Lydians para acessar no teste
	Log  pula

sera_enviado_o_cpf_do_novo_cliente_para_o_salesforce
	Set Test Variable       ${cpf}      86417962075
	${token}		Gera Token SF
	Consulta SF		${token}		${cpf}

Gera Token SF
    Abrir conexao   https://test.salesforce.com
    ${HEADERS}  Create Dictionary
    ...		Content-Type=application/x-www-form-urlencoded
	...		Cookie=BrowserId=bccQ5W0TEeqmzJPiRxvSXA
    ${PARAMS}  Create Dictionary
    ...  grant_type=password
    ...  client_id=3MVG9Vik22TUgUphKhs9dOlQTssExvUo53Ez3Jl1IAYrDcy5Ete.gED_B5Or1jjCyQSv_hKXViKgmnTym9R1T
    ...  client_secret=FA050230A427D30689D08F4DA22F9ABAFCE5AF0602CFC65A5D940DF6D7D5A4F4
    ...  username=admin.integracao@bari.com.br.qabari
    ...  password=P@ssIntegracao@5
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      services/oauth2/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}

Consulta SF
    [Arguments]  ${token}  ${cpf}
	Abrir conexao		https://barigui--qabari.my.salesforce.com/
    ${HEADERS}  Create Dictionary
    ...     Authorization=${token}
	${RESPOSTA}     Get Request     newsession      services/apexrest/Bari/WS_GET_ContaPagamento?customerId=${cpf}
    ...     headers=${HEADERS}
    Log                             ${RESPOSTA.text}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
	
	${json_value}  Set Variable  ${RESPOSTA.json()}
	Should Be True      '${json_value['body'][0]['numeroConta']}' == '14308'