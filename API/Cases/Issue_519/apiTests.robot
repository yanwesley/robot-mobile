*** Settings ***
Documentation
...    As usuário do salesforce
...    Want visualizo o saldo em conta dos clientes
Default Tags	automacao       Sprint30
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***

O Salesforce acessando o saldo do cliente
	[Tags]	backend	regression
	Given que_o_salesforce_consulta_o_saldo_do_cliente
	When ocorrer_a_chamada_get_de_consultar_o_saldo
	Then retorna_os_dados_requisitados_pelo_salesforce

*** Keywords ***

que_o_salesforce_consulta_o_saldo_do_cliente
	Set Test Variable       ${cpf}      09478206982
	${token}  Gera Token Salesforce
	Set Test Variable  ${token}
	
ocorrer_a_chamada_get_de_consultar_o_saldo
	${saldo}  consultar saldo  ${token}  ${cpf}
	
retorna_os_dados_requisitados_pelo_salesforce
	Should Be True		'${retornoSaldo['accountId']}'!=''
	Should Be True		'${retornoSaldo['agency']}'!=''
	Should Be True		'${retornoSaldo['balance']}'!=''
	Should Be True		'${retornoSaldo['creationDate']}'!=''