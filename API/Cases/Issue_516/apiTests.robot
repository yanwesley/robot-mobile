*** Settings ***
Documentation
...  As cliente do Banco Bari com uma conta Ativa
...  Want favoritar históricos de contatos de transferências
Default Tags	automacao       Sprint33
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***
    
Listar os contatos de transferencias
	[Tags]	backend	regression
	Given endpoint_para_listar_todos_os_contatos_de_transferencias
	When realizado_o_get_request_para_listar_os_contatos
	Then sera_validado_com_a_tabela_de_contatos

Favoritando um contato de transferencia
	[Tags]	backend	regression
	Given endpoint_para_favoritar_um_contato_de_transferencia
	When realizado_o_put_request_para_favoritar_o_contato
	Then sera_adicionado_o_contato_nos_favoritos

Tirar o favorito do contato de transferencia
	[Tags]	backend	regression
	Given endpoint_para_tirar_o_favorito_do_contato_de_transferencia
	When realizado_o_put_request_para_tirar_o_favorito_do_contato
	Then sera_tirado_o_contato_nos_favoritos

*** Keywords ***

endpoint_para_listar_todos_os_contatos_de_transferencias
    Set Test Variable       ${cpf}      09478206982
    ${token}     Gera token       ${cpf}  
    Set Test Variable  ${token}

realizado_o_get_request_para_listar_os_contatos
   Consultar Contatos Transferencia  ${token}  ${cpf}
   Set Test Variable  ${idContato}
   Set Test Variable  ${nomeContato}

sera_validado_com_a_tabela_de_contatos
    New Token DynamoDB
    ${retorno}      consulta Dynamo Two Attributes String And Bool      Barigui.Services.Account_RelatedAccounts
    ...     CustomerId       ${cpf}
    ...     Active       true
    Log  ${retorno}
    # Should Be True    '${idContato}' == '${retorno[0]['RelatedAccountId']}'
    # Should Be True    '${nomeContato}' == '${retorno[0]['Name']}'
    ${length} =     Get Length      ${idContato}
    ${aux} =     Get Length      ${nomeContato}
    ${auxNum}       Set Variable      ${idContato}
    ${auxNome}       Set Variable      ${nomeContato}
    :FOR        ${index}      IN RANGE        ${length}
    \   Should Be True    '${auxNome[${index}]}' == '${retorno[${aux}-1]['Name']}'
    \   Should Be True    '${auxNum[${index}]}' == '${retorno[${aux}-1]['RelatedAccountId']}'
    \   ${aux}       Set Variable        ${aux}-1


#OUTRO TESTE

endpoint_para_favoritar_um_contato_de_transferencia
    Set Test Variable       ${cpf}      09478206982
    ${favorito}     Convert To Boolean      true
    Set Test Variable       ${favorito}
    ${token}     Gera token       ${cpf}  
    Set Test Variable  ${token}
    Consultar Contatos Transferencia  ${token}  ${cpf}
    Log  ${nomeContato[0]}
    Log  ${idContato[0]}	   

realizado_o_put_request_para_favoritar_o_contato
    ${PARAMS}       Create Dictionary       name=${nomeContato[0]}        favorite=${favorito}
	${HEADERS}      Create Dictionary       accept=application/json  User-Agent=901f78d0-062a-4ec9-b232-63629b8baad9    Authorization=${token}      Content-Type=application/json-patch+json
    ${RESPOSTAREQUEST}     Put Request     newsession      account/v1/accounts/${cpf}/contacts/${idContato[0]}		
	...		data=${PARAMS}        headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text}

    ${retornoFavorito}  Set Variable  ${RESPOSTAREQUEST.json()} 
    ${nomeRetorno}  Catenate      ${retornoFavorito['name']}
    ${cpfRetorno}  Catenate      ${retornoFavorito['cpfCnpj']}
    ${agenciaRetorno}  Catenate      ${retornoFavorito['relatedAccounts'][0]['agencyNumber']}
    ${contaRetorno}  Catenate      ${retornoFavorito['relatedAccounts'][0]['accountNumber']}

    Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200
	Set Test Variable  ${nomeRetorno}
    Set Test Variable  ${cpfRetorno}
    Set Test Variable  ${agenciaRetorno}
    Set Test Variable  ${contaRetorno}


sera_adicionado_o_contato_nos_favoritos
    ${retorno}      consulta Dynamo Two Attributes String And Bool      Barigui.Services.Account_RelatedAccounts
    ...     CustomerId       ${cpf}
    ...     Active       true
    Log  ${retorno}
    ${length} =     Get Length      ${idContato}
    Log  ${retorno[${length}-1]}
    Should Be True    '${nomeRetorno}' == '${retorno[${length}-1]['Name']}'
    Should Be True    'True' == '${retorno[${length}-1]['Favorite']}'
    Should Be True    '${agenciaRetorno}' == '${retorno[${length}-1]['RelatedAccounts'][0]['AgencyNumber']}'
    Should Be True    '${contaRetorno}' == '${retorno[${length}-1]['RelatedAccounts'][0]['AccountNumber']}'

#OUTRO TESTE

endpoint_para_tirar_o_favorito_do_contato_de_transferencia
    Set Test Variable       ${cpf}      09478206982
    ${favorito}     Convert To Boolean      false
    Set Test Variable       ${favorito}
    ${token}     Gera token       ${cpf}  
    Set Test Variable  ${token}
    Consultar Contatos Transferencia  ${token}  ${cpf}
    Log  ${nomeContato[0]}
    Log  ${idContato[0]}	

realizado_o_put_request_para_tirar_o_favorito_do_contato
    ${PARAMS}       Create Dictionary       name=${nomeContato[0]}        favorite=${favorito}
	${HEADERS}      Create Dictionary       accept=application/json  User-Agent=901f78d0-062a-4ec9-b232-63629b8baad9    Authorization=${token}      Content-Type=application/json-patch+json
    ${RESPOSTAREQUEST}     Put Request     newsession      account/v1/accounts/${cpf}/contacts/${idContato[0]}		
	...		data=${PARAMS}        headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text}

    ${retornoFavorito}  Set Variable  ${RESPOSTAREQUEST.json()} 
    ${nomeRetorno}  Catenate      ${retornoFavorito['name']}
    ${cpfRetorno}  Catenate      ${retornoFavorito['cpfCnpj']}
    ${agenciaRetorno}  Catenate      ${retornoFavorito['relatedAccounts'][0]['agencyNumber']}
    ${contaRetorno}  Catenate      ${retornoFavorito['relatedAccounts'][0]['accountNumber']}

    Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200
	Set Test Variable  ${nomeRetorno}
    Set Test Variable  ${agenciaRetorno}
    Set Test Variable  ${contaRetorno}

sera_tirado_o_contato_nos_favoritos
    ${retorno}      consulta Dynamo Two Attributes String And Bool      Barigui.Services.Account_RelatedAccounts
    ...     CustomerId       ${cpf}
    ...     Active       true
    Log  ${retorno}
    ${length} =     Get Length      ${idContato}
    Should Be True    '${nomeRetorno}' == '${retorno[${length}-1]['Name']}'
    Should Be True    'False' == '${retorno[${length}-1]['Favorite']}'
    Should Be True    '${agenciaRetorno}' == '${retorno[${length}-1]['RelatedAccounts'][0]['AgencyNumber']}'
    Should Be True    '${contaRetorno}' == '${retorno[${length}-1]['RelatedAccounts'][0]['AccountNumber']}'