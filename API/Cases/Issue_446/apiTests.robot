*** Settings ***
Documentation
...  As cliente ativo do Banco Bari com uma subconta cartão,
...  Want ativa a função de transferência recorrente para a subconta.
Default Tags	automacao       Sprint36
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***

Habilitando a função de transferência recorrente na subconta objetivo cartão
    Given usuario_tenha_uma_conta
    And usuario_tenha_uma_subconta_controle
    When habilita_a_funçao_de_ativacao_de_transferencia_recorrente
    Then funcao_de_transferencia_recorrente_estara_habilitada

Habilitando a função de transferência recorrente usando opcao mes
    Given usuario_tenha_uma_subconta
    When habilita_a_funçao_de_transferencia_recorrente_por_mes
    Then a_funcao_de_transferencia_recorrente_estara_habilitada_por_mes

Habilitando a função de transferência recorrente usando opcao dia
    Given usuario_tenha_uma_subconta_controle_ativa
    When habilita_a_funçao_de_transferencia_recorrente_por_dia
    Then a_funcao_de_transferencia_recorrente_estara_habilitada_por_dia

Desabilitando a função de transferência recorrente na subconta objetivo cartão
    Given usuario_tenha_uma_conta_ativa
    When desabilita_a_funçao_de_ativacao_de_transferencia_recorrente
    Then funcao_de_transferencia_recorrente_estara_desabilitada

*** Keywords ***

# Case 01:

usuario_tenha_uma_conta
    Set Test Variable      ${cpf}      08517615930

    ${token}  Gera Token   ${cpf}
    Set Test Variable  ${token}

    ${numConta}		${numAgen}		Consultar Numero Da Conta		${token}		${cpf}
    Set Test Variable  ${numConta}
    Set Test Variable  ${testeAtivar}  True

usuario_tenha_uma_subconta_controle
    ${retorno}  Consulta Dynamo Three Attributes Two Int And Bool
    ...         Barigui.Services.Account_FinancialGoals        AccountId        ${numConta}
    ...         Type       2
    ...         Active     true
    Log         ${retorno}
    ${idSubconta}=        Convert To String        ${retorno[0]['Id']}
    Set Test Variable  ${idSubconta}
    Set Test Variable  ${infoSubconta}  ${retorno[0]}
    Log  ${infoSubconta}

habilita_a_funçao_de_ativacao_de_transferencia_recorrente

    Set Test Variable  ${ativarTrans}  true
    Set Test Variable  ${valorTransf}  1
    Set Test Variable  ${tipoRecor}  weekly
    Set Test Variable  ${valorRecor}  Tuesday

    ${returnActiveRec}     Ativacao Transferencia Recorrente  ${token}  ${numConta}  ${idSubconta}  ${ativarTrans}  ${valorTransf}
    ...  ${tipoRecor}  ${valorRecor}

funcao_de_transferencia_recorrente_estara_habilitada
    Should Be True    '${returnActiveRec['id']}' == '${idSubconta}'
    Should Be True    '${returnActiveRec['name']}' == '${infoSubconta['Name']}'
    Should Be True    '${returnActiveRec['image']}' == '${infoSubconta['Image']}'
    Should Be True    '${returnActiveRec['recurrentDeposit']['value']}' == '${valorTransf}'
    Should Be True    '${returnActiveRec['recurrentDeposit']['active']}' == '${testeAtivar}'
    Should Be True    '${returnActiveRec['recurrentDeposit']['repetition']['dailyValue']}' == 'None'
    Should Be True    '${returnActiveRec['recurrentDeposit']['repetition']['type']}' == '${tipoRecor}'
    Should Be True    '${returnActiveRec['recurrentDeposit']['repetition']['value']}' == '${valorRecor}'

    ${retornoRecur}  Consulta Dynamo Two Attributes Int And Bool  Barigui.Services.Account_RecurrenceSchedule
    ...  FinancialGoalId  ${idSubconta}
    ...  Active  ${ativarTrans}
    Should Be True    '${retornoRecur[0]['AccountId']}' == '${numConta}'
    Should Be True    '${retornoRecur[0]['Active']}' == '${testeAtivar}'
    Should Be True    '${retornoRecur[0]['NextSchedule']}' == '${retornoRecur[0]['NextSchedule']}'
    Should Be True    '${retornoRecur[0]['Type']}' == '1'
    Should Be True    '${retornoRecur[0]['TypeValue']}' == '${valorRecor}'
    Should Be True    '${retornoRecur[0]['Value']}' == '1'
    

#OUTRO TESTE

usuario_tenha_uma_subconta
    Set Test Variable      ${cpf}      08517615930

    ${token}  Gera Token   ${cpf}
    Set Test Variable  ${token}

    ${numConta}		${numAgen}		Consultar Numero Da Conta		${token}		${cpf}
    Set Test Variable  ${numConta}
    Set Test Variable  ${testeAtivar}  True

    ${retorno}  Consulta Dynamo Three Attributes Two Int And Bool
    ...         Barigui.Services.Account_FinancialGoals        AccountId        ${numConta}
    ...         Type       2
    ...         Active     true
    Log         ${retorno}
    ${idSubconta}=        Convert To String        ${retorno[0]['Id']}
    Set Test Variable  ${idSubconta}
    Set Test Variable  ${infoSubconta}  ${retorno[0]}
    Log  ${infoSubconta}

habilita_a_funçao_de_transferencia_recorrente_por_mes
    Set Test Variable  ${ativarTrans}  true
    Set Test Variable  ${valorTransf}  1
    Set Test Variable  ${tipoRecor}  monthly
    Set Test Variable  ${valorRecor}  30

    ${returnActiveRec}     Ativacao Transferencia Recorrente  ${token}  ${numConta}  ${idSubconta}  ${ativarTrans}  ${valorTransf}
    ...  ${tipoRecor}  ${valorRecor}

a_funcao_de_transferencia_recorrente_estara_habilitada_por_mes
    Should Be True    '${returnActiveRec['id']}' == '${idSubconta}'
    Should Be True    '${returnActiveRec['name']}' == '${infoSubconta['Name']}'
    Should Be True    '${returnActiveRec['image']}' == '${infoSubconta['Image']}'
    Should Be True    '${returnActiveRec['recurrentDeposit']['value']}' == '${valorTransf}'
    Should Be True    '${returnActiveRec['recurrentDeposit']['active']}' == '${testeAtivar}'
    Should Be True    '${returnActiveRec['recurrentDeposit']['repetition']['dailyValue']}' == 'None'
    Should Be True    '${returnActiveRec['recurrentDeposit']['repetition']['type']}' == '${tipoRecor}'
    Should Be True    '${returnActiveRec['recurrentDeposit']['repetition']['value']}' == '${valorRecor}'

    ${retornoRecur}  Consulta Dynamo Two Attributes Int And Bool  Barigui.Services.Account_RecurrenceSchedule
    ...  FinancialGoalId  ${idSubconta}
    ...  Active  ${ativarTrans}
    Should Be True    '${retornoRecur[0]['AccountId']}' == '${numConta}'
    Should Be True    '${retornoRecur[0]['Active']}' == '${testeAtivar}'
    Should Be True    '${retornoRecur[0]['NextSchedule']}' == '${retornoRecur[0]['NextSchedule']}'
    Should Be True    '${retornoRecur[0]['Type']}' == '2'
    Should Be True    '${retornoRecur[0]['TypeValue']}' == '${valorRecor}'
    Should Be True    '${retornoRecur[0]['Value']}' == '1'
    
#TERCEIRO TESTE

usuario_tenha_uma_subconta_controle_ativa
    Set Test Variable      ${cpf}      08517615930

    ${token}  Gera Token   ${cpf}
    Set Test Variable  ${token}

    ${numConta}		${numAgen}		Consultar Numero Da Conta		${token}		${cpf}
    Set Test Variable  ${numConta}
    Set Test Variable  ${testeAtivar}  True

    ${retorno}  Consulta Dynamo Three Attributes Two Int And Bool
    ...         Barigui.Services.Account_FinancialGoals        AccountId        ${numConta}
    ...         Type       2
    ...         Active     true
    Log         ${retorno}
    ${idSubconta}=        Convert To String        ${retorno[0]['Id']}
    Set Test Variable  ${idSubconta}
    Set Test Variable  ${infoSubconta}  ${retorno[0]}
    Log  ${infoSubconta}

habilita_a_funçao_de_transferencia_recorrente_por_dia
    Set Test Variable  ${ativarTrans}  true
    Set Test Variable  ${valorTransf}  1
    Set Test Variable  ${tipoRecor}  daily
    Set Test Variable  ${valorRecor}  None

    ${returnActiveRec}     Ativacao Transferencia Recorrente  ${token}  ${numConta}  ${idSubconta}  ${ativarTrans}  ${valorTransf}
    ...  ${tipoRecor}  ${valorRecor}

a_funcao_de_transferencia_recorrente_estara_habilitada_por_dia
    Should Be True    '${returnActiveRec['id']}' == '${idSubconta}'
    Should Be True    '${returnActiveRec['name']}' == '${infoSubconta['Name']}'
    Should Be True    '${returnActiveRec['image']}' == '${infoSubconta['Image']}'
    Should Be True    '${returnActiveRec['recurrentDeposit']['value']}' == '${valorTransf}'
    Should Be True    '${returnActiveRec['recurrentDeposit']['active']}' == '${testeAtivar}'
    Should Be True    '${returnActiveRec['recurrentDeposit']['repetition']['dailyValue']}' == 'None'
    Should Be True    '${returnActiveRec['recurrentDeposit']['repetition']['type']}' == '${tipoRecor}'
    Should Be True    '${returnActiveRec['recurrentDeposit']['repetition']['value']}' == '${valorRecor}'

    ${retornoRecur}  Consulta Dynamo Two Attributes Int And Bool  Barigui.Services.Account_RecurrenceSchedule
    ...  FinancialGoalId  ${idSubconta}
    ...  Active  ${ativarTrans}
    Log  ${retornoRecur[0]}
    Should Be True    '${retornoRecur[0]['AccountId']}' == '${numConta}'
    Should Be True    '${retornoRecur[0]['Active']}' == '${testeAtivar}'
    Should Be True    '${retornoRecur[0]['NextSchedule']}' == '${retornoRecur[0]['NextSchedule']}'
    Should Be True    '${retornoRecur[0]['Type']}' == '3'
    Should Be True    '${retornoRecur[0]['Value']}' == '1'

# QUARTO TESTE

usuario_tenha_uma_conta_ativa
    Set Test Variable      ${cpf}      08517615930

    ${token}  Gera Token   ${cpf}
    Set Test Variable  ${token}

    ${numConta}		${numAgen}		Consultar Numero Da Conta		${token}		${cpf}
    Set Test Variable  ${numConta}
    Set Test Variable  ${testeAtivar}  False

    ${retorno}  Consulta Dynamo Three Attributes Two Int And Bool
    ...         Barigui.Services.Account_FinancialGoals        AccountId        ${numConta}
    ...         Type       2
    ...         Active     true
    Log         ${retorno}
    ${idSubconta}=        Convert To String        ${retorno[0]['Id']}
    Set Test Variable  ${idSubconta}
    Set Test Variable  ${infoSubconta}  ${retorno[0]}
    Log  ${infoSubconta}

desabilita_a_funçao_de_ativacao_de_transferencia_recorrente
    Set Test Variable  ${ativarTrans}  false
    Set Test Variable  ${valorTransf}  1
    Set Test Variable  ${tipoRecor}  weekly
    Set Test Variable  ${valorRecor}  Tuesday

    ${returnActiveRec}     Ativacao Transferencia Recorrente  ${token}  ${numConta}  ${idSubconta}  ${ativarTrans}  ${valorTransf}
    ...  ${tipoRecor}  ${valorRecor}

funcao_de_transferencia_recorrente_estara_desabilitada
    Should Be True    '${returnActiveRec['id']}' == '${idSubconta}'
    Should Be True    '${returnActiveRec['name']}' == '${infoSubconta['Name']}'
    Should Be True    '${returnActiveRec['image']}' == '${infoSubconta['Image']}'
    Should Be True    '${returnActiveRec['recurrentDeposit']['value']}' == '${valorTransf}'
    Should Be True    '${returnActiveRec['recurrentDeposit']['active']}' == '${testeAtivar}'
    Should Be True    '${returnActiveRec['recurrentDeposit']['repetition']['dailyValue']}' == 'None'
    Should Be True    '${returnActiveRec['recurrentDeposit']['repetition']['type']}' == '${tipoRecor}'
    Should Be True    '${returnActiveRec['recurrentDeposit']['repetition']['value']}' == '${valorRecor}'

    # ${retornoRecur}  Consulta Dynamo Two Attributes Int And Bool  Barigui.Services.Account_RecurrenceSchedule
    # ...  FinancialGoalId  ${idSubconta}
    # ...  Active  False

    ${retornoRecur}  Consulta Dynamo Int  Barigui.Services.Account_RecurrenceSchedule
    ...  FinancialGoalId  ${idSubconta}

    Should Be True    '${retornoRecur[0]['AccountId']}' == '${numConta}'
    Should Be True    '${retornoRecur[0]['Active']}' == '${testeAtivar}'
    Should Be True    '${retornoRecur[0]['NextSchedule']}' == '${retornoRecur[0]['NextSchedule']}'
    Should Be True    '${retornoRecur[0]['Type']}' == '1'
    Should Be True    '${retornoRecur[0]['TypeValue']}' == '${valorRecor}'
    Should Be True    '${retornoRecur[0]['Value']}' == '1'