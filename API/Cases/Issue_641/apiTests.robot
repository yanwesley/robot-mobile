*** Settings ***
Documentation
...  As cliente do Banco Bari com uma conta Ativa
...  Want solicitar meu cartão da subconta cartão.
Default Tags	automacao       Sprint35
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***

Valida o nome cartao
	[Tags]	backend	regression	automation
	Given que_digita_as_informacoes_para_realizar_o_teste
	When chama_o_endpoint_de_validar_nome_cartao
	Then valida_o_que_veio_no_retorno_da_chamada

Valida nome invalido cartao
	[Tags]	backend	regression	automation
	Given que_digita_as_informacoes_para_realizar_o_teste_do_nome_cartao
	When chama_o_endpoint_de_validar_nome_cartao_enviando_um_invalido
	Then valida_que_o_nome_e_invalido

Solicita cartao
	[Tags]	backend	regression	automation
	Given que_digita_as_informacoes_para_solicitar_o_cartao
	When chama_o_endpoint_de_solicitar_o_cartao
	Then valida_que_a_solicitacao_esta_na_tabela_do_banco

*** Keywords ***

que_digita_as_informacoes_para_realizar_o_teste
    Set Test Variable       ${cpf}      09478206982 
	${token}  Gera Token  ${cpf}
	Set Test Variable  ${token}

chama_o_endpoint_de_validar_nome_cartao
    ${PARAMS}=      catenate       {\"Name\": \"Teste\"}
	${HEADERS}      Create Dictionary       accept=application/json  User-Agent=08b86528-dd82-4982-b6a4-a5a2669864bf    Authorization=${token}      Content-Type=application/json-patch+json
    ${RESPOSTAREQUEST}     Post Request     newsession      account/v1/accounts/financialgoals/validatecardname		
	...		data=${PARAMS}        headers=${HEADERS}
    Log  ${RESPOSTAREQUEST.json()} 
    ${retorno}  Set Variable  ${RESPOSTAREQUEST.json()} 
    Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200
    Set Test Variable  ${retorno}

valida_o_que_veio_no_retorno_da_chamada
    Should Be True    '${retorno['isValid']}' == 'True'

#OUTRO TESTE
que_digita_as_informacoes_para_realizar_o_teste_do_nome_cartao
    Set Test Variable       ${cpf}      09478206982 
	${token}  Gera Token  ${cpf}
	Set Test Variable  ${token}

    New Token DynamoDB

    ${nomeInvalido}      Consulta Dynamo Attribute     Barigui.Services.Account_FinancialGoals_NotAllowedNames
    ...     Id
    Log  ${nomeInvalido[0]['Values'][10]}
    Set Test Variable  ${nomeInvalido}      ${nomeInvalido[0]['Values'][10]}

chama_o_endpoint_de_validar_nome_cartao_enviando_um_invalido
    ${PARAMS}=      catenate       {\"name\": \"${nomeInvalido}\"}
	${HEADERS}      Create Dictionary       accept=application/json  User-Agent=08b86528-dd82-4982-b6a4-a5a2669864bf    Authorization=${token}      Content-Type=application/json-patch+json
    ${RESPOSTAREQUEST}     Post Request     newsession      account/v1/accounts/financialgoals/validatecardname		
	...		data=${PARAMS}        headers=${HEADERS}
    Log  ${RESPOSTAREQUEST.json()} 
    ${retorno}  Set Variable  ${RESPOSTAREQUEST.json()} 
    Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200
    Set Test Variable  ${retorno}

valida_que_o_nome_e_invalido
    Should Be True    '${retorno['isValid']}' == 'False'

#TERCEIRO TESTE

que_digita_as_informacoes_para_solicitar_o_cartao
    Set Test Variable       ${cpf}      09478206982 
    Set Test Variable       ${nomeCartao}      Teste QA 

	${token}  Gera Token  ${cpf}
	Set Test Variable  ${token}

    ${numConta}		${numAgen}		Consultar Numero Da Conta		${token}		${cpf}
    Set Test Variable  ${numConta}

    ${retorno}      Consulta Dynamo Three Attributes Two Int And Bool       
    ...  Barigui.Services.Account_FinancialGoals     AccountId		${numConta}      Type       2
    ...  Active		true
    Log  ${retorno}
    Set Test Variable        ${idSubconta}      ${retorno[0]['Id']}

chama_o_endpoint_de_solicitar_o_cartao
    ${PARAMS}       Create Dictionary       cardPrintedName=${nomeCartao}
	${HEADERS}      Create Dictionary       accept=application/json  Content-Type=application/json  User-Agent=cd8fc073-7ffc-4387-9c17-6b94146ddd5f    Authorization=${token}
    ${RESPOSTAREQUEST}     Post Request     newsession      account/v1/accounts/${numConta}/subaccounts/${idSubconta}/cards		data=${PARAMS}        headers=${HEADERS}
    Set Test Variable		${retornoResposta}		${RESPOSTAREQUEST.json()}
	Log           ${RESPOSTAREQUEST.text}
	Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200

valida_que_a_solicitacao_esta_na_tabela_do_banco
    ${retorno}     Consulta Dynamo Int   
	...  Barigui.Services.Account_FinancialGoals_CardRequests		FinancialGoalId		${idSubconta}
	Log  ${retorno}

    Should Be True    '${retorno[0]['Id']}' == '${retorno[0]['Id']}'
    Should Be True    '${retorno[0]['CardPrintedName']}' == '${nomeCartao}'
    Should Be True    '${retorno[0]['Status']}' == 'PENDING'
    Should Be True    '${retorno[0]['Active']}' == 'True'
