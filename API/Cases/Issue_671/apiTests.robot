*** Settings ***
Documentation
...  As cliente de uma conta e subconta meta ativa no Banco Bari
...  Want visualizar os rendimentos dos meus depósitos
...  So ver o detalhe das aplicações
Default Tags	automacao       Sprint34
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***

Visualizando o rendimento total da minha subconta meta
	[Tags]	backend	regression
	Given endpoint_para_trazer_os_detalhes_de_uma_subconta
	When realizado_o_get_request_para_ver_os_detalhes
	Then ira_trazer_o_rendimento_detalhado_daquele_subconta

*** Keywords ***

endpoint_para_trazer_os_detalhes_de_uma_subconta
    Set Test Variable       ${cpf}      83886710904
	${token}  Gera Token  ${cpf}
	Set Test Variable  ${token}
	${numeroConta}  ${agencia}  Consultar Numero Da Conta  ${token}  ${cpf}
	Set Test Variable  ${numeroConta}
    Set Test Variable  ${agencia}
    ${retorno}      Consulta Dynamo Two Attributes Int And Bool    
	...  Barigui.Services.Account_FinancialGoals		AccountId		${numeroConta}
	...  Active		true
	Log  ${retorno}
	${idSubconta}=		Convert To String		${retorno[1]['Id']}
	Set Test Variable  ${idSubconta}

realizado_o_get_request_para_ver_os_detalhes
    Set Test Variable       @{nomeSubconta}       @{EMPTY}
    Set Test Variable       @{numeroSubconta}       @{EMPTY}
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=280422cf-25b9-4328-8a4c-795c8fc51c3f    Authorization=${token}
    ${RESPOSTAREQUEST}     Get Request     newsession      account/v1/accounts/${numeroConta}/financialgoals/${idSubconta}      headers=${HEADERS}
    Set Test Variable       ${resultadoGet}     ${RESPOSTAREQUEST.json()}
    Log           ${resultadoGet}
    

ira_trazer_o_rendimento_detalhado_daquele_subconta
    Should Be True    ${resultadoGet['investmentBalance']} == ${resultadoGet['investmentBalance']}
    Should Be True    ${resultadoGet['incomeNetBalance']} == ${resultadoGet['incomeNetBalance']}
    Should Be True    ${resultadoGet['investmentTotalBalance']} == ${resultadoGet['investmentTotalBalance']}
    Should Be True    ${resultadoGet['incomeTotalBalance']} == ${resultadoGet['incomeTotalBalance']}
    Should Be True    ${resultadoGet['incomeTax']} == ${resultadoGet['incomeTax']}
    Should Be True    ${resultadoGet['iof']} == ${resultadoGet['iof']}