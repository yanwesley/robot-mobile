*** Settings ***
Documentation
...    As cliente da conta e subconta do banco Bari
...    Want depositar um valor em minha subconta
Default Tags	automacao       Sprint30
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***

Usuario deposita na sua subconta
	[Tags]	backend	regression
	Given o_usuario_tenha_uma_conta_criada_na_subconta_meta
	And possui_saldo_na_conta_principal
	When chamar_o_endpoint_de_deposito_na_subconta
	Then valida_no_endpoint_de_saldo_se_foi_transferido
	And se_no_endpoint_de_saldo_na_suconta_recebeu_o_valor

Usuario deposita na sua subconta sem saldo
	[Tags]	backend	regression
	Given usuario_tenha_uma_conta_criada_na_subconta_meta
	And nao_possui_saldo_na_conta_principal
	When chama_o_endpoint_de_deposito_na_subconta
	Then valida_se_deu_erro
	
*** Keywords ***

o_usuario_tenha_uma_conta_criada_na_subconta_meta
	Set Test Variable       ${cpf}      09478206982
	${token}  Gera Token  ${cpf}
	Set Test Variable  ${token}

	${numConta}  ${agencia}  consultar numero da conta  ${token}  ${cpf}
    Set Test Variable  ${numConta}
    Set Test Variable  ${agencia}

	${valorTrans}	Convert To Integer      100
    Set Test Variable       ${valorTrans}
	
possui_saldo_na_conta_principal
	${saldoAntes}  consultar saldo  ${token}  ${cpf}
	#New Token DynamoDB
	${retorno}      Consulta Dynamo Two Attributes Int And Bool    
	...  Barigui.Services.Account_FinancialGoals		AccountId		${numConta}
	...  Active		true
	Log  ${retorno}
	${idSubconta}=		Convert To String		${retorno[0]['Id']}
	${saldoSubAntes}  Consultar Saldo Subconta  ${token}  ${numConta}  ${idSubconta}
	Set Test Variable		${idSubconta}
	Set Test Variable		${saldoAntes}
	Set Test Variable		${saldoSubAntes}		${saldoSubAntes['investmentBalance']}
	${idSubcontaInt}	Convert To Integer      ${idSubconta}
    Set Test Variable       ${idSubcontaInt}

chamar_o_endpoint_de_deposito_na_subconta
	${PARAMS}       Create Dictionary       financialGoalId=${idSubcontaInt}  value=${valorTrans}
	${HEADERS}      Create Dictionary       accept=application/json  User-Agent=cd8fc073-7ffc-4387-9c17-6b94146ddd5f    Authorization=${token}
	...		Content-Type=application/json-patch+json  
    ${RESPOSTAREQUEST}     Post Request     newsession      account/v1/accounts/${numConta}/financialgoals/deposit		data=${PARAMS}        headers=${HEADERS}
    ${retornoResposta}  Set Variable  ${RESPOSTAREQUEST.json()} 
	Log           ${RESPOSTAREQUEST.text}
	Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200

valida_no_endpoint_de_saldo_se_foi_transferido
	${saldoDepois}		consultar saldo		${token}     ${cpf}
	Set Test Variable  ${saldoDepois}
	Should Be True    ${saldoDepois} == ${saldoAntes}-${valorTrans}

se_no_endpoint_de_saldo_na_suconta_recebeu_o_valor
	${saldoSubDepois}  Consultar Saldo Subconta  ${token}  ${numConta}  ${idSubconta}
	${saldoSubDepois}		Set Variable		${saldoSubDepois['investmentBalance']}
	Should Be True    ${saldoSubDepois} == ${saldoSubAntes}+${valorTrans}

usuario_tenha_uma_conta_criada_na_subconta_meta
	Set Test Variable       ${cpf}      09478206982
	${token}  Gera Token  ${cpf}
	Set Test Variable  ${token}

	${numConta}  ${agencia}  consultar numero da conta  ${token}  ${cpf}
    Set Test Variable  ${numConta}
    Set Test Variable  ${agencia}
	
	#Set Test Variable		${numConta}		19503
	${valorTrans}	Convert To Integer      1000000000
    Set Test Variable       ${valorTrans}

nao_possui_saldo_na_conta_principal
	${saldo}  consultar saldo  ${token}  ${cpf}
	#New Token DynamoDB
	${retorno}      Consulta Dynamo Int     Barigui.Services.Account_FinancialGoals        AccountId      19503
	${idSubconta}=		Convert To Integer		${retorno[0]['Id']}
	Should Be True    ${saldo} < ${valorTrans}
	Set Test Variable		${idSubconta}
	Set Test Variable		${saldo}

chama_o_endpoint_de_deposito_na_subconta
	${PARAMS}       Create Dictionary       financialGoalId=${idSubconta}  value=${valorTrans}
	${HEADERS}      Create Dictionary       accept=application/json  User-Agent=cd8fc073-7ffc-4387-9c17-6b94146ddd5f    Authorization=${token}
	...		Content-Type=application/json-patch+json  
    ${RESPOSTAREQUEST}     Post Request     newsession      account/v1/accounts/${numConta}/financialgoals/deposit		data=${PARAMS}        headers=${HEADERS}
    Set Test Variable		${retornoResposta}		${RESPOSTAREQUEST.json()}
	Log           ${RESPOSTAREQUEST.text}

valida_se_deu_erro
	Should Be True    '${retornoResposta['notifications'][0]['message']}'=='Erro ao consultar Subcontas: Error posting to Lydians REST API: Operação cancelada por insuficiência de saldo na conta principal! '
	Should Be True    '${retornoResposta['notifications'][1]['message']}'=='Depósito não efetuado'