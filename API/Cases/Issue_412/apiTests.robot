*** Settings ***
Documentation
...    Como cliente do banco Bari
Default Tags	automacao		Sprint28
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***

Usuario solicita o cancelamento da conta
	[Tags]	backend	regression	automation
	Given que_o_usuario_socilitou_o_cancelamento_da_conta
	When chamar_a_api_de_alteracao_de_status_da_conta
	Then altera_o_status
	And foi_enviado_o_email

Solicitacao de status do cliente via API
	[Tags]	backend	regression	automation
	Given cancelamento_ja_foi_solicitado
	When chamar_o_endpoint_para_alterar_status_da_conta
	Then valida_se_status_esta_igual_ao_do_banco

*** Keywords ***

que_o_usuario_socilitou_o_cancelamento_da_conta
    Set Test Variable       ${cpf}      09478206982
    ${token}  Gera Token  ${cpf}
    ${numeroConta}  ${agencia}  consultar numero da conta  ${token}  ${cpf}
    Set Test Variable  ${numeroConta}
    Set Test Variable  ${token}
    Set Test Variable  ${numeroConta}
    Alterar status da conta   ${token}  ${numeroConta}  1  ACTIVE

chamar_a_api_de_alteracao_de_status_da_conta
    #${PARAMS}       Create Dictionary       checkingAccountStatus=2       accountId=16806     status=CLOSUREREQUESTED
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=101fe16c-14bb-4c12-bf2b-01a805dd89fb    Authorization=${token}  
    ${RESPOSTAREQUEST}     Delete Request     newsession      /account/v1/accounts/${cpf}        headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text} 

altera_o_status
    New Token DynamoDB
    Valida status  ${cpf}

foi_enviado_o_email
    Log     Será validado email via front

#OUTRO TESTE
cancelamento_ja_foi_solicitado
    Set Test Variable       ${cpf}      09478206982
    ${token}  Gera Token  ${cpf}
    ${numeroConta}  ${agencia}  consultar numero da conta  ${token}  ${cpf}
    Set Test Variable  ${numeroConta}
    Set Test Variable  ${token}
    Set Test Variable  ${agencia}

chamar_o_endpoint_para_alterar_status_da_conta
    Alterar status da conta   ${token}  ${numeroConta}  2  CLOSUREREQUESTED

valida_se_status_esta_igual_ao_do_banco
    New Token DynamoDB
    Valida status  ${cpf}

#Usado nos dois testes

Alterar status da conta 
    [Arguments]  ${token}  ${numeroConta}  ${numeroStatus}  ${nomeStatus}
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=75c5502d-f430-4e93-93dc-17231d4aca25    Authorization=${token}  
    ${RESPOSTAREQUEST}     Put Request     newsession      /account/v1/accounts/status/${numeroStatus}?accountId=${numeroConta}&status=${nomeStatus}        headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text} 

Valida status
    [Documentation]  Status cancelado sempre será 2
    [Arguments]     ${cpf}
    ${retorno}      Consulta Dynamo     Barigui.Services.Account_CheckingAccount        CustomerId      ${cpf}
    ${retorno}  Set Variable  ${retorno[0]}
    Log     ${cpf}
    Should Be True    2 == ${retorno['Status']}

