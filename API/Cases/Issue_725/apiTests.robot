*** Settings ***
Documentation
...  As cliente com conta e subconta Bari
...  Want alterar data e valor da subconta meta
Default Tags	automacao       Sprint33
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***

Excluindo um contato de transferencia
	[Tags]	backend	regression
	Given endpoint_para_excluir_um_contato_de_transferencia
	When realizado_o_delete_request_para_excluir_o_contato
	Then sera_excluido_o_contato_nos_favoritos

*** Keywords ***

endpoint_para_excluir_um_contato_de_transferencia
    Set Test Variable       ${cpf}      09478206982
    ${token}     Gera token       ${cpf}  
    Set Test Variable  ${token}
    Consultar Contatos Transferencia  ${token}  ${cpf}
    Log  ${nomeContato[0]}
    Log  ${idContato[0]}	 

realizado_o_delete_request_para_excluir_o_contato
    #${PARAMS}       Create Dictionary       customerId=${cpf}        id=${idContato[0]}
	${HEADERS}      Create Dictionary       accept=application/json  User-Agent=901f78d0-062a-4ec9-b232-63629b8baad9    Authorization=${token}
    ${RESPOSTAREQUEST}     Delete Request     newsession      account/v1/accounts/${cpf}/contacts/${idContato[0]}	
	...        headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text}
    
    Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		202

sera_excluido_o_contato_nos_favoritos
    ${retorno}      Consulta Dynamo     Barigui.Services.Account_RelatedAccounts        RelatedAccountId        ${idContato[0]}
    Log  ${retorno}
    Should Be True    '${nomeContato[0]}' == '${retorno[0]['Name']}'