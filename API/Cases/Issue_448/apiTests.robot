*** Settings ***
Documentation
...    Como cliente do banco Bari
Default Tags	automacao       Sprint29
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***

Abrir uma subconta objetivo meta
	[Tags]	backend	regression
	Given endpoint_para_criacao_de_subconta_objetivo_meta
	When realizado_o_post_request_com_dados_do_usuario_com_conta_ativa_no_banco_bari
	Then sera_criado_um_registro_no_banco_referente_a_criacao_da_subconta_objetivo_meta

Pesquisar todas as subcontas objetivos metas de um usuario
	[Tags]	backend	regression
	Given endpoint_para_buscar_as_subcontas_objetivos_metas_de_um_usuario
	When realizado_o_get_request_com_dados_da_conta_ativa_do_usuario
	Then existira_um_registros_no_banco_referente_a_cada_subconta_do_usuario

*** Keywords ***

endpoint_para_criacao_de_subconta_objetivo_meta
    Set Test Variable       ${cpf}      09478206982
    ${token}        Gera token       ${cpf} 
	${numeroConta}  ${agencia}  consultar numero da conta  ${token}  ${cpf}
	Set Test Variable  ${numeroConta}
    Set Test Variable  ${agencia}

realizado_o_post_request_com_dados_do_usuario_com_conta_ativa_no_banco_bari
    ${PARAMS}       Create Dictionary       name=Aposentaria2063  image=string  goal=0  deadLine=2020-03-05T21:16:59.450Z  type=SAVING_ACCOUNT
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=d25cf88d-468c-45b1-b529-8958f2793207    Authorization=${token}        Content-Type=application/json-patch+json
    ${RESPOSTAREQUEST}     Post Request     newsession      account/v1/accounts/${numeroConta}/financialgoals       data=${PARAMS}      headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text} 
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()} 
    ${nomeSubconta}  Catenate      ${json_value['name']}
    ${numeroSubconta}  Catenate      ${json_value['id']}
    Set Test Variable  ${nomeSubconta}
    Set Test Variable  ${numeroSubconta}

sera_criado_um_registro_no_banco_referente_a_criacao_da_subconta_objetivo_meta
    New Token DynamoDB
    ${retorno}      Consulta Dynamo Int     Barigui.Services.Account_FinancialGoals        Id      ${numeroSubconta}
    log  ${retorno}
    Should Be True    '${nomeSubconta}' == '${retorno[0]['Name']}'

#OUTRO TESTE

endpoint_para_buscar_as_subcontas_objetivos_metas_de_um_usuario
    Set Test Variable       ${cpf}      09478206982
    ${token}        Gera token       ${cpf} 
	${numeroConta}  ${agencia}  consultar numero da conta  ${token}  ${cpf}
	Set Test Variable  ${numeroConta}
    Set Test Variable  ${agencia}

realizado_o_get_request_com_dados_da_conta_ativa_do_usuario
    consultar subconta de um usuario        ${token}     ${cpf}     ${numeroConta}   

existira_um_registros_no_banco_referente_a_cada_subconta_do_usuario   
    ${length} =     Get Length      ${numeroSubconta}
    ${auxNum}       Set Variable      ${numeroSubconta}
    ${auxNome}       Set Variable      ${nomeSubconta}
    :FOR        ${index}      IN RANGE        ${length}
    \   ${retorno}      Consulta Dynamo Two Attributes Int      Barigui.Services.Account_FinancialGoals        Id        ${auxNum[${index}]}        AccountId  ${numeroConta}
    \   log  ${retorno}
    \   Should Be True    '${auxNome[${index}]}' == '${retorno[0]['Name']}'