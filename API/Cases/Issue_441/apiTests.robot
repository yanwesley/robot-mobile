*** Settings ***
Documentation
...  As usuário ativo no APP do Banco Bari,
...  Want cria uma subconta objetivo cartão
Default Tags	automacao       Sprint34
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***

Abrir uma subconta objetivo cartao
	[Tags]	backend	regression
	Given endpoint_para_abrir_uma_subconta
	When realizado_o_put_request_para_criar_a_subconta_cartao
	Then ira_retornar_a_subconta_cartao_aberta

*** Keywords ***

endpoint_para_abrir_uma_subconta
    Set Test Variable       ${cpf}      09478206982
    ${token}        Gera token       ${cpf} 
	Set Test Variable  ${token}
	${numeroConta}  ${agencia}  Consultar Numero Da Conta  ${token}  ${cpf}
	Set Test Variable  ${numeroConta}
    Set Test Variable  ${agencia}

realizado_o_put_request_para_criar_a_subconta_cartao
	${PARAMS}       Create Dictionary       name=Aposentaria2063  image=string  goal=0  deadLine=2020-03-05T21:16:59.450Z  type=2
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=d25cf88d-468c-45b1-b529-8958f2793207    Authorization=${token}        Content-Type=application/json-patch+json
    ${RESPOSTAREQUEST}     Post Request     newsession      account/v1/accounts/${numeroConta}/financialgoals       data=${PARAMS}      headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text} 
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()} 
    ${nomeSubCartao}  Catenate      ${json_value['name']}
    ${numeroSubCartao}  Catenate      ${json_value['id']}
    Set Test Variable  ${nomeSubCartao}
    Set Test Variable  ${numeroSubCartao}

ira_retornar_a_subconta_cartao_aberta
    New Token DynamoDB
	${retorno}      Consulta Dynamo Int     Barigui.Services.Account_FinancialGoals        Id      ${numeroSubCartao}
    log  ${retorno}
    Should Be True    '${nomeSubCartao}' == '${retorno[0]['Name']}'