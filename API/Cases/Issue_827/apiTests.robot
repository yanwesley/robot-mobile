*** Settings ***
Documentation
...  As cliente do Banco Bari com uma conta Ativa
...  Want fazer pagamento de boleto inserindo o código do boleto
Default Tags	automacao       Sprint36
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***

Dados do boleto
	[Tags]	backend	regression
	Given endpoint_para_receber_os_dados_do_boleto
	When realizado_o_post_request_com_o_numero_do_boleto
	Then retornara_os_dados_do_boleto

*** Keywords ***

endpoint_para_receber_os_dados_do_boleto
    Set Test Variable       ${cpf}      09478206982
    #Set Test Variable       ${numBoleto}      858500000266975203281836250720183161065089722510
    Set Test Variable       ${numBoleto}       42297030060000269528643818598625883460000000759

	${token}  Gera Token  ${cpf}
	Set Test Variable  ${token}

realizado_o_post_request_com_o_numero_do_boleto
    ${json_value}   Verificar Dados Do Boleto       ${token}      ${numBoleto} 
    Set Test Variable       ${json_value}   

retornara_os_dados_do_boleto
    # So valida se o campo existe pois tem que mudar o boleto conforme a data de vencimento dele
    
    Should Be True    '${json_value['id']}' == '${json_value['id']}'
    Should Be True    '${json_value['transactionId']}' == '${json_value['transactionId']}'
    Should Be True    '${json_value['assignor']}' == '${json_value['assignor']}'
    Should Be True    '${json_value['dueDate']}' == '${json_value['dueDate']}'
    Should Be True    '${json_value['endHour']}' == '${json_value['endHour']}'
    Should Be True    '${json_value['initHour']}' == '${json_value['initHour']}'
    Should Be True    '${json_value['digitable']}' == '${numBoleto}'
    Should Be True    '${json_value['allowChangeValue']}' == '${json_value['allowChangeValue']}'
    Should Be True    '${json_value['recipient']}' == '${json_value['recipient']}'
    Should Be True    '${json_value['payer']}' == '${json_value['payer']}'
    Should Be True    '${json_value['valueInCents']}' == '${json_value['valueInCents']}'
    Should Be True    '${json_value['maxValueInCents']}' == '${json_value['maxValueInCents']}'
    Should Be True    '${json_value['minValueInCents']}' == '${json_value['minValueInCents']}'
    Should Be True    '${json_value['discountValueInCents']}' == '${json_value['discountValueInCents']}'
    Should Be True    '${json_value['interestValueCalculatedInCents']}' == '${json_value['interestValueCalculatedInCents']}'
    Should Be True    '${json_value['fineValueCalculatedInCents']}' == '${json_value['fineValueCalculatedInCents']}'
    Should Be True    '${json_value['totalUpdatedInCents']}' == '${json_value['totalUpdatedInCents']}'
    Should Be True    '${json_value['totalWithDiscountInCents']}' == '${json_value['totalWithDiscountInCents']}'
    Should Be True    '${json_value['totalWithAdditionalInCents']}' == '${json_value['totalWithAdditionalInCents']}'
    Should Be True    '${json_value['paymentDate']}' == '${json_value['paymentDate']}'
    Should Be True    '${json_value['allowedTime']}' == '${json_value['allowedTime']}'


