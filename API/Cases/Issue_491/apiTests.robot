*** Settings ***
Documentation
...  As cliente com conta e subconta Bari
...  Want definir meta na minha subconta
Default Tags	automacao       Sprint33
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.qa.bancobari.com.br

*** Test Cases ***

Alterar uma subconta para definir meta
	[Tags]	backend	regression
	Given endpoint_para_alterar_uma_subconta
	When realizado_o_put_request_com_a_meta_e_prazo
	Then sera_adicionado_esses_dados_ao_registro

*** Keywords ***

endpoint_para_alterar_uma_subconta
    Set Test Variable       ${cpf}      09478206982
    #Set Test Variable       ${valorMeta}       10000
    ${valorMeta}	Convert To Integer      10000
    Set Test Variable       ${valorMeta}
    Set Test Variable       ${prazoMeta}       2020-11-20
    ${token}     Gera token       ${cpf}  
    Set Test Variable  ${token}
	${numeroConta}  ${agencia}  consultar numero da conta  ${token}  ${cpf}
    Set Test Variable  ${numeroConta}
    Set Test Variable  ${agencia}

realizado_o_put_request_com_a_meta_e_prazo
    consultar subconta de um usuario        ${token}     ${cpf}     ${numeroConta}
    ${auxNum}       Set Variable      ${numeroSubconta}
    ${auxNomeSub}       Set Variable      ${nomeSubconta}
    ${PARAMS}       Create Dictionary       name=${auxNomeSub[1]}       image=CAR      goal=${valorMeta}       deadLine=${prazoMeta}
    ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=83334e79-ba7b-47a3-8188-2f7b0b8dcb0a    Authorization=${token}        Content-Type=application/json-patch+json  
    ${RESPOSTAREQUEST}     Put Request     newsession      account/v1/accounts/${numeroConta}/financialgoals/${auxNum[1]}       data=${PARAMS}      headers=${HEADERS}
    Log           ${RESPOSTAREQUEST.text} 
    Should Be Equal As Strings		${RESPOSTAREQUEST.status_code}		200
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()} 
    ${valor}  Catenate      ${json_value['goal']}
    ${prazo}  Catenate      ${json_value['deadLine']}
    Set Test Variable  ${subcontaEscolhida}     ${auxNum[1]} 
    Set Test Variable        ${valor}
    Set Test Variable        ${prazo}

sera_adicionado_esses_dados_ao_registro
    ${retorno}      Consulta Dynamo Int     Barigui.Services.Account_FinancialGoals     Id       ${subcontaEscolhida}
    Log  ${retorno}
    ${retornoValor}  Set Variable  ${retorno[0]['Goal']}
    ${retornoPrazo}  Set Variable  ${retorno[0]['DeadLine']}
    Should Be True    '${valor}' == '${retornoValor}'
    Should Be True    '${prazo}' == '${retornoPrazo}'