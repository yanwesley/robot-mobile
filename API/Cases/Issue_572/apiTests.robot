*** Settings ***
Documentation
...  As cliente de uma conta e subconta meta ativa no Banco Bari
...  Want visualizar minhas últimas movimentações
Default Tags	automacao       Sprint32
Resource	${EXECDIR}/Resources/main.resource
Suite Setup         Abrir conexao		https://api.stage.bancobari.com.br

*** Test Cases ***

Visualiza os ultimos 90 lancamentos
	[Tags]	backend	regression	automation
	Given que_digita_os_dados_necessarios_para_a_consulta
	When chama_o_endpoint_da_subconta_que_mostra_o_extrato
	Then valida_se_os_dados_do_retorno_estao_conforme_esperado

*** Keywords ***

que_digita_os_dados_necessarios_para_a_consulta
    Set Test Variable       ${cpf}      09478206982
	Set Test Variable		${valorTrans}		100
	Set Test Variable		${qntdExtrato}		90 

	${token}  Gera Token  ${cpf}
	Set Test Variable  ${token}

	${numConta}		${numAgen}		Consultar Numero Da Conta		${token}		${cpf}
    Set Test Variable  ${numConta}
	
    # ${retorno}      Consulta Dynamo Two Attributes Int And Bool    
	# ...  Barigui.Services.Account_FinancialGoals		AccountId		${numConta}
	# ...  Active		true
	# Log  ${retorno}
	${retorno}      Consulta Dynamo Three Attributes Two Int And Bool       
    ...  Barigui.Services.Account_FinancialGoals     AccountId		${numConta}      Type       1
    ...  Active		true
    Log  ${retorno}
	${idSubconta}=		Convert To String		${retorno[0]['Id']}
	Set Test Variable  ${idSubconta}

chama_o_endpoint_da_subconta_que_mostra_o_extrato
	${retornoExtrato}		Consulta Extrato Subconta		${token}        ${numConta}     ${idSubconta}      ${qntdExtrato}

valida_se_os_dados_do_retorno_estao_conforme_esperado
    Should Be True    '${retornoExtrato['itemsPerPage']}'=='90'
	${ultimoExt}  Convert To Integer  ${retornoExtrato['totalItems']}
	Log  ${ultimoExt}
	#Esse é para validar o primeiro extrato
	#Esses passos são apenas para validar se os itens existem no retorno
	Should Be True    '${retornoExtrato['items'][0]['type']}'=='${retornoExtrato['items'][0]['type']}'
	Should Be True    '${retornoExtrato['items'][0]['item']['value']}'=='${retornoExtrato['items'][0]['item']['value']}'
	Should Be True    '${retornoExtrato['items'][0]['item']['category']}'=='${retornoExtrato['items'][0]['item']['category']}'
	Should Be True    '${retornoExtrato['items'][0]['item']['description']}'=='${retornoExtrato['items'][0]['item']['description']}'
	Should Be True    '${retornoExtrato['items'][0]['item']['investment']['title']}'=='${retornoExtrato['items'][0]['item']['investment']['title']}'
	Should Be True    '${retornoExtrato['items'][0]['item']['investment']['value']}'=='${retornoExtrato['items'][0]['item']['investment']['value']}'
	#Esse é para validar o último extrato
	Should Be True    '${retornoExtrato['items'][${ultimoExt}-1]['type']}'=='${retornoExtrato['items'][${ultimoExt}-1]['type']}'
	Should Be True    '${retornoExtrato['items'][${ultimoExt}-1]['item']['value']}'=='${retornoExtrato['items'][${ultimoExt}-1]['item']['value']}'
	Should Be True    '${retornoExtrato['items'][${ultimoExt}-1]['item']['category']}'=='${retornoExtrato['items'][${ultimoExt}-1]['item']['category']}'
	Should Be True    '${retornoExtrato['items'][${ultimoExt}-1]['item']['description']}'=='${retornoExtrato['items'][${ultimoExt}-1]['item']['description']}'
	Should Be True    '${retornoExtrato['items'][${ultimoExt}-1]['item']['investment']['title']}'=='${retornoExtrato['items'][${ultimoExt}-1]['item']['investment']['title']}'
	Should Be True    '${retornoExtrato['items'][${ultimoExt}-1]['item']['investment']['value']}'=='${retornoExtrato['items'][${ultimoExt}-1]['item']['investment']['value']}'