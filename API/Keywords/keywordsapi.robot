*** Keywords ***

# #276
# que_eu_defino_o_endpoint_para_consulta_de_cpf
# 	[Arguments]	${endpoint}
#     Set Test Variable  ${endpoint}

# #276
# eu_defino_o_request_header
#     Create Dictionary    content-type=application/json

# #276
# faco_uma_chamada_get
#     [Arguments]     ${cpf}
#     ${RESPOSTAREQUEST}     Get Request     customer     /v1/customers/${cpf}/checkUserInvited
#     Log                           ${RESPOSTAREQUEST.text}
#     Set Test Variable             ${RESPOSTAREQUEST}

# #276
# o_usuario_atingiu_o_limite_de_tentativas_de_login_com_o_cpf
# 	[Arguments]     @{cpf}

#     ${validarDesc}       Set Variable        Bearer The username and password do not match

#     :FOR  ${index}  IN  @{cpf}
# 	\	Gera Token  ${index}
#     \   ${PARAMS}       Create Dictionary       username=${index}  password=123Abd  grant_type=password  client_id=mobile  client_secret=mH8341Kop  content-Type=text/plain
#     \   ${HEADERS}      Create Dictionary       content-type=application/x-www-form-urlencoded  
#     \   ${RESPOSTAREQUEST}     Post Request     newsession      auth/connect/token      data=${PARAMS}      headers=${HEADERS}
#     \   ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}    
#     \   ${descricao}  Catenate  Bearer  ${json_value['error_description']}
#     \   Should Be True    '${descricao}' == '${validarDesc}'

# #276
# recebo_a_resposta_referente_ao_cpf
#     [Arguments]     ${resposta}
#     Log             ${resposta}
#     Dictionary Should Contain Item    ${RESPOSTAREQUEST.json()}    status             ${resposta}

# #276
# sera_recebido_a_confirmacao_do_usuario_na_black_list
#     [Arguments]     @{cpf}
#     #Consulta Dynamo     Barigui.Services.IdentityServer_LoginTries     

# #276
# sera_marcado_como_inativo_o_registro_no_banco_referente_a_subconta_objetivo_meta_escolhida
#     [Arguments]     ${numeroSubconta}
#     ${numeroSubconta}=      Convert To Number  ${numeroSubconta}  
#     ${retorno}      Consulta Dynamo     Barigui.Services.Account_FinancialGoals        CustomerId      ${numeroSubconta}
#     ${retorno}  Set Variable  ${retorno[0]}
#     # Contas inativas sempre terão número ...
#     Should Be True    0 == ${retorno['AgencyId']}

# #412
# cancelamento_ja_foi_solicitado
#     Set Test Variable       ${cpf}      09478206982
#     ${token}  Gera Token  ${cpf}
#     ${numeroConta}  ${agencia}  consultar numero da conta  ${token}  ${cpf}
#     Set Test Variable  ${numeroConta}
#     Set Test Variable  ${token}
#     Set Test Variable  ${agencia}

# #412
# chamar_o_endpoint_para_alterar_status_da_conta
#     Alterar status da conta   ${token}  ${numeroConta}  2  CLOSUREREQUESTED

# #412
# valida_se_status_esta_igual_ao_do_banco
#     Valida status  ${cpf}

# #412
# que_o_usuario_socilitou_o_cancelamento_da_conta
#     Set Test Variable       ${cpf}      09478206982
#     ${token}  Gera Token  ${cpf}
#     ${numeroConta}  ${agencia}  consultar numero da conta  ${token}  ${cpf}
#     Set Test Variable  ${numeroConta}
#     Set Test Variable  ${token}
#     Set Test Variable  ${numeroConta}
#     Alterar status da conta   ${token}  ${numeroConta}  1  ACTIVE

# #412
# chamar_a_api_de_alteracao_de_status_da_conta
#     #${PARAMS}       Create Dictionary       checkingAccountStatus=2       accountId=16806     status=CLOSUREREQUESTED
#     ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=101fe16c-14bb-4c12-bf2b-01a805dd89fb    Authorization=${token}  
#     ${RESPOSTAREQUEST}     Delete Request     newsession      /account/v1/accounts/${cpf}        headers=${HEADERS}
#     Log           ${RESPOSTAREQUEST.text} 

# #412
# altera_o_status
#     Valida status  ${cpf}

# #412
# foi_enviado_o_email
#     Log     Será validado email via front

# #437
# o_usuario_tem_uma_conta_ativa_no_banco_bari
#     ${token}    Gera Token  09478206982
#     Set Test Variable  ${token}

# #437
# consultar_no_endpoint_o_numero_da_conta
# 	Set Test Variable       ${cpf}      09478206982
# 	${numeroConta}  ${agencia}  consultar numero da conta  ${token}  ${cpf}
# 	Set Test Variable  ${numeroConta}
#     Set Test Variable  ${agencia}

# #437
# verificar_se_os_dados_estao_corretos
#     ${retorno}      Consulta Dynamo     Barigui.Services.Account_CheckingAccount        CustomerId      ${cpf}
#     ${retorno}  Set Variable  ${retorno[0]}
#     Should Be True    ${numeroConta} == ${retorno['Id']}
#     # POR ENQUANTO TODAS AS AGENCIAS SERÃO NÚM 0 CASO ISSO ALTERE SÓ USAR A VAR ${agencia}
#     Should Be True    0 == ${retorno['AgencyId']}

# #448
# endpoint_para_criacao_de_subconta_objetivo_meta
#     Set Test Variable       ${cpf}      09478206982
#     ${token}        Gera token       ${cpf} 
# 	${numeroConta}  ${agencia}  consultar numero da conta  ${token}  ${cpf}
# 	Set Test Variable  ${numeroConta}
#     Set Test Variable  ${agencia}

# #448
# realizado_o_post_request_com_dados_do_usuario_com_conta_ativa_no_banco_bari
#     ${PARAMS}       Create Dictionary       name=Aposentaria2063  image=string  goal=0  deadLine=2020-03-05T21:16:59.450Z  type=SAVING_ACCOUNT
#     ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=d25cf88d-468c-45b1-b529-8958f2793207    Authorization=${token}        Content-Type=application/json-patch+json
#     ${RESPOSTAREQUEST}     Post Request     newsession      account/v1/accounts/${numeroConta}/financialgoals       data=${PARAMS}      headers=${HEADERS}
#     Log           ${RESPOSTAREQUEST.text} 
#     ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()} 
#     ${nomeSubconta}  Catenate      ${json_value['name']}
#     ${numeroSubconta}  Catenate      ${json_value['id']}
#     Set Test Variable  ${nomeSubconta}
#     Set Test Variable  ${numeroSubconta}

# #448
# sera_criado_um_registro_no_banco_referente_a_criacao_da_subconta_objetivo_meta
#     ${retorno}      Consulta Dynamo Key     Barigui.Services.Account_FinancialGoals        Id      ${numeroSubconta}
#     log  ${retorno}
#     Should Be True    '${nomeSubconta}' == '${retorno[0]['Name']}'

# #448
# endpoint_para_buscar_as_subcontas_objetivos_metas_de_um_usuario
#     Set Test Variable       ${cpf}      09478206982
#     ${token}        Gera token       ${cpf} 
# 	${numeroConta}  ${agencia}  consultar numero da conta  ${token}  ${cpf}
# 	Set Test Variable  ${numeroConta}
#     Set Test Variable  ${agencia}

# #448
# realizado_o_get_request_com_dados_da_conta_ativa_do_usuario
#     consultar subconta de um usuario        ${token}     ${cpf}     ${numeroConta}    

# #448
# existira_um_registros_no_banco_referente_a_cada_subconta_do_usuario   
#     ${length} =     Get Length      ${numeroSubconta}
#     ${auxNum}       Set Variable      ${numeroSubconta}
#     ${auxNome}       Set Variable      ${nomeSubconta}
#     :FOR        ${index}      IN RANGE        ${length}
#     \   ${retorno}      Consulta Dynamo Key     Barigui.Services.Account_FinancialGoals        Id        ${auxNum[${index}]}
#     \   log  ${retorno}
#     \   Should Be True    '${auxNome[${index}]}' == '${retorno[0]['Name']}'

# #444
# endpoint_para_alterar_uma_subconta_objetivo_meta
#     Set Test Variable       ${cpf}      09478206982
#     ${token}     Gera token       ${cpf}  
#     Set Test Variable  ${token}
# 	${numeroConta}  ${agencia}  consultar numero da conta  ${token}  ${cpf}
#     Set Test Variable  ${numeroConta}
#     Set Test Variable  ${agencia}

# #444
# realizado_o_put_request_com_as_informacoes_da_subconta_objetivo_meta
#     consultar subconta de um usuario        ${token}     ${cpf}     ${numeroConta}
#     ${auxNum}       Set Variable      ${numeroSubconta}
#     ${PARAMS}       Create Dictionary       name=Aposentadoria       image=CAR      goal=0       deadLine=2020-03-10T19:31:02.175Z
#     ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=83334e79-ba7b-47a3-8188-2f7b0b8dcb0a    Authorization=${token}        Content-Type=application/json-patch+json  
#     ${RESPOSTAREQUEST}     Put Request     newsession      account/v1/accounts/${numeroConta}/financialgoals/${auxNum[0]}       data=${PARAMS}      headers=${HEADERS}
#     Log           ${RESPOSTAREQUEST.text} 
#     ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()} 
#     ${nomeSubconta}  Catenate      ${json_value['name']}
#     ${imagemSubconta}  Catenate      ${json_value['image']}
#     Set Test Variable  ${subcontaEscolhida}     ${auxNum[0]} 
#     Set Test Variable        ${nomeSubconta}
#     Set Test Variable        ${imagemSubconta}

# #444
# sera_alterado_o_registro_no_banco_referente_a_subconta_objetivo_meta_escolhida
#     ${retorno}      Consulta Dynamo Int     Barigui.Services.Account_FinancialGoals     Id       ${subcontaEscolhida}
#     ${retornoNome}  Set Variable  ${retorno[0]['Name']}
#     ${retornoImagem}  Set Variable  ${retorno[0]['Image']}
#     Should Be True    '${nomeSubconta}' == '${retornoNome}'
#     Should Be True    '${imagemSubconta}' == '${retornoImagem}'

# #449
# endpoint_para_excluir_uma_subconta_objetivo_meta
#     Set Test Variable       ${cpf}      09478206982
#     ${token}     Gera token       ${cpf}  
#     Set Test Variable  ${token}
# 	${numeroConta}  ${agencia}  consultar numero da conta  ${token}  ${cpf}
#     Set Test Variable  ${numeroConta}
#     Set Test Variable  ${agencia}

# #449
# realizado_o_delete_request_com_o_id_da_subconta_objetivo_meta
#     consultar subconta de um usuario        ${token}     ${cpf}     ${numeroConta}
#     ${auxNum}       Set Variable      ${numeroSubconta}
#     ${HEADERS}      Create Dictionary       accept=application/json  User-Agent=a5b12449-5783-4134-96b9-1db3a0fdf9b5    Authorization=${token}        content-type=application/x-www-form-urlencoded  
#     ${RESPOSTAREQUEST}     Delete Request     newsession      account/v1/accounts/${numeroConta}/financialgoals/${auxNum[0]}      headers=${HEADERS}
#     Log           ${RESPOSTAREQUEST.text} 
#     Log           ${auxNum[0]}
#     Set Test Variable  ${subcontaEscolhida}     ${auxNum[0]} 

# #449
# sera_marcado_como_inativo_no_banco_referente_a_subconta_objetivo_meta_escolhida
#     ${retorno}      Consulta Dynamo Key     Barigui.Services.Account_FinancialGoals     Id       ${subcontaEscolhida}
#     ${statusAtivo}  Set Variable  ${retorno[0]['Active']}
#     Should Be True    'False' == '${statusAtivo}'