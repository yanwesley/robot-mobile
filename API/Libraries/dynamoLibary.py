import ast
import decimal
import json
import decimal
import boto3
from boto3 import resource
from boto3.dynamodb.conditions import Attr, Key
from botocore.config import Config
from datetime import datetime

def  retorna_data(stringData):
    date = str(datetime.strptime(stringData.strip(' \t\r\n'), '%b %d %Y'))
    data = date[0:10] 
    print(data)
    return  data

def help_converter(item, key):
    item[key] = json.loads(item[key])
    return item

def consulta_dynamo(nameTable,nameAttribute,value):
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=Attr(nameAttribute).contains(value))
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=Attr(nameAttribute).contains(value), ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])
    return item

def consulta_dynamo_int(nameTable,nameAttribute,value):
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=Attr(nameAttribute).eq(int(value)))
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=Attr(nameAttribute).eq(int(value)), ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])
    return item

def consulta_dynamo_bool(nameTable,nameAttribute,value):
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=Attr(nameAttribute).eq(bool(value)))
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=Attr(nameAttribute).eq(bool(value)), ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])
    return item
    
def consulta_dynamo_attribute(nameTable,nameAttribute):
    att = 'attribute_exists('+str(nameAttribute)+')'
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=att)
    #response = table.scan(AttributesToGet=[nameAttribute])
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=att, ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])
    return item

def consulta_dynamo_two_attributes(nameTable,nameAttribute1,value1,nameAttribute2,value2):
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=Attr(nameAttribute1).contains(value1) and Attr(nameAttribute2).contains(value2))
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=Attr(nameAttribute1).contains(value1) and Attr(nameAttribute2).contains(value2), ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])

    return item

def consulta_dynamo_two_attributes_int_and_bool(nameTable,nameAttribute1,value1,nameAttribute2,value2):
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=Attr(nameAttribute1).eq(int(value1)) & Attr(nameAttribute2).eq(bool(value2)))
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=Attr(nameAttribute1).eq(int(value1)) & Attr(nameAttribute2).eq(bool(value2)), ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])
    return item

def consulta_dynamo_two_attributes_int(nameTable,nameAttribute1,value1,nameAttribute2,value2):
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=Attr(nameAttribute1).eq(int(value1)) & Attr(nameAttribute2).eq(int(value2)))
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=Attr(nameAttribute1).eq(int(value1)) & Attr(nameAttribute2).eq(int(value2)), ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])
    return item

def consulta_dynamo_three_attributes_two_int_and_bool(nameTable,nameAttribute1,value1,nameAttribute2,value2,nameAttribute3,value3):
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=Attr(nameAttribute1).eq(int(value1)) & Attr(nameAttribute2).eq(int(value2)) & Attr(nameAttribute3).eq(bool(value3)))
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=Attr(nameAttribute1).eq(int(value1)) & Attr(nameAttribute2).eq(int(value2)) & Attr(nameAttribute3).eq(bool(value3)), ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])
    return item

def consulta_dynamo_two_attributes_string_and_bool(nameTable,nameAttribute1,value1,nameAttribute2,value2):
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=Attr(nameAttribute1).contains(value1) & Attr(nameAttribute2).eq(bool(value2)))
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=Attr(nameAttribute1).contains(value1) & Attr(nameAttribute2).eq(bool(value2)), ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])
    return item

def gera_guid():
    a = uuid.uuid4()
    return str(a)
