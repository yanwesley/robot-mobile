*** Keywords ***
ConfirmaTransferencia
    [Arguments]                 ${Contato}  ${Cpf}  ${Agencia}  ${Conta}  ${Banco}
    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  300
    Element Text Should Be  ${pageTransferencia.lblPergunta}  Defina um valor para transferir
    Capture Page Screenshot
    Clear Text          ${pageTransferencia.valor}
    Input Text          ${pageTransferencia.valor}  100
    Capture Page Screenshot
    Click Element       ${pageLogin.btncontinue}
    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  300
    Element Text Should Be  ${pageTransferencia.lblPergunta}  Escolha uma descrição, caso queira.
    Capture Page Screenshot
    Input Text          ${pageTransferencia.descricaoTransferencia}  Automação Bari
    Click Element       ${pageLogin.btncontinue}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    Wait Until Element Is Visible  ${pageLabelsTransferencia.lblBanco}  300
    Element Should Be Visible  ${pageLabelsTransferencia.lblBanco}
    Element Should Be Visible  ${pageLabelsTransferencia.lblAgencia}
    Element Should Be Visible  ${pageLabelsTransferencia.lblConta}
    Element Should Be Visible  ${pageLabelsTransferencia.lblFavorecido}
    Element Should Be Visible  ${pageLabelsTransferencia.lblCPF}
    Element Should Be Visible  ${pageLabelsTransferencia.lblTipoConta}
    Element Should Be Visible  ${pageLabelsTransferencia.lblData}
    Element Should Be Visible  ${pageLabelsTransferencia.lblDescricao}
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblBanco}  ${Banco}
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblAgencia}  ${Agencia}
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblConta}  	${Conta}
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblFavorecido}  ${Contato}
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblCPF} 	${Cpf}
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblTipoConta}  	Conta Corrente
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblDescricao}  Automação Bari
    Capture Page Screenshot
    Click Element       ${pageLogin.btncontinue}
    Wait Until Element Is Visible  ${pageFinalizaTransferencia.lblSenha}  300
    Input Text          ${pageLogin.psw}  1470
    Capture Page Screenshot
    Click Element       ${pageFinalizaTransferencia.btnConfirma}
    Wait Until Element Is Visible  ${pageFinalizaTransferencia.lblTransferenciaSucesso}  300
    Capture Page Screenshot
    Element Text Should Be  ${pageFinalizaTransferencia.lblvalor}  	R$ 1,00
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblFavorecido}  ${Contato}	
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblBanco}  ${Banco}
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblAgencia}  ${Agencia}
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblConta}  	${Conta}
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblDescricao}  Automação Bari
    Element Should Be Visible  ${pageFinalizaTransferencia.lblAutentica}
    Wait Until Element Is Visible  ${pageFinalizaTransferencia.btnFechar}  3


ListaContatos
    [Arguments]     ${cpf}

    ${token}  GeraToken  ${cpf}
    Log       ${token} 
    ${HEADERS}      Create Dictionary       accept=application/json   Content-Type=application/json  User-Agent=8d31a8d5-f4a5-429b-943f-04a6d15bda46    Authorization=${token}     
    Create Session    session    https://api.qa.bancobari.com.br/account
    ${RESPOSTA}     Get Request    session   v1/accounts/${cpf}/contacts   headers=${HEADERS}    
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200 
    ${json_response}=        Set Variable        ${RESPOSTA.json()}
    [Return]        ${json_response}


LimpaContatos
    [Arguments]     ${cpf}

    ${contatos}  ListaContatos  ${cpf}
    Log       ${contatos}  

    ${AccountId}   RetornaNumeroConta  ${cpf}

    ${token}  GeraToken  ${cpf}
    Log       ${token} 
    ${HEADERS}      Create Dictionary       accept=application/json   Content-Type=application/json  User-Agent=8d31a8d5-f4a5-429b-943f-04a6d15bda46    Authorization=${token}     
    Create Session    session    https://api.qa.bancobari.com.br/account

    ${length}    Get Length  ${contatos}
    FOR     ${i}  IN RANGE  ${length}
            ${contato_id}=   Set Variable  ${contatos[${i}]['id']}
            ${RESPOSTA}     Delete Request    session   v1/accounts/${cpf}/contacts/${contato_id}   headers=${HEADERS}    
            Should Be Equal As Strings      ${RESPOSTA.status_code}    202 
    END
    
BuscaDownload
    [Arguments]     ${nome_buscado}   ${sessionId}    ${operacao} 
    Create Session  newsession  http://localhost:4723/wd/hub/session/${sessionID}

    ${PARAMS}=  catenate
        ...     {
        ...        "using": "id", 
        ...        "value": "${pageFinalizaTransferencia.listaNotificacoes}"
        ...     }


    ${HEADERS}      Create Dictionary       accept=application/json   Content-Type=application/json
  
    ${RESPOSTA}     Post Request     newsession      elements   data=${PARAMS}   headers=${HEADERS}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    ${elementos}=    Set Variable    ${RESPOSTA.json()['value']}

    ${length}    Get Length  ${elementos}
    Log     ${length}
    FOR     ${i}  IN RANGE  ${length}
        ${element1}=    Set Variable    ${elementos[${i}]['ELEMENT']}
        
        ${status}   Run Keyword If     """${operacao}""" == """Buscar"""    VerificaNome    ${nome_buscado}   ${element1}   ${HEADERS}
        ...         ELSE   VerificaComprovante    ${nome_buscado}   ${HEADERS}  

        Run Keyword If    ${status}     Exit For Loop 
    END
    Log     ${status}
    [Return]        ${status}   ${element1}



VerificaNome
    [Arguments]     ${nome_buscado}   ${element1}   ${HEADERS}    

    ${RESPOSTA}     Get Request     newsession      element/${element1}/text   headers=${HEADERS}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    ${nome_item}=    Set Variable    ${RESPOSTA.json()['value']}
    ${status}    Run Keyword And Return Status    Should Contain   ${nome_item}   ${nome_buscado}

    [Return]        ${status}


VerificaComprovante
    [Arguments]     ${element1}   ${HEADERS}    

    ${RESPOSTA}     Post Request     newsession      element/${element1}/click   headers=${HEADERS}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    ${status} =    Set Variable      True

    [Return]        ${status}




