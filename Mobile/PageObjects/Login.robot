*** Settings ***
Resource                  ${EXECDIR}/Resources/PageObjects.resource

*** Keywords ***
Login
    [Arguments]  ${cpf}  ${senha}

    Wait Until Element Is Visible  ${pageLogin.btnAcessar}  300
    Click Element       ${pageLogin.btnAcessar}
    Wait Until Element Is Visible  ${pageLogin.cpf}  30
    Input Text          ${pageLogin.cpf}  ${cpf}
    Wait Until Element Is Visible  ${pageLogin.btncontinue}  300
    Click Element       ${pageLogin.btncontinue}
    Wait Until Element Is Visible  ${pageLogin.psw}  30
    Input Text          ${pageLogin.psw}  ${senha}
    Wait Until Element Is Visible  ${pageLogin.btncontinue}  300
    Click Element       ${pageLogin.btncontinue}
    Wait Until Element Is Visible  ${pageBiometria.btnAgoraNao}  300
    Click Element       ${pageBiometria.btnAgoraNao}  
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}  

OpenApp
    Open Application  remote_url=http://localhost:4723/wd/hub  udid=RQ8M509Z3TA  platformName=Android  deviceName=Galaxy A30  appPackage=br.com.bancobari.${ambiente}  appActivity=br.com.bancobari.OnboardingActivity

GeraToken
    [Arguments]     ${cpf}
    Create Session      newsession   https://api.qa.bancobari.com.br    verify=True
    ${PARAMS}       Create Dictionary       username=${cpf}  password=${senha}  grant_type=password  client_id=mobile  client_secret=mH8341Kop  content-Type=text/plain
    ${HEADERS}      Create Dictionary       content-type=application/x-www-form-urlencoded       
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      auth/connect/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}    
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}


RetornaNumeroConta
    [Arguments]     ${cpf}

    ${token}  GeraToken  ${cpf}
    Log       ${token} 
    ${HEADERS}      Create Dictionary       accept=application/json   Content-Type=application/json  User-Agent=8d31a8d5-f4a5-429b-943f-04a6d15bda46    Authorization=${token}     
    Create Session    session    https://api.qa.bancobari.com.br
    ${RESPOSTA}     Get Request    session   account/v1/accounts?customerId=${cpf}   headers=${HEADERS}    
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200 
    ${AccountId}=        Set Variable        ${RESPOSTA.json()["accountId"]}
    [Return]        ${AccountId}

