*** Keyword ***
transferencia_ct5  

    ${token}  GeraToken  09699387998
    ${Idempotency-Key}=   Set Variable    b98ada1d-bd97-424d-8676-65dae5b5a0d6
    ${Cookie}=            Set Variable    TS01f5a73e=01ff77ebbf5efdc6fb1b63d3d5ff193ff8e4e83498bb57c9b6c582fbac6b7a2a996e60ffe7a6799a83de9c237eb7a91c2e4d5ff947
    Log       ${token} 
    ${HEADERS}      Create Dictionary       accept=application/json   Content-Type=application/json  User-Agent=8d31a8d5-f4a5-429b-943f-04a6d15bda46    Authorization=${token}   Idempotency-Key=${Idempotency-Key}    Cookie=${Cookie}   
    
    #--------------------------------------------------------------------------------------
    # Get request 
    #-------------------------------------------------------------------------------------- 
    Create Session    session    https://api.qa.bancobari.com.br/account/v1/accounts
    Log       session 
    
    ${RESPOSTA}     Get Request    session  ?customerId=09699387998    headers=${HEADERS}  
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200 


    ${token}  GeraToken  09478206982
    ${Idempotency-Key}=   Set Variable    b98ada1d-bd97-424d-8676-65dae5b5a0a1
        Log       ${token} 
    ${HEADERS}      Create Dictionary       accept=application/json   Content-Type=application/json  User-Agent=8d31a8d5-f4a5-429b-943f-04a6d15bda46    Authorization=${token}   Idempotency-Key=${Idempotency-Key}      
    ${PARAMS}=  catenate
    ...     {
    ...        "description": "nvd",
    ...        "toAccount": 1,
    ...        "toAgency": 47108,
    ...        "toBank": 1,
    ...        "toDocument": "09478206982",
    ...        "toName": "teste",
    ...        "tpAccount": "cc",
    ...        "transactionDate": "2020-08-31",
    ...        "value": 100,
    ...     }

    #--------------------------------------------------------------------------------------
    # Get request 
    #-------------------------------------------------------------------------------------- 
    Create Session    session    https://api.qa.bancobari.com.br/account/v1/accounts
    Log       session 
    
    ${RESPOSTA}     Post Request    session  ?customerId=09699387998    headers=${HEADERS}    data=${PARAMS}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200 