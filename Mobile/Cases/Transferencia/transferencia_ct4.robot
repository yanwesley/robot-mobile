*** Keyword ***
Transferencia_ct4
    #--------------------------------------------------------------------------------------
    # Inicia transferência
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  30
    Click Element       ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnTrasferencia}  30
    Capture Page Screenshot
    Click Element       ${MainMenu.btnTrasferencia}
    Wait Until Element Is Visible  ${MainMenu.btnBarraLateralItens}  30
    Element Text Should Be  ${MainMenu.btnBarraLateralItens}  Transferência
    Input Text          ${pageTransferencia.pesquisarContato}  Aline De Oliveira
    Element Text Should Be  ${pageTransferencia.btnContato}  Aline De Oliveira
    Capture Page Screenshot
    Click Element       ${pageTransferencia.btnContato}

    #Clica no botão para selecionar outro banco
    Wait Until Element Is Visible  ${pageTransferencia.btnOutroBanco}  30
    Click Element                  ${pageTransferencia.btnOutroBanco}

    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}

    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  30
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Qual é o banco destinatário?
    Input Text                     ${pageTransferencia.bancoDest}    237
    Wait Until Element Is Visible  ${pageTransferencia.btnBanco}  30
    Tap                            ${pageTransferencia.btnBanco}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    
    #--------------------------------------------------------------------------------------
    # Validar exibição do título em número da agência na transferência
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  30
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Qual é o número da agência?
    #--------------------------------------------------------------------------------------
    # Validar exibição do teclado ao digitar o número da agência
    #--------------------------------------------------------------------------------------
    Log         Validar exibição do teclado
    Capture Page Screenshot
    Page Should Not Contain Element   ${pageLogin.btncontinue}
    Input Text                        ${pageTransferencia.agencia}      1
    Page Should Contain Element       ${pageLogin.btncontinue}
    Clear Text                        ${pageTransferencia.agencia}
    Input Text                        ${pageTransferencia.agencia}      1528
    Click Element                     ${pageLogin.btncontinue}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}


    #--------------------------------------------------------------------------------------
    # Validar exibição do título em conta com dígito na transferência
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  30
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Qual a conta com dígito?
    #--------------------------------------------------------------------------------------
    # Validar exibição do teclado ao digitar o número da conta com digito
    #--------------------------------------------------------------------------------------
    Log         Validar exibição do teclado
    Capture Page Screenshot
    Page Should Not Contain Element   ${pageLogin.btncontinue}
    Input Text                        ${pageTransferencia.conta}      12
    Page Should Not Contain Element   ${pageLogin.btncontinue}
    Clear Text                        ${pageTransferencia.conta}
    Input Text                        ${pageTransferencia.conta}      256
    Element Text Should Be            ${pageTransferencia.conta}      25-6
    Click Element                     ${pageLogin.btncontinue} 
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}


    #--------------------------------------------------------------------------------------
    # Validar exibição do título em tipo de conta (corrente ou poupança) na transferência
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  30
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Escolha o tipo de conta
    Element Text Should Be         ${pageTransferencia.btnContaCorrente}   Corrente
    Element Text Should Be         ${pageTransferencia.btnContaPoupanca}   Poupança
    Click Element                  ${pageTransferencia.btnContaCorrente}      
    Click Element                  ${pageLogin.btncontinue}  
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}


    #--------------------------------------------------------------------------------------
    # Validar exibição do título em valor da transferência 
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  300
    Element Text Should Be  ${pageTransferencia.lblPergunta}  Defina um valor para transferir
    #--------------------------------------------------------------------------------------
    # Validar exibição do teclado ao digitar o valor da transferência
    #--------------------------------------------------------------------------------------
    Log         Validar exibição do teclado
    Capture Page Screenshot
    Element Text Should Be  ${pageTransferencia.valor}   R$ 0,00
    Element Text Should Be  ${pageTransferencia.lblSaldoConta}   Saldo em conta
    Element Should Contain Text     ${pageTransferencia.lblValorSaldo}   R$
    Clear Text          ${pageTransferencia.valor}
    Input Text          ${pageTransferencia.valor}  100
    Capture Page Screenshot
    Click Element       ${pageLogin.btncontinue}


    #--------------------------------------------------------------------------------------
    # Validar exibição do título na descrição da transferência
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  300
    Element Text Should Be  ${pageTransferencia.lblPergunta}  Escolha uma descrição, caso queira.
    #--------------------------------------------------------------------------------------
    # Validar exibição do teclado ao digitar a descrição da transferência
    #--------------------------------------------------------------------------------------
    Log         Validar exibição do teclado
    Capture Page Screenshot
    Page Should Contain Element   ${pageLogin.btncontinue}     
    Input Text          ${pageTransferencia.descricaoTransferencia}  Automação Bari
    Click Element       ${pageLogin.btncontinue}
    Wait Until Element Is Visible  ${pageFinalizaTransferencia.btnFechar}  30
    Click Element  ${pageFinalizaTransferencia.btnFechar}
