*** Keyword ***
Transferencia_ct1

    #--------------------------------------------------------------------------------------
    # Realizar transferência para outra titularidade que é bari
    #--------------------------------------------------------------------------------------
    Click Element                  ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnTrasferencia}       300
    Capture Page Screenshot
    Click Element                  ${MainMenu.btnTrasferencia}
    Wait Until Element Is Visible  ${MainMenu.btnBarraLateralItens}  300
    Element Text Should Be         ${MainMenu.btnBarraLateralItens}  Transferência
    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  300
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Qual é o CPF?
    Input Text                     ${pageTransferencia.cpf}          09699387998
    Click Element                  ${pageLogin.btncontinue}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    Wait Until Element Is Visible  ${pageTransferencia.lblMsgRegras}  300
    Element Text Should Be         ${pageTransferencia.lblMsgRegras}  Regras de transferência
    #Element Text Should Be         ${pageTransferencia.lblDescricao}  Pensando na sua segurança, nesse momento estamos autorizando apenas transferências entre contas Bari ou contas da sua titularidade em outros bancos.
    Click Text                  Ok, entendi
    Wait Until Element Is Visible  ${pageTransferencia.lblContato}  300
    Element Text Should Be         ${pageTransferencia.lblContato}  Aline de Oliveira
    Capture Page Screenshot
    Click Element       ${pageTransferencia.lblBancoCliente}
    ConfirmaTransferencia   Aline de Oliveira  096.993.879-98  0001  4630-6  Bco Barigui Inv Fin S/A
    Click Element  ${pageFinalizaTransferencia.btnFechar}

    #--------------------------------------------------------------------------------------
    # Realizar transferência para outra titularidade que é bari que já é um contato
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  300
    Click Element       ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnTrasferencia}  300
    Capture Page Screenshot
    Click Element       ${MainMenu.btnTrasferencia}
    Wait Until Element Is Visible  ${MainMenu.btnBarraLateralItens}  300
    Element Text Should Be  ${MainMenu.btnBarraLateralItens}  Transferência
    Input Text          ${pageTransferencia.pesquisarContato}  Aline de Oliveira
    Element Text Should Be  ${pageTransferencia.btnContato}  Aline de Oliveira
    Capture Page Screenshot
    Click Element       ${pageTransferencia.btnContato}
    Wait Until Element Is Visible  ${pageTransferencia.lblBancoCliente}  300
    Element Text Should Be  ${pageTransferencia.lblBancoCliente}  Bco Barigui Inv Fin S/A
    Capture Page Screenshot
    Click Element       ${pageTransferencia.lblBancoCliente}
    ConfirmaTransferencia   Aline de Oliveira  096.993.879-98  0001  4710-8  Bco Barigui Inv Fin S/A
    Click Element  ${pageFinalizaTransferencia.btnFechar}

    #--------------------------------------------------------------------------------------
    # Realizar transferência da mesma titularidade que não é um contato
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  300
    Click Element                  ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnTrasferencia}       300
    Capture Page Screenshot
    Click Element                  ${MainMenu.btnTrasferencia}
    Wait Until Element Is Visible  ${MainMenu.btnBarraLateralItens}  300
    Element Text Should Be         ${MainMenu.btnBarraLateralItens}  Transferência
    Wait Until Element Is Visible  ${pageTransferencia.btnNovaTransferencia}  300
    Click Element                  ${pageTransferencia.btnNovaTransferencia}
    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  300
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Qual é o CPF?
    Input Text                     ${pageTransferencia.cpf}          09478206982
    Click Element                  ${pageLogin.btncontinue}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}

    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  300
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Qual é o banco destinatário?
    Input Text                     ${pageTransferencia.bancoDest}    237
    Wait Until Element Is Visible  ${pageTransferencia.btnBanco}  300
    Tap                            ${pageTransferencia.btnBanco}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    
    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  300
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Qual é o número da agência?
    Input Text                     ${pageTransferencia.agencia}      1528
    Click Element                  ${pageLogin.btncontinue}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}

    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  300
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Qual a conta com dígito?
    Input Text                     ${pageTransferencia.conta}      256
    Click Element                  ${pageLogin.btncontinue} 
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}

    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  300
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Escolha o tipo de conta
    Click Element                  ${pageTransferencia.btnContaCorrente}      
    Click Element                  ${pageLogin.btncontinue}  
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}

    ConfirmaTransferencia   Daffyne Alves Dias  094.782.069-82  1528  25-6  Banco Bradesco Sa
    Click Element  ${pageFinalizaTransferencia.btnFechar}

    #--------------------------------------------------------------------------------------
    # Realizar transferência da mesma titularidade que já é um contato
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  300
    Click Element       ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnTrasferencia}  300
    Capture Page Screenshot
    Click Element       ${MainMenu.btnTrasferencia}
    Wait Until Element Is Visible  ${MainMenu.btnBarraLateralItens}  300
    Element Text Should Be  ${MainMenu.btnBarraLateralItens}  Transferência
    Input Text          ${pageTransferencia.pesquisarContato}  Daffyne Alves Dias
    Element Text Should Be  ${pageTransferencia.btnContato}  Daffyne Alves Dias
    Capture Page Screenshot
    Click Element       ${pageTransferencia.btnContato}
    Wait Until Element Is Visible  ${pageTransferencia.lblBancoCliente}  300
    Element Text Should Be  ${pageTransferencia.lblBancoCliente}  Banco Bradesco Sa
    Capture Page Screenshot
    Click Element       ${pageTransferencia.lblBancoCliente}
    ConfirmaTransferencia   Daffyne Alves Dias  094.782.069-82  1528  25-6  Banco Bradesco Sa
    Click Element  ${pageFinalizaTransferencia.btnFechar}

    #--------------------------------------------------------------------------------------
    # Clicar no lápis para alterar o valor da transferência colocando mesmo valor
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  300
    Click Element       ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnTrasferencia}  300
    Capture Page Screenshot
    Click Element       ${MainMenu.btnTrasferencia}
    Wait Until Element Is Visible  ${MainMenu.btnBarraLateralItens}  300
    Element Text Should Be  ${MainMenu.btnBarraLateralItens}  Transferência
    Input Text          ${pageTransferencia.pesquisarContato}  Daffyne Alves Dias
    Element Text Should Be  ${pageTransferencia.btnContato}  Daffyne Alves Dias
    Capture Page Screenshot
    Click Element       ${pageTransferencia.btnContato}
    Wait Until Element Is Visible  ${pageTransferencia.lblBancoCliente}  300
    Element Text Should Be  ${pageTransferencia.lblBancoCliente}  Banco Bradesco Sa
    Capture Page Screenshot
    Click Element       ${pageTransferencia.lblBancoCliente}
    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  300
    Element Text Should Be  ${pageTransferencia.lblPergunta}  Defina um valor para transferir
    Capture Page Screenshot
    Clear Text          ${pageTransferencia.valor}
    Input Text          ${pageTransferencia.valor}  100
    Capture Page Screenshot
    Click Element       ${pageLogin.btncontinue}
    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  300
    Element Text Should Be  ${pageTransferencia.lblPergunta}  Escolha uma descrição, caso queira.
    Capture Page Screenshot
    Input Text          ${pageTransferencia.descricaoTransferencia}  Automação Bari
    Click Element       ${pageLogin.btncontinue}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    # Clica no lápis
    Wait Until Element Is Visible  ${pageTransferencia.btnLapis}  300
    Click Element     ${pageTransferencia.btnLapis}  
    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  300
    Element Text Should Be  ${pageTransferencia.lblPergunta}  Defina um valor para transferir
    Capture Page Screenshot
    Clear Text          ${pageTransferencia.valor}
    Input Text          ${pageTransferencia.valor}  100
    Capture Page Screenshot
    Click Element       ${pageLogin.btncontinue}
    #
    Element Should Be Visible  ${pageLabelsTransferencia.lblBanco}
    Element Should Be Visible  ${pageLabelsTransferencia.lblAgencia}
    Element Should Be Visible  ${pageLabelsTransferencia.lblConta}
    Element Should Be Visible  ${pageLabelsTransferencia.lblFavorecido}
    Element Should Be Visible  ${pageLabelsTransferencia.lblCPF}
    Element Should Be Visible  ${pageLabelsTransferencia.lblTipoConta}
    Element Should Be Visible  ${pageLabelsTransferencia.lblData}
    Element Should Be Visible  ${pageLabelsTransferencia.lblDescricao}
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblBanco}  Banco Bradesco Sa
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblAgencia}  1528 
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblConta}  	25-6
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblFavorecido}  Daffyne Alves Dias
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblCPF} 	094.782.069-82
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblTipoConta}  	Conta Corrente
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblDescricao}  Automação Bari
    Capture Page Screenshot
    Click Element       ${pageLogin.btncontinue}
    Wait Until Element Is Visible  ${pageFinalizaTransferencia.lblSenha}  300
    Input Text          ${pageLogin.psw}  1470
    Capture Page Screenshot
    Click Element       ${pageFinalizaTransferencia.btnConfirma}
    Wait Until Element Is Visible  ${pageFinalizaTransferencia.lblTransferenciaSucesso}  300
    Capture Page Screenshot
    Element Text Should Be  ${pageFinalizaTransferencia.lblvalor}  	R$ 1,00
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblFavorecido}  Daffyne Alves Dias
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblBanco}  Banco Bradesco Sa
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblAgencia}  1528 
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblConta}  	25-6
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblDescricao}  Automação Bari
    Element Should Be Visible  ${pageFinalizaTransferencia.lblAutentica}
    Wait Until Element Is Visible  ${pageFinalizaTransferencia.btnFechar}  30
    Click Element  ${pageFinalizaTransferencia.btnFechar}


   #--------------------------------------------------------------------------------------
    # Clicar no lápis para alterar o valor da transferência colocando outro valor
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  300
    Click Element       ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnTrasferencia}  300
    Capture Page Screenshot
    Click Element       ${MainMenu.btnTrasferencia}
    Wait Until Element Is Visible  ${MainMenu.btnBarraLateralItens}  300
    Element Text Should Be  ${MainMenu.btnBarraLateralItens}  Transferência
    Input Text          ${pageTransferencia.pesquisarContato}  Daffyne Alves Dias
    Element Text Should Be  ${pageTransferencia.btnContato}  Daffyne Alves Dias
    Capture Page Screenshot
    Click Element       ${pageTransferencia.btnContato}
    Wait Until Element Is Visible  ${pageTransferencia.lblBancoCliente}  300
    Element Text Should Be  ${pageTransferencia.lblBancoCliente}  Banco Bradesco Sa
    Capture Page Screenshot
    Click Element       ${pageTransferencia.lblBancoCliente}
    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  300
    Element Text Should Be  ${pageTransferencia.lblPergunta}  Defina um valor para transferir
    Capture Page Screenshot
    Clear Text          ${pageTransferencia.valor}
    Input Text          ${pageTransferencia.valor}  100
    Capture Page Screenshot
    Click Element       ${pageLogin.btncontinue}
    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  300
    Element Text Should Be  ${pageTransferencia.lblPergunta}  Escolha uma descrição, caso queira.
    Capture Page Screenshot
    Input Text          ${pageTransferencia.descricaoTransferencia}  Automação Bari
    Click Element       ${pageLogin.btncontinue}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    # Clica no lápis
    Wait Until Element Is Visible  ${pageTransferencia.btnLapis}  300
    Click Element     ${pageTransferencia.btnLapis}  
    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  300
    Element Text Should Be  ${pageTransferencia.lblPergunta}  Defina um valor para transferir
    Capture Page Screenshot
    Clear Text          ${pageTransferencia.valor}
    Input Text          ${pageTransferencia.valor}  200
    Capture Page Screenshot
    Click Element       ${pageLogin.btncontinue}
    #
    Element Should Be Visible  ${pageLabelsTransferencia.lblBanco}
    Element Should Be Visible  ${pageLabelsTransferencia.lblAgencia}
    Element Should Be Visible  ${pageLabelsTransferencia.lblConta}
    Element Should Be Visible  ${pageLabelsTransferencia.lblFavorecido}
    Element Should Be Visible  ${pageLabelsTransferencia.lblCPF}
    Element Should Be Visible  ${pageLabelsTransferencia.lblTipoConta}
    Element Should Be Visible  ${pageLabelsTransferencia.lblData}
    Element Should Be Visible  ${pageLabelsTransferencia.lblDescricao}
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblBanco}  Banco Bradesco Sa
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblAgencia}  1528 
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblConta}  	25-6
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblFavorecido}  Daffyne Alves Dias
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblCPF} 	094.782.069-82
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblTipoConta}  	Conta Corrente
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblDescricao}  Automação Bari
    Capture Page Screenshot
    Click Element       ${pageLogin.btncontinue}
    Wait Until Element Is Visible  ${pageFinalizaTransferencia.lblSenha}  300
    Input Text          ${pageLogin.psw}  1470
    Capture Page Screenshot
    Click Element       ${pageFinalizaTransferencia.btnConfirma}
    Wait Until Element Is Visible  ${pageFinalizaTransferencia.lblTransferenciaSucesso}  300
    Capture Page Screenshot
    Element Text Should Be  ${pageFinalizaTransferencia.lblvalor}  	R$ 2,00
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblFavorecido}  Daffyne Alves Dias
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblBanco}  Banco Bradesco Sa
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblAgencia}  1528 
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblConta}  	25-6
    Element Text Should Be  ${pageConfirmaDadosTransferencia.lblDescricao}  Automação Bari
    Element Should Be Visible  ${pageFinalizaTransferencia.lblAutentica}
    Wait Until Element Is Visible  ${pageFinalizaTransferencia.btnFechar}  30
    Click Element  ${pageFinalizaTransferencia.btnFechar}
