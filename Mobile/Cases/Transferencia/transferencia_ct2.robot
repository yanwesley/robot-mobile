*** Keyword ***
Transferencia_ct2
    
    # Excluir contato Aline, caso exista

    #--------------------------------------------------------------------------------------
    # Escolher outra transferência sendo que esse cpf não é um contato (mas existe outros contatos)
    #--------------------------------------------------------------------------------------
    Click Element                  ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnTrasferencia}       30
    Capture Page Screenshot
    Click Element                  ${MainMenu.btnTrasferencia}
    Wait Until Element Is Visible  ${MainMenu.btnBarraLateralItens}  30
    Element Text Should Be         ${MainMenu.btnBarraLateralItens}  Transferência
    #Clica no botão de transferencia
    Wait Until Element Is Visible  ${pageTransferencia.btnNovaTransferencia}  30
    Click Element                  ${pageTransferencia.btnNovaTransferencia}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando} 

    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  30
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Qual é o CPF?
    Input Text                     ${pageTransferencia.cpf}          09699387998
    Click Element                  ${pageLogin.btncontinue}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    Wait Until Element Is Visible  ${pageTransferencia.lblMsgRegras}  30
    Element Text Should Be         ${pageTransferencia.lblMsgRegras}  Regras de transferência
    #Element Text Should Be         ${pageTransferencia.lblDescricao}  Pensando na sua segurança, nesse momento estamos autorizando apenas transferências entre contas Bari ou contas da sua titularidade em outros bancos.
    Click Text                  Ok, entendi
    Wait Until Element Is Visible  ${pageTransferencia.lblContato}  30
    Element Text Should Be         ${pageTransferencia.lblContato}  Aline De Oliveira
    Capture Page Screenshot
    Wait Until Element Is Visible  ${pageTransferencia.lblBancoCliente}  30
    Element Text Should Be  ${pageTransferencia.lblBancoCliente}  Bco Barigui Inv Fin S/A
    Capture Page Screenshot
    Click Element       ${pageTransferencia.lblBancoCliente}

    ConfirmaTransferencia  Aline De Oliveira  096.993.879-98  0001  4340-4  Bco Barigui Inv Fin S/A
    Click Element  ${pageFinalizaTransferencia.btnFechar}

    #--------------------------------------------------------------------------------------
    # Escolher outra transferência sendo esse cpf já é um contato para um banco diferente 
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  30
    Click Element       ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnTrasferencia}  30
    Capture Page Screenshot
    Click Element       ${MainMenu.btnTrasferencia}
    Wait Until Element Is Visible  ${MainMenu.btnBarraLateralItens}  30
    Element Text Should Be  ${MainMenu.btnBarraLateralItens}  Transferência
    Input Text          ${pageTransferencia.pesquisarContato}  Aline De Oliveira
    Element Text Should Be  ${pageTransferencia.btnContato}  Aline De Oliveira
    Capture Page Screenshot
    Click Element       ${pageTransferencia.btnContato}

    #Clica no botão para selecionar outro banco
    Wait Until Element Is Visible  ${pageTransferencia.btnOutroBanco}  30
    Click Element                  ${pageTransferencia.btnOutroBanco}

    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}

    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  30
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Qual é o banco destinatário?
    Input Text                     ${pageTransferencia.bancoDest}    237
    Wait Until Element Is Visible  ${pageTransferencia.btnBanco}  30
    Tap                            ${pageTransferencia.btnBanco}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    
    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  30
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Qual é o número da agência?
    Input Text                     ${pageTransferencia.agencia}      1528
    Click Element                  ${pageLogin.btncontinue}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}

    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  30
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Qual a conta com dígito?
    Input Text                     ${pageTransferencia.conta}      256
    Click Element                  ${pageLogin.btncontinue} 
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}

    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  30
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Escolha o tipo de conta
    Click Element                  ${pageTransferencia.btnContaCorrente}      
    Click Element                  ${pageLogin.btncontinue}  
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}

    ConfirmaTransferencia   Aline De Oliveira  096.993.879-98  1528  25-6  Banco Bradesco Sa
    Click Element  ${pageFinalizaTransferencia.btnFechar}
 
    #--------------------------------------------------------------------------------------
    # Escolher outra transferência sendo esse cpf já é um contato, para o mesmo banco registrado no contato
    #--------------------------------------------------------------------------------------
    Click Element       ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnTrasferencia}  300
    Capture Page Screenshot
    Click Element       ${MainMenu.btnTrasferencia}
    Wait Until Element Is Visible  ${MainMenu.btnBarraLateralItens}  300
    Element Text Should Be  ${MainMenu.btnBarraLateralItens}  Transferência
    Input Text          ${pageTransferencia.pesquisarContato}  Aline De Oliveira
    Element Text Should Be  ${pageTransferencia.btnContato}  Aline De Oliveira
    Capture Page Screenshot
    Click Element       ${pageTransferencia.btnContato}
    Wait Until Element Is Visible  ${pageTransferencia.lblBancoCliente}  300
    Element Text Should Be  ${pageTransferencia.lblBancoCliente}  Bco Barigui Inv Fin S/A
    Capture Page Screenshot
    Click Element       ${pageTransferencia.lblBancoCliente}
    ConfirmaTransferencia   Aline De Oliveira  096.993.879-98  0001  4340-4  Bco Barigui Inv Fin S/A
    Click Element  ${pageFinalizaTransferencia.btnFechar}
