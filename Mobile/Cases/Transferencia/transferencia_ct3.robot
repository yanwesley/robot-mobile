*** Keyword ***
Transferencia_ct3

    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  30
    Click Element                  ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnTrasferencia}       30
    Capture Page Screenshot
    Click Element                  ${MainMenu.btnTrasferencia}
    Wait Until Element Is Visible  ${MainMenu.btnBarraLateralItens}  30
    Element Text Should Be         ${MainMenu.btnBarraLateralItens}  Transferência
    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  30
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Qual é o CPF?
    Input Text                     ${pageTransferencia.cpf}          09699387998
    Click Element                  ${pageLogin.btncontinue}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}

    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  30
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Qual é o banco destinatário?
    Input Text                     ${pageTransferencia.bancoDest}    237
    Wait Until Element Is Visible  ${pageTransferencia.btnBanco}  30
    Tap                            ${pageTransferencia.btnBanco}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    
    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  30
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Qual é o número da agência?
    Input Text                     ${pageTransferencia.agencia}      1528
    Click Element                  ${pageLogin.btncontinue}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}

    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  30
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Qual a conta com dígito?
    Input Text                     ${pageTransferencia.conta}      256
    Click Element                  ${pageLogin.btncontinue} 
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}

    Wait Until Element Is Visible  ${pageTransferencia.lblPergunta}  30
    Element Text Should Be         ${pageTransferencia.lblPergunta}  Escolha o tipo de conta
    Click Element                  ${pageTransferencia.btnContaCorrente}      
    Click Element                  ${pageLogin.btncontinue}  
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}

    ConfirmaTransferencia   Aline De Oliveira  096.993.879-98  1528  25-6  Banco Bradesco Sa
    Click Element       ${pageFinalizaTransferencia.btnDownloadComprovante}
    Wait Until Element Is Visible  ${pageFinalizaTransferencia.btnPermissaoDownload}  300
    Click Element       ${pageFinalizaTransferencia.btnPermissaoDownload}
    Sleep  3s

    #--------------------------------------------------------------------------------------
    # Validar baixa do comprovante de transferência
    #--------------------------------------------------------------------------------------

    # não faz nenhuma chamada para o back, vai ter que validar nas notificações

    ${sessionId}  Get Appium SessionId
    Log          ${sessionId}
    Create Session  newsession  http://localhost:4723/wd/hub/session/${sessionID}

    ${RESPOSTA}     Post Request     newsession     appium/device/open_notifications
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200

    Sleep    3s

    ${data_atual}=      Get Current Date      result_format=%d-%m-%y
    FOR    ${cont}    IN RANGE    0    20 
        ${status}   ${elementoEncontrado}   BuscaDownload   transferencia-${data_atual}  ${sessionId}   Buscar
        Log    ${status}
        Run Keyword If    ${status}     Exit For Loop 

        ${PARAMS}=  catenate
        ...    {
        ...        "startX":401,
        ...        "startY":2064,
        ...        "endX":433,
        ...        "endY":770,
        ...        "steps":14
        ...    }


        Create Session  newsession  http://127.0.0.1:8200/wd/hub/session/${sessionID}

        ${HEADERS}      Create Dictionary       Content-Type=application/json
        
        ${RESPOSTA}     Post Request     newsession    touch/perform    data=${PARAMS}    headers=${HEADERS}
        Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    END

    Should Be Equal As Strings      ${status}     True
    Run Keyword If    ${status}     BuscaDownload    ${elementoEncontrado}  ${sessionId}    Clicar  

    Sleep  3s

    Capture Page Screenshot

    #--------------------------------------------------------------------------------------
    # Validar compartilhamento do comprovante de transferência
    #--------------------------------------------------------------------------------------

    #--------------------------------------------------------------------------------------
    # Verificar nome do comprovante de transferência em baixar
    #--------------------------------------------------------------------------------------


    #--------------------------------------------------------------------------------------
    # Verificar nome do comprovante de transferência em compartilhar
    #--------------------------------------------------------------------------------------



    #--------------------------------------------------------------------------------------
    # Verificar como está a imagem do comprovante de transferência em baixar
    #--------------------------------------------------------------------------------------


    #--------------------------------------------------------------------------------------
    # Verificar como está a imagem do comprovante de transferência em compartilhar
    #--------------------------------------------------------------------------------------


