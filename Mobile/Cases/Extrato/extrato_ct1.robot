*** Keyword ***
Extrato_ct1

    #--------------------------------------------------------------------------------------
    # Visualizar saldo menu principal clicando no olho
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${pageHome.btnOcultarSaldo}        30 
    Click Element                  ${pageHome.btnOcultarSaldo}
    Wait Until Element Is Visible  ${pageHome.lblSaldo}
    Element Should Contain Text    ${pageHome.lblSaldo}    R$
   
    #--------------------------------------------------------------------------------------
    # Para de visualizar o saldo menu principal clicando no olho
    #--------------------------------------------------------------------------------------
    Click Element                  ${pageHome.btnOcultarSaldo}
    Wait Until Element Is Visible  ${pageHome.lblSaldo}
    Element Text Should Be         ${pageHome.lblSaldo}    Mostrar saldo

    #--------------------------------------------------------------------------------------
    # Visualizar saldo menu principal clicando no "mostrar saldo"
    #--------------------------------------------------------------------------------------
    Click Element                  ${pageHome.btnMostrarSaldo}
    Element Should Contain Text    ${pageHome.lblSaldo}    R$

    #--------------------------------------------------------------------------------------
    # Clicar no valor do saldo e não deverá ocorrer nada
    #--------------------------------------------------------------------------------------
    Click Element                  ${pageHome.lblSaldo}
    Element Should Contain Text    ${pageHome.lblSaldo}    R$

    Click Element                  ${pageHome.btnOcultarSaldo}
    Wait Until Element Is Visible  ${pageHome.lblSaldo}
    Element Text Should Be         ${pageHome.lblSaldo}    Mostrar saldo

    Wait Until Element Is Visible  ${MainMenu.btnVoltar}        30
    Click Element                  ${MainMenu.btnVoltar}