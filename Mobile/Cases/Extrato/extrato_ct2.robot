*** Keyword ***
Extrato_ct2

    #--------------------------------------------------------------------------------------
    # Visualizar saldo no extrato
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${pageExtrato.btnExtrato}        30 
    Click Element                  ${pageExtrato.btnExtrato}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    Wait Until Element Is Visible  ${pageHome.btnOcultarSaldo}        30 
    Click Element                  ${pageHome.btnOcultarSaldo}
    Wait Until Element Is Visible  ${pageHome.lblSaldo}
    Element Should Contain Text    ${pageHome.lblSaldo}    R$

    #--------------------------------------------------------------------------------------
    # Para de visualizar o saldo menu principal clicando no olho
    #--------------------------------------------------------------------------------------
    Click Element                  ${pageHome.btnOcultarSaldo}
    Wait Until Element Is Visible  ${pageHome.lblSaldo}
    Element Text Should Be         ${pageHome.lblSaldo}    Mostrar saldo

    #--------------------------------------------------------------------------------------
    # Visualizar saldo no extrato clicando em "Mostrar Saldo"
    #--------------------------------------------------------------------------------------
    Click Element                  ${pageHome.btnMostrarSaldo}
    Element Should Contain Text    ${pageHome.lblSaldo}    R$

    #--------------------------------------------------------------------------------------
    # Clicar no valor do saldo e não deverá ocorrer nada
    #--------------------------------------------------------------------------------------
    Click Element                  ${pageHome.lblSaldo}
    Element Should Contain Text    ${pageHome.lblSaldo}    R$
    Click Element                  ${pageHome.btnOcultarSaldo}
    Wait Until Element Is Visible  ${pageHome.lblSaldo}
    Element Text Should Be         ${pageHome.lblSaldo}    Mostrar saldo
    Wait Until Element Is Visible  ${MainMenu.btnVoltar}        30
    Click Element                  ${MainMenu.btnVoltar}


    #--------------------------------------------------------------------------------------
    # Se saldo está fechado na dashboard em extrato tem que vir o saldo fechado
    #--------------------------------------------------------------------------------------
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    Wait Until Element Is Visible  ${pageHome.btnOcultarSaldo}        30 
    Element Text Should Be         ${pageHome.lblSaldo}    Mostrar saldo
    Click Element                  ${pageExtrato.btnExtrato}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    Wait Until Element Is Visible  ${pageHome.lblSaldo}
    Element Text Should Be         ${pageHome.lblSaldo}    Mostrar saldo
    Wait Until Element Is Visible  ${MainMenu.btnVoltar}        30
    Click Element                  ${MainMenu.btnVoltar}


    #--------------------------------------------------------------------------------------
    # Se saldo está aberto na dashboard em extrato tem que vir o saldo aberto
    #--------------------------------------------------------------------------------------
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    Wait Until Element Is Visible  ${pageHome.btnOcultarSaldo}        30 
    Click Element                  ${pageHome.btnOcultarSaldo}
    Click Element                  ${pageHome.lblSaldo}
    Element Should Contain Text    ${pageHome.lblSaldo}    R$

    Wait Until Element Is Visible  ${pageExtrato.btnExtrato}        30 
    Click Element                  ${pageExtrato.btnExtrato} 
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    Wait Until Element Is Visible  ${pageHome.lblSaldo}
    Element Should Contain Text    ${pageHome.lblSaldo}    R$

    Wait Until Element Is Visible  ${MainMenu.btnVoltar}        30
    Click Element                  ${MainMenu.btnVoltar}

    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    Wait Until Element Is Visible  ${pageHome.btnOcultarSaldo}        30 
    Click Element                  ${pageHome.btnOcultarSaldo}