*** Keyword ***
Extrato_ct3

    #--------------------------------------------------------------------------------------
    # Visualizar extrato pelo menu lateral
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}        30 
    Click Element                  ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnExtrato}    30    
    Click Element                  ${MainMenu.btnExtrato}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    Wait Until Element Is Visible  ${pageExtrato.lblExtrato}  30
    Capture Page Screenshot
    sleep       5s

    #--------------------------------------------------------------------------------------
    # Filtro de entrada 
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible      ${pageExtrato.btnFiltroEntrada}  30
    Click Element                      ${pageExtrato.btnFiltroEntrada}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    sleep       3s
    Wait Until Element Is Visible      ${pageExtrato.lblPrimeiraTransacao}  30
    Element Should Not Contain Text    ${pageExtrato.lblPrimeiraTransacao}    -
    Element Should Contain Text        ${pageExtrato.lblPrimeiraTransacao}    R$


    #--------------------------------------------------------------------------------------
    # Filtro de saída 
    #--------------------------------------------------------------------------------------
    Click Element                  ${pageExtrato.btnFiltroSaida}
    sleep       5s
    Element Should Contain Text    ${pageExtrato.lblPrimeiraTransacao}    - R$


