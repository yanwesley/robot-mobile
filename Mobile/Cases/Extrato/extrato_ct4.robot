*** Keyword ***
Extrato_ct3

    #--------------------------------------------------------------------------------------
    # Visualizar extrato pelo menu lateral
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}        30 
    Click Element                  ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnExtrato}    30    
    Click Element                  ${MainMenu.btnExtrato}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    Wait Until Element Is Visible  ${pageExtrato.lblExtrato}  30
    Capture Page Screenshot
    sleep       5s


    #--------------------------------------------------------------------------------------
    # Filtro deve aparecer apartir do 2° mês da abertura da conta
    #--------------------------------------------------------------------------------------
   