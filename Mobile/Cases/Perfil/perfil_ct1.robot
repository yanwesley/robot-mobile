*** Keyword ***
Perfil_ct1

    #--------------------------------------------------------------------------------------
    # Verificar se no perfil está aparecendo o número da conta com -
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}        30 
    Click Element                  ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnPerfil}    30    
    Click Element                  ${MainMenu.btnPerfil}
    Wait Until Element Is Visible  ${pagePerfil.lblPerfil}  30
    Capture Page Screenshot
    Element Should Contain Text    ${pagePerfil.lblConta}   -
    Wait Until Element Is Visible  ${MainMenu.btnVoltar}  30
    Click Element                  ${MainMenu.btnVoltar}

    #--------------------------------------------------------------------------------------
    # Verificar se no perfil está aparecendo número da conta, banco e agência
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}        30  
    Click Element                  ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnPerfil}        30    
    Click Element                  ${MainMenu.btnPerfil}
    Wait Until Element Is Visible  ${pagePerfil.lblPerfil}      30
    Capture Page Screenshot
    Element Text Should Be         ${pagePerfil.lblBanco}       330
    Element Text Should Be         ${pagePerfil.lblAgencia}     0001
    Element Text Should Be         ${pagePerfil.lblConta}       4370-6
    Wait Until Element Is Visible  ${MainMenu.btnVoltar}        30
    Click Element                  ${MainMenu.btnVoltar}
