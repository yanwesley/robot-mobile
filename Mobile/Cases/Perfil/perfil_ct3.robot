*** Keyword ***
Perfil_ct3

    #--------------------------------------------------------------------------------------
    # Verificar se ao clicar em cancelar conta está aparecendo a mensagem
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}        30 
    Click Element                  ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnPerfil}               30    
    Click Element                  ${MainMenu.btnPerfil}
    Wait Until Element Is Visible  ${pagePerfil.lblPerfil}             30
    Capture Page Screenshot
    Click Element                  ${pagePerfil.btnCancelarConta}
    Wait Until Element Is Visible  ${pagePerfil.lblCancelamento}       30
    Element Should Contain Text    ${pagePerfil.lblCancelamento}       Cancelamento
    Element Text Should Be         ${pagePerfil.lblMsgCancelamento}    Você confirma o desejo de cancelar sua conta?


    #--------------------------------------------------------------------------------------
    # Verificar se ao cancelar conta aparece mensagem explicando que não é imediato
    #--------------------------------------------------------------------------------------
    Element Text Should Be         ${pagePerfil.lblAviso}       	O cancelamento da sua conta não é imediato e passará por um processo manual, podendo levar alguns dias até a conclusão.


    #--------------------------------------------------------------------------------------
    # Verificar se ao clicar em não está fechando a mensagem de cancelar conta
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${pagePerfil.btnNao}               30    
    Click Element                  ${pagePerfil.btnNao}
    Wait Until Element Is Visible  ${pagePerfil.lblPerfil}    


    #--------------------------------------------------------------------------------------
    # Verificar se ao cancelar conta é encaminhado um email
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${pagePerfil.btnCancelarConta} 
    Click Element                  ${pagePerfil.btnCancelarConta}
    Wait Until Element Is Visible  ${pagePerfil.lblCancelamento}      30
    Element Should Contain Text    ${pagePerfil.lblCancelamento}      Cancelamento
    Wait Until Element Is Visible  ${pagePerfil.btnSim}               30    
    Click Element                  ${pagePerfil.btnSim}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    Wait Until Element Is Visible  ${pagePerfil.lblCancelamento}               30
    Element Text Should Be         ${pagePerfil.lblCancelamento}      Cancelamento em andamento
    Element Text Should Be         ${pagePerfil.lblMsgCancelamento}   O processo de cancelamento da sua conta já foi iniciado e enviaremos todas as informações em seu e-mail.