*** Keyword ***
Perfil_ct2

    #--------------------------------------------------------------------------------------
    # Verificar se o copiar no perfil está trazendo apenas o número da conta
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}        30 
    Click Element                  ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnPerfil}    30    
    Click Element                  ${MainMenu.btnPerfil}
    Wait Until Element Is Visible  ${pagePerfil.lblPerfil}  30
    Click Element                  ${pagePerfil.btnCopiarConta}

    #--------------------------------------------------------------------------------------
    # Verificar se depois de copiar no perfil está mostrando mensagem verde
    #--------------------------------------------------------------------------------------
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}  
    Wait Until Element Is Visible             ${pageHome.lblMsgAvisoVerde}    30
    Element Text Should Be        ${pageHome.lblMsgAvisoVerde}     Conta copiada

    ${sessionId}  Get Appium SessionId
    Log          ${sessionId}
    Create Session  newsession  http://localhost:4723/wd/hub/session/${sessionID}

    ${HEADERS}      Create Dictionary       Content-Type=string
  
    ${RESPOSTA}     Post Request     newsession      appium/device/get_clipboard    headers=${HEADERS}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    ${conta_base64}=    Set Variable    ${RESPOSTA.json()['value']}
    # verificar
    ${conta}=    Convert To String    ${conta_base64}   
    Should Be Equal As Strings     ${conta}    NDcxMC04\n

    #--------------------------------------------------------------------------------------
    # Verificar se o compartilhar no perfil está trazendo número da conta, banco, cpf, nome, etc
    #--------------------------------------------------------------------------------------
    Click Element                  ${pagePerfil.btnCompartilharConta}
    Sleep  5s

    ${sessionId}  Get Appium SessionId
    Log          ${sessionId}
    Create Session  newsession  http://localhost:4723/wd/hub/session/${sessionID}


    ${PARAMS}=  catenate
    ...     {
    ...        "using": "id", 
    ...        "value": "${pagePerfil.lblMeioCompartilhamento}"
    ...     }


    ${HEADERS}      Create Dictionary       accept=application/json   Content-Type=application/json
  
    ${RESPOSTA}     Post Request     newsession      elements   data=${PARAMS}   headers=${HEADERS}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    ${elementos}=    Set Variable    ${RESPOSTA.json()['value']}

    ${length}    Get Length  ${elementos}
    Log     ${length}
    FOR     ${i}  IN RANGE  ${length}
        ${element1}=    Set Variable    ${elementos[${i}]['ELEMENT']}
        ${RESPOSTA}     Get Request     newsession      element/${element1}/text   headers=${HEADERS}
        Should Be Equal As Strings      ${RESPOSTA.status_code}    200
        ${nome_item}=    Set Variable    ${RESPOSTA.json()['value']}
        ${status}    Run Keyword And Return Status    Should Contain   ${nome_item}   Copy to clipboard
        Run Keyword If    ${status}     Exit For Loop 
    END

    Should Be Equal As Strings      ${status}    True
    
    ${element1}=    Set Variable    ${elementos[${i}]['ELEMENT']}
    ${RESPOSTA}     Post Request     newsession      element/${element1}/click   headers=${HEADERS}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200


    #Confere valor 
    ${sessionId}  Get Appium SessionId
    Log          ${sessionId}
    Create Session  newsession  http://localhost:4723/wd/hub/session/${sessionID}

    ${HEADERS}      Create Dictionary       Content-Type=string
  
    ${RESPOSTA}     Post Request     newsession      appium/device/get_clipboard    headers=${HEADERS}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    ${conta_completa_base64}=    Set Variable    ${RESPOSTA.json()['value']}
    # verificar
    ${conta_completa}=    Convert To String    ${conta_completa_base64}   
    Should Be Equal As Strings     ${conta_completa}    RXNzZXMgc8OjbyBvcyBkYWRvcyBkYSBtaW5oYSBjb250YSBCYXJpIQoKQWxpbmUgRGUgT2xpdmVp\ncmEKQ1BGOiAwOTYuOTkzLjg3OS05OAoKTsO6bWVybyBkbyBCYW5jbzogMzMwIC0gQmFyaQpBZ8Oq\nbmNpYTogMDAwMQpDb250YTogNDcxMC04CgpBcHJvdmVpdGUgcGFyYSBhYnJpciBhIHN1YSBjb250\nYSB0YW1iw6ltISA7KQp3d3cuYmFuY29iYXJpLmNvbS5icg==\n\n

    # Should Be Equal As Strings      ${nome_item}    Esses são os dados da minha conta Bari! Aline De Oliveira CPF: 096.993.879-98 Número do Banco: 330 - Bari Agência: 0001 Conta: 4710-8 Aproveite para abrir a sua conta também! ;) www.bancobari.com.br 