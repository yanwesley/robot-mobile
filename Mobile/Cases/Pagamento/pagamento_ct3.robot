*** Keyword ***
Pagamento_ct3

    #--------------------------------------------------------------------------------------
    # Realizar o pagamento  clicando no icone camera e depois clicar no botao de digitar
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  30  
    Click Element       ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnPagamento}  30    
    Click Element       ${MainMenu.btnPagamento}
    Wait Until Element Is Visible  ${pagePagamento.lblPagamento}  30
    Capture Page Screenshot
    Click Element       ${pagePagamento.btnCamera}
    Wait Until Element Is Visible  ${pagePagamento.btnPermiteAcessoCam}  30    
    Click Element       ${pagePagamento.btnPermiteAcessoCam}

    Wait Until Element Is Visible  ${pagePagamento.btnDigitarCodigo}  30  

    Click Element       ${pagePagamento.btnHabilitalanterna}
    Sleep    5s
    Click Element       ${pagePagamento.btnDesabilitalanterna}


    Create Session  newsession  http://localhost:4723/wd/hub/session/${sessionID}

    ${PARAMS}=  catenate
        ...     {
        ...        "using": "id", 
        ...        "value": "${pagePagamento.btnDigitarCodigo}"
        ...     }


    ${HEADERS}      Create Dictionary       accept=application/json   Content-Type=application/json
  
    ${RESPOSTA}     Post Request     newsession      elements   data=${PARAMS}   headers=${HEADERS}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    ${elementos}=    Set Variable    ${RESPOSTA.json()['value']}
    ${element1}=    Set Variable    ${elementos[0]['ELEMENT']}
    ${RESPOSTA}     Post Request     newsession      element/${element1}/click   headers=${HEADERS}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200


    Wait Until Element Is Visible  ${pagePagamento.lblPagamento}  30