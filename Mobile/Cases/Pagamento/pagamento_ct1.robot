*** Keyword ***
Pagamento_ct1

    #--------------------------------------------------------------------------------------
    # Realizar o pagamento de um boleto que já foi pago digitando o codigo
    #--------------------------------------------------------------------------------------
    Click Element       ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnPagamento}  30    
    Click Element       ${MainMenu.btnPagamento}
    Wait Until Element Is Visible  ${pagePagamento.lblPagamento}  30
    Capture Page Screenshot
    
    Element Text Should Be  ${pagePagamento.lblPagamento}   Pagamento
    Element Text Should Be  ${pagePagamento.lblCodigoBarras}   Código de barras
    Element Text Should Be  ${pagePagamento.lblDescricao}   Descrição

    Input Text          ${pagePagamento.codigoBarras}   836000000015378101110005001010202099270605446334
    Capture Page Screenshot
    Input Text          ${pagePagamento.descricao}  Boleto ja pago
    Capture Page Screenshot   
    Click Element       ${pageLogin.btncontinue} 

    #--------------------------------------------------------------------------------------
    # Verificar se aparece a mensagem de erro informando que já foi pago
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  	${pagePagamento.imgAviso}  30
    Element Text Should Be  ${pagePagamento.lblAviso}  Pagamento não autorizado
    #Element Text Should Be  ${pagePagamento.txtAviso} Parece que este boleto já foi pago!
    Capture Page Screenshot
    Click Element       ${pagePagamento.btnFecharAviso}
    Wait Until Element Is Visible  ${MainMenu.btnVoltar}  30
    Click Element  ${MainMenu.btnVoltar}


    #--------------------------------------------------------------------------------------
    # Realizar o pagamento de um boleto invalido digitando o codigo
    #--------------------------------------------------------------------------------------
    Click Element       ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnPagamento}  30    
    Click Element       ${MainMenu.btnPagamento}
    Wait Until Element Is Visible  ${pagePagamento.lblPagamento}  30
    Capture Page Screenshot
    
    Element Text Should Be  ${pagePagamento.lblPagamento}   Pagamento
    Element Text Should Be  ${pagePagamento.lblCodigoBarras}   Código de barras
    Element Text Should Be  ${pagePagamento.lblDescricao}   Descrição

    Input Text          ${pagePagamento.codigoBarras}   836000000015378101111105001010202099270605446334
    Capture Page Screenshot
    Input Text          ${pagePagamento.descricao}  Boleto Invalido
    Capture Page Screenshot
    Click Element       ${pageLogin.btncontinue}


    #--------------------------------------------------------------------------------------
    # Verificar se aparece uma mensagem de erro 
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible  	${pagePagamento.imgAviso}  30
    Element Text Should Be  ${pagePagamento.lblAviso}  Pagamento não\nautorizado
    #Element Text Should Be  ${pagePagamento.txtAviso} Boleto invalido!
    Capture Page Screenshot
    Click Element       ${pagePagamento.btnFecharAviso}
    Wait Until Element Is Visible  ${MainMenu.btnVoltar}  30
    Click Element  ${MainMenu.btnVoltar}