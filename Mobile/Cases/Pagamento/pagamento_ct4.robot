*** Keyword ***
Pagamento_ct4  
   
   
    # Conferir o valor antes do pagamento do boleto  
    Wait Until Element Is Visible      ${pageHome.btnMostrarSaldo}     30
    Click Element                      ${pageHome.btnMostrarSaldo}
    # Verifica o saldo inicial
    ${saldoContaInicial}               Get Text               ${pageHome.lblSaldo}
    ${saldoContaInicialNumber}=        ConvertReaisToNumber   ${saldoContaInicial}  
    Click Element                      ${pageHome.btnOcultarSaldo} 

    #--------------------------------------------------------------------------------------
    # Realizar o pagamento de um boleto válido
    #--------------------------------------------------------------------------------------
    Click Element       ${MainMenu.btnMenuPrincipal}
    Wait Until Element Is Visible  ${MainMenu.btnPagamento}  30    
    Click Element       ${MainMenu.btnPagamento}
    Wait Until Element Is Visible  ${pagePagamento.lblPagamento}  30
    Capture Page Screenshot
    
    Element Text Should Be  ${pagePagamento.lblPagamento}   Pagamento
    Element Text Should Be  ${pagePagamento.lblCodigoBarras}   Código de barras
    Element Text Should Be  ${pagePagamento.lblDescricao}   Descrição

    Input Text          ${pagePagamento.codigoBarras}   03399201595100435015614104501029594860000084883
    Capture Page Screenshot
    Input Text          ${pagePagamento.descricao}  Boleto Ok
    Capture Page Screenshot   
    Click Element       ${pageLogin.btncontinue} 
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando} 

    #Confere dados
    Wait Until Element Is Visible  ${ConfirmaDadosPagamento.lblTituloValor}  30
    Capture Page Screenshot
    
    Element Text Should Be  ${ConfirmaDadosPagamento.lblTituloValor}        R$ 848,83
    Element Text Should Be  ${ConfirmaDadosPagamento.lblPerguntaValor}  	Valor do boleto
    Element Text Should Be  ${ConfirmaDadosPagamento.lblValor}              R$ 848,83
    Element Text Should Be  ${ConfirmaDadosPagamento.lblPerguntaData}       Data do pagamento
    ${data_atual}=      Get Current Date      result_format=%d/%m/%Y
    Element Text Should Be  ${ConfirmaDadosPagamento.lblData}               ${data_atual}
    Element Text Should Be  ${ConfirmaDadosPagamento.lblPerguntaVencimento}        Vencimento
    Element Text Should Be  ${ConfirmaDadosPagamento.lblVencimento}        27/09/2023
    Element Text Should Be  ${ConfirmaDadosPagamento.lblPerguntaDescricao}        Descrição
    Element Text Should Be  ${ConfirmaDadosPagamento.lblDescricao}        Boleto Ok
    Element Text Should Be  ${ConfirmaDadosPagamento.lblPerguntaFavorecido}        Para
    Element Text Should Be  ${ConfirmaDadosPagamento.lblFavorecido}        BENEFICIARIO AMBIENTE HOMOLOGACAO
    Element Text Should Be  ${ConfirmaDadosPagamento.lblPerguntaBanco}        Banco
    Element Text Should Be  ${ConfirmaDadosPagamento.lblBanco}        BANCO SANTANDER S.A
    Element Text Should Be  ${ConfirmaDadosPagamento.lblPerguntaCodigo}        Código de barras
    Element Text Should Be  ${ConfirmaDadosPagamento.lblCodigo}        03399.20159 51004.350156 14104.501029 5 94860000084883
    Click Element       ${pageLogin.btncontinue} 

    #Conferir msg de sucesso
    Wait Until Element Is Visible  ${ConfirmaDadosPagamento.lblConfirmaPagamento}  30
    Element Text Should Be  ${ConfirmaDadosPagamento.lblConfirmaPagamento}        Pagamento realizado com sucesso!
    #Validar informacoes no comprovante

    Capture Page Screenshot
    Click Element       ${pagePagamento.btnFecharAviso}

    #--------------------------------------------------------------------------------------
    # verificar se o valor foi descontado da conta
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible      ${pageHome.btnMostrarSaldo}     30
    Click Element                      ${pageHome.btnMostrarSaldo}
    Wait Until Element Is Visible      ${pageHome.lblSaldo}   30
    ${saldoContaFinal}                 Get Text               ${pageHome.lblSaldo}
    ${saldoContaFinalNumber}=          ConvertReaisToNumber   ${saldoContaFinal}   
    ${diferenca}=                      Evaluate               ${saldoContaInicialNumber} - ${saldoContaFinalNumber}
    Log                                ${diferenca}
    Should Be Equal                    ${diferenca}           ${848.83}
    Click Element                      ${pageHome.btnOcultarSaldo}


    #--------------------------------------------------------------------------------------
    # verificar se o pagamento aparece no extrato
    #--------------------------------------------------------------------------------------
    
    ${conta}    RetornaNumeroConta   ${cpf}

    ${token}  GeraToken  ${cpf}
    Log       ${token} 
    ${HEADERS}      Create Dictionary       accept=application/json   User-Agent=8d31a8d5-f4a5-429b-943f-04a6d15bda46    Authorization=${token}  
    
    Create Session    session    https://api.qa.bancobari.com.br/account/v1 
    Log       session 
    
    ${RESPOSTA}     Get Request    session   transactions?accountId=${conta}&page=0&itemsPerPage=10    headers=${HEADERS}  
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200 
    Log     ${RESPOSTA.json()}
    ${json_value}  Set Variable  ${RESPOSTA.json()} 
    ${operacoes}  Set Variable     ${json_value['items']}
    Log     ${operacoes}

    ${length}    Get Length  ${operacoes}
    ${data_atual}=      Get Current Date      result_format=%Y-%m-%d
    ${status}=   Set Variable   True
    FOR     ${i}  IN RANGE  ${length}
            ${data}=   Set Variable  ${operacoes[${i}]['dateTime']}
            ${data}   ${rest}=   Split String    ${data}    T
            Log     ${data}
 
            ${valor}=   Set Variable  ${operacoes[${i}]['item']['value']}
            ${descricao}=   Set Variable  ${operacoes[${i}]['item']['description']}
            Log     ${valor}
            ${dataOk}=    Evaluate     '${data}'=='${data_atual}'
            ${valorOk}=    Evaluate     '${valor}'=='-84883'
            ${descricaoOk}=    Evaluate     '${descricao}'=='PAGAMENTO DE BOLETO'
            Run Keyword If     ${dataOk}==True and ${valorOk}==True and ${descricaoOk}==True  Exit For Loop
            ...         ELSE   ${status}=False
    END

    Should Be True    ${status}    


