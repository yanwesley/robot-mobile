*** Keyword ***
Subconta_ct15
    
    Subconta_ct12

    ${AccountId}   RetornaNumeroConta  ${cpf}

    ${token}  GeraToken  ${cpf}
    Log       ${token} 
    ${HEADERS}      Create Dictionary       accept=application/json   Content-Type=application/json  User-Agent=8d31a8d5-f4a5-429b-943f-04a6d15bda46    Authorization=${token}  
    
    Create Session    session    https://api.qa.bancobari.com.br/account/v1/accounts
    ${RESPOSTA}     Get Request    session   ${AccountId}/financialgoals    headers=${HEADERS}  
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    Log     ${RESPOSTA.json()}
    ${json_value}  Set Variable  ${RESPOSTA.json()} 
    ${sms_config}  Set Variable     ${json_value[0]['smsConfig']}
    Dictionary Should Contain Item   ${sms_config}      active    True

    LimpaDadosSubconta

    subconta_ct14

    Create Session    session    https://api.qa.bancobari.com.br/account/v1/accounts
    ${RESPOSTA}     Get Request    session   ${AccountId}/financialgoals    headers=${HEADERS}  
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200
    Log     ${RESPOSTA.json()}
    ${json_value}  Set Variable  ${RESPOSTA.json()} 
    ${sms_config}  Set Variable     ${json_value[0]['smsConfig']}
    Dictionary Should Contain Item   ${sms_config}      active    False