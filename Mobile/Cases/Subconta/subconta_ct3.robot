*** Keyword ***
Subconta_ct3
    #--------------------------------------------------------------------------------------
    # Cria uma subconta do tipo objetivo 
    CriaSubconta      Novo objetivo        ${pageSubconta.btnObjetivoMeta}
    #--------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------
    # Definir meta para uma subconta meta
    # --------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                      ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible      ${pageMeta.btnAdicionarMeta}     30
    Click Element                      ${pageMeta.btnAdicionarMeta}
    Wait Until Element Is Visible      ${pageMeta.lblPerguntaMeta}      30
    Element Text Should Be             ${pageMeta.lblPerguntaMeta}  	Defina um valor para seu objetivo
    Input Text                         ${pageMeta.valor}                20000
    Click Element                      ${pageMeta.btnAdicionarPrazo}
    Wait Until Element Is Visible             ${pageMetaConfig.lblAno}              30
    Click Element                             ${pageMetaConfig.lblAno}
    Wait Until Element Is Visible             ${pageMetaConfig.lblSelecionaAno}     30
    Click Element                             ${pageMetaConfig.lblSelecionaAno}
    Wait Until Element Is Visible      ${pageMeta.lblData}              30
    Click Element                      ${pageMeta.lblData}
    Click Element                      ${pageMeta.btnConfirmaPrazo}
    Wait Until Element Is Visible      ${pageMeta.btnDefinir}           30
    Click Element                      ${pageMeta.btnDefinir}
    # Verifica msg de confirmação verde
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}  
    Wait Until Element Is Visible             ${pageHome.lblMsgAvisoVerde}    30
    Capture Page Screenshot
    # Conferir se o valor da meta foi setado 
    Wait Until Element Is Visible      ${pageMeta.lblPerguntaMeta}      30
    ${Saldo}                           GetValor                         ${pageMeta.lblPerguntaMeta}
    Log                                ${Saldo}
    Should Be Equal                    ${Saldo}                         20000
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}        30
    Click Element                      ${pageSubconta.btnVoltar}


    #--------------------------------------------------------------------------------------
    # Ao não ter nenhum valor depósitado na subconta não deverá aparecer o botão de resgate
    #--------------------------------------------------------------------------------------
    # FOR    ${cont}    IN RANGE    0    20
    #     Swipe    15    600    15    200
    #     ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
    #     Run Keyword If    ${elemento}     Exit For Loop
    # END
    # Click Element                      ${pageSubconta.btnMeta} 
    # Wait Until Element Is Visible      ${pageMeta.btnDepositar}             30
    # Page Should Not Contain Element    ${pageMeta.btnResgatar}
    # Capture Page Screenshot
    # Wait Until Element Is Visible      ${pageSubconta.btnVoltar}            30
    # Click Element                      ${pageSubconta.btnVoltar}  


    #--------------------------------------------------------------------------------------
    # Depositar em uma subconta meta pelo botão 
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible      ${pageHome.btnMostrarSaldo}     30
    Click Element                      ${pageHome.btnMostrarSaldo}
    # Verifica o saldo inicial
    ${saldoContaInicial}               Get Text               ${pageHome.lblSaldo}
    ${saldoContaInicialNumber}=        ConvertReaisToNumber   ${saldoContaInicial}  
    Click Element                      ${pageHome.btnOcultarSaldo} 
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                      ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible      ${pageMeta.btnMostrarSaldo}          30
    Click Element                      ${pageMeta.btnMostrarSaldo} 
    Capture Page Screenshot
    Click Element                      ${pageMeta.btnOcultarSaldo} 
    Wait Until Element Is Visible      ${pageMeta.btnDepositar}             30
    Click Element                      ${pageMeta.btnDepositar} 
    # Wait Until Element Is Visible      ${pageMeta.lblPergunta}              30
    # Element Text Should Be             ${pageMeta.lblPergunta}   	        Qual valor você quer depositar em seu objetivo?
    Wait Until Element Is Visible      ${pageMeta.valorDeposito}             30
    Input Text                         ${pageMeta.valorDeposito}            20000
    Click Element                      ${pageMeta.btnDepositar} 

    #Verifica msg de confirmação verde
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}  
    Wait Until Element Is Visible             ${pageHome.lblMsgAvisoVerde}    30
    Capture Page Screenshot

    # Confere se o deposito aparece nas ultimas movimentações
    Wait Until Element Is Visible      ${pageMeta.lblUltimasMovimentacoes}  30
    Wait Until Element Is Visible      ${pageMeta.lblUltimoDeposito}        30
    Capture Page Screenshot
    Element Text Should Be             ${pageMeta.lblUltimoDeposito}  	    R$ 200,00
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}            30
    Click Element                      ${pageSubconta.btnVoltar}
    sleep  3s
    # Confere se o saldo total reduziu
    Wait Until Element Is Visible      ${pageHome.btnMostrarSaldo}     30
    Click Element                      ${pageHome.btnMostrarSaldo}
    Wait Until Element Is Visible      ${pageHome.lblSaldo}   30
    ${saldoContaFinal}                 Get Text               ${pageHome.lblSaldo}
    ${saldoContaFinalNumber}=          ConvertReaisToNumber   ${saldoContaFinal}   
    ${diferenca}=                      Evaluate               ${saldoContaInicialNumber} - ${saldoContaFinalNumber}
    Log                                ${diferenca}
    Should Be Equal                    ${diferenca}           ${200.0}
    Click Element                      ${pageHome.btnOcultarSaldo}

    #--------------------------------------------------------------------------------------
    #Verificar se ao tentar resgatar em uma subconta meta pelo botão aparece mensagem motivacional 
    #--------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                      ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible      ${pageMeta.btnResgatar}              30
    Click Element                      ${pageMeta.btnResgatar} 
    Wait Until Element Is Visible      ${pageMeta.lblMsgResgatar}           30
    Element Text Should Be             ${pageMeta.lblMsgResgatar}  	        Para que nossos objetivos se tornem realidade, devemos ser fiéis a eles ;) 
    Element Text Should Be             ${pageMeta.lblMsgObjetivo}           Deseja realmente retirar o dinheiro do seu objetivo?
    Capture Page Screenshot
    Click Element                      ${pageBiometria.btnAgoraNao} 
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}            30
    Click Element                      ${pageSubconta.btnVoltar}    


    #--------------------------------------------------------------------------------------
    # Resgatar em uma subconta meta pelo botão
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible      ${pageHome.btnMostrarSaldo}     30
    Click Element                      ${pageHome.btnMostrarSaldo}
    # Verifica o saldo inicial
    ${saldoContaInicial}               Get Text               ${pageHome.lblSaldo}
    ${saldoContaInicialNumber}=        ConvertReaisToNumber   ${saldoContaInicial}  
    Click Element                      ${pageHome.btnOcultarSaldo}         
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                      ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible      ${pageMeta.btnMostrarSaldo}          30
    Click Element                      ${pageMeta.btnMostrarSaldo} 
    Capture Page Screenshot
    Wait Until Element Is Visible      ${pageMeta.lblSaldo}          30
    ${saldoInicial}                    Get Text                             ${pageMeta.lblSaldo}
    Log                                ${saldoInicial}
    Click Element                      ${pageMeta.btnOcultarSaldo}
    Wait Until Element Is Visible      ${pageMeta.btnResgatar}              30
    Click Element                      ${pageMeta.btnResgatar} 
    Capture Page Screenshot
    Wait Until Element Is Visible      ${pageMeta.lblMsgResgatar}           30
    Element Text Should Be             ${pageMeta.lblMsgResgatar}  	        Para que nossos objetivos se tornem realidade, devemos ser fiéis a eles ;) 
    Element Text Should Be             ${pageMeta.lblMsgObjetivo}           Deseja realmente retirar o dinheiro do seu objetivo?
    Click Element                      ${pageMeta.btnConfirma}
    Wait Until Element Is Visible      ${pageMeta.lblDescricaoResgate}      30
    Element Text Should Be             ${pageMeta.lblDescricaoResgate}  	Para que você não perca dinheiro, nós facilitamos a compra e a venda dos seus papéis.\n\n Por isso quando você faz um saque e um depósito no mesmo dia, nós mostramos apenas a diferença dessa transação no seu extrato.
    Click Element                      ${pageMeta.btncontinue} 
    Capture Page Screenshot
    Wait Until Element Is Visible      ${pageMeta.lblPergunta}              30
    Element Text Should Be             ${pageMeta.lblPergunta}  	        Qual valor você quer enviar para sua conta?
    Input Text                         ${pageMeta.valorDeposito}            1000
    Click Element                      ${pageMeta.btnResgatar}
    # Verifica msg de confirmação verde
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}  
    Wait Until Element Is Visible             ${pageHome.lblMsgAvisoVerde}    30
    Capture Page Screenshot
    Wait Until Element Is Visible      ${pageMeta.btnDepositar}             30
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}            30
    Click Element                      ${pageSubconta.btnVoltar}
    Wait Until Element Is Visible      ${MainMenu.btnMenuPrincipal}         30
    

    #--------------------------------------------------------------------------------------
    # Realizar o resgate, ao abrir a tela novamente de resgate deve atualizar o saldo da conta
    #--------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                      ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible      ${pageMeta.btnMostrarSaldo}          30
    Click Element                      ${pageMeta.btnMostrarSaldo} 
    Capture Page Screenshot
    ${saldoFinal}                      Get Text               ${pageMeta.lblSaldo}
    Log                                ${saldoFinal}
    Click Element                      ${pageMeta.btnOcultarSaldo}
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}            30
    Click Element                      ${pageSubconta.btnVoltar}
    #Confere se o valor foi descontado na subconta
    ${saldoInicialNumber}              ConvertReaisToNumber   ${saldoInicial}
    ${saldoFinalNumber}                ConvertReaisToNumber   ${saldoFinal}
    ${diferenca}=                      Evaluate               ${saldoInicialNumber} - ${saldoFinalNumber}
    Log                                ${diferenca}
    Should Be Equal                    ${diferenca}           ${10.0}
    # Confere se o valor foi adicionada ao saldo total
    Wait Until Element Is Visible      ${pageHome.btnMostrarSaldo}     30
    Click Element                      ${pageHome.btnMostrarSaldo}
    Wait Until Element Is Visible      ${pageHome.lblSaldo}   30
    ${saldoContaFinal}                 Get Text               ${pageHome.lblSaldo}
    ${saldoContaFinalNumber}=          ConvertReaisToNumber   ${saldoContaFinal}   
    ${diferenca}=                      Evaluate               ${saldoContaFinalNumber} - ${saldoContaInicialNumber}
    Log                                ${diferenca}
    Should Be Equal                    ${diferenca}           ${10.0}
    Click Element                      ${pageHome.btnOcultarSaldo}


    #--------------------------------------------------------------------------------------
    # Na mensagem de resgatar na meta ao clicar "não mostrar", a mensagem não deve aparecer em nenhuma subconta (cache) 
    #--------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                      ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible      ${pageMeta.btnResgatar}              30
    Click Element                      ${pageMeta.btnResgatar} 
    Wait Until Element Is Visible      ${pageMeta.lblNaoMostrarMsg}         30
    Element Text Should Be             ${pageMeta.lblNaoMostrarMsg}  	        Não mostrar novamente
    Capture Page Screenshot
    Click Element                      ${pageMeta.chkNaoMostrarMsg}   
    Click Element                      ${pageBiometria.btnAgoraNao} 
    #Confere se o elemento não está mais sendo exibido
    Wait Until Element Is Visible      ${pageMeta.btnResgatar}              30
    Click Element                      ${pageMeta.btnResgatar} 
    Page Should Not Contain Element    ${pageMeta.lblMsgResgatar}
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}            30
    Click Element                      ${pageSubconta.btnVoltar}  
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}            30
    Click Element                      ${pageSubconta.btnVoltar}  	


    #--------------------------------------------------------------------------------------
    # Excluir subconta meta com depositos
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible      ${pageHome.btnMostrarSaldo}     30
    Click Element                      ${pageHome.btnMostrarSaldo}
    # Verifica o saldo inicial
    ${saldoContaInicial}               Get Text               ${pageHome.lblSaldo}
    ${saldoContaInicialNumber}=        ConvertReaisToNumber   ${saldoContaInicial} 
    Click Element                      ${pageHome.btnOcultarSaldo}
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                     ${pageSubconta.btnMeta} 

    Wait Until Element Is Visible      ${pageMeta.btnMostrarSaldo}          30
    Click Element                      ${pageMeta.btnMostrarSaldo} 
    Capture Page Screenshot
    Wait Until Element Is Visible      ${pageMeta.lblSaldo}        30
    ${Meta}                            Get Text                         ${pageMeta.lblSaldo}
    ${MetaNum}                         ConvertReaisToNumber             ${Meta}
    Click Element                      ${pageMeta.btnOcultarSaldo}
    Wait Until Element Is Visible      ${pageMeta.btnConfigMeta}        30
    Click Element                      ${pageMeta.btnConfigMeta} 
    Wait Until Element Is Visible      ${pageMeta.btnExcluir}           30
    Click Element                      ${pageMeta.btnExcluir}
    #Conferir a mensagem
    Wait Until Element Is Visible      ${pageMeta.btnConfirmaExclusao}  30
    Click Element                      ${pageMeta.btnConfirmaExclusao}
    Capture Page Screenshot
    #Conferir se ele resgatou o valor do item e colocou no saldo total
    Wait Until Element Is Visible      ${pageHome.btnMostrarSaldo}     30
    Click Element                      ${pageHome.btnMostrarSaldo}
    Wait Until Element Is Visible      ${pageHome.lblSaldo}  30
    ${saldoContaFinal}                 Get Text              ${pageHome.lblSaldo}
    ${saldoContaFinalNumber}=          ConvertReaisToNumber  ${saldoContaFinal}   
    ${diferenca}=                      Evaluate              ${saldoContaFinalNumber} - ${saldoContaInicialNumber}
    Log                                ${diferenca}
    Should Be Equal                    ${diferenca}          ${MetaNum}
    Click Element                      ${pageHome.btnOcultarSaldo}
