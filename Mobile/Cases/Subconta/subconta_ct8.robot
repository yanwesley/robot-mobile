*** Keyword ***
Subconta_ct8

    # --------------------------------------------------------------------------------------
    # Criar subconta cartão
    # --------------------------------------------------------------------------------------
    CriaSubconta      Novo controle        ${pageSubcontaControle.btnCriarSubconta}

    # --------------------------------------------------------------------------------------
    # Alterar imagem subconta cartão
    # --------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                   ${pageSubconta.btnMeta}   
    Wait Until Element Is Visible   ${pageMeta.btnConfigMeta}       30
    Click Element                   ${pageMeta.btnConfigMeta} 
    Wait Until Element Is Visible   ${pageMeta.btnEditaImagemMeta}  30
    Click Element                   ${pageMeta.btnEditaImagemMeta} 
    Wait Until Element Is Visible   ${pageSubconta.btnImg2}         30
    Click Element                   ${pageSubconta.btnImg2}
    Wait Until Element Is Visible   ${pageLogin.btncontinue}        30
    Click Element                   ${pageLogin.btncontinue}
    Capture Page Screenshot
    Wait Until Element Is Visible   ${pageSubconta.btnVoltar}       30
    Click Element                   ${pageSubconta.btnVoltar}
    Wait Until Element Is Visible   ${pageSubconta.btnVoltar}       30
    Click Element                   ${pageSubconta.btnVoltar}
    Capture Page Screenshot


    # --------------------------------------------------------------------------------------
    # Alterar nome subconta cartão
    # --------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                   ${pageSubconta.btnMeta}   
    Wait Until Element Is Visible   ${pageMeta.btnConfigMeta}     30
    Click Element                   ${pageMeta.btnConfigMeta} 
    Capture Page Screenshot
    Wait Until Element Is Visible   ${pageMeta.btnEditaNomeMeta}  30
    Click Element                   ${pageMeta.btnEditaNomeMeta}
    Capture Page Screenshot
    Wait Until Element Is Visible   ${pageSubcontaControle.lblPergunta}       30
    Element Should Contain Text     ${pageSubcontaControle.lblPergunta}       Defina um novo nome para o seu
    Element Should Contain Text     ${pageSubcontaControle.lblPergunta}       objetivo
    Wait Until Element Is Visible   ${pageMeta.editaNomeMeta}     30
    Clear Text                      ${pageMeta.editaNomeMeta}
    Input Text                      ${pageMeta.editaNomeMeta}     Carro
    Click Element                   ${pageMeta.btnSalvar}
    Capture Page Screenshot 
    #Conferir se ele alterou....
    Wait Until Element Is Visible   ${pageMeta.lblNomeMeta}       30
    Element Text Should Be          ${pageMeta.lblNomeMeta}       Carro
    Click Element                   ${pageSubconta.btnVoltar}
    Wait Until Element Is Visible   ${pageMeta.nomeMeta}          30
    Element Text Should Be          ${pageMeta.nomeMeta}          Carro
    Click Element                   ${pageSubconta.btnVoltar}
    Capture Page Screenshot


    # --------------------------------------------------------------------------------------
    # Alterar imagem da subconta cartão, alterar nome e voltar para dashboard
    # --------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta2}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                  ${pageSubconta.btnMeta2}   
    Wait Until Element Is Visible  ${pageMeta.btnConfigMeta}                  30
    Click Element                  ${pageMeta.btnConfigMeta} 
    Wait Until Element Is Visible  ${pageMeta.btnEditaImagemMeta}             30
    Click Element                  ${pageMeta.btnEditaImagemMeta} 
    Wait Until Element Is Visible  ${pageSubconta.btnImg1}                    30
    Click Element                  ${pageSubconta.btnImg1}
    Wait Until Element Is Visible  ${pageLogin.btncontinue}                   30
    Click Element                  ${pageLogin.btncontinue}
    Capture Page Screenshot
    Wait Until Element Is Visible  ${pageMeta.btnEditaNomeMeta}               30
    Click Element                  ${pageMeta.btnEditaNomeMeta}
    Wait Until Element Is Visible   ${pageSubcontaControle.lblPergunta}       30
    Element Should Contain Text     ${pageSubcontaControle.lblPergunta}       Defina um novo nome para o seu
    Element Should Contain Text     ${pageSubcontaControle.lblPergunta}       objetivo
    Wait Until Element Is Visible  ${pageMeta.editaNomeMeta}                  30
    Clear Text                     ${pageMeta.editaNomeMeta}
    Input Text                     ${pageMeta.editaNomeMeta}                  ${meta}
    Click Element                  ${pageMeta.btnSalvar}
    Capture Page Screenshot 
    #Conferir se ele alterou....
    Wait Until Element Is Visible  ${pageMeta.lblNomeMeta}                    30
    Element Text Should Be         ${pageMeta.lblNomeMeta}                    ${meta}
    Click Element                  ${pageSubconta.btnVoltar}
    Wait Until Element Is Visible  ${pageMeta.nomeMeta}                       30
    Element Text Should Be         ${pageMeta.nomeMeta}                       ${meta}
    Click Element                  ${pageSubconta.btnVoltar}
    Capture Page Screenshot


    # --------------------------------------------------------------------------------------
    # Alterar nome da subconta cartão, alterar imagem e voltar para dashboard
    # --------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                  ${pageSubconta.btnMeta}   
    Wait Until Element Is Visible  ${pageMeta.btnConfigMeta}     30
    Click Element                  ${pageMeta.btnConfigMeta} 
    Capture Page Screenshot
    Wait Until Element Is Visible  ${pageMeta.btnEditaNomeMeta}  30
    Click Element                  ${pageMeta.btnEditaNomeMeta}
    Capture Page Screenshot
    Wait Until Element Is Visible   ${pageSubcontaControle.lblPergunta}       30
    Element Should Contain Text     ${pageSubcontaControle.lblPergunta}       Defina um novo nome para o seu
    Element Should Contain Text     ${pageSubcontaControle.lblPergunta}       objetivo
    Clear Text                     ${pageMeta.editaNomeMeta}
    Input Text                     ${pageMeta.editaNomeMeta}    Carro
    Click Element                  ${pageMeta.btnSalvar}
    Capture Page Screenshot 
    #Conferir se ele alterou....
    Wait Until Element Is Visible  ${pageMeta.lblNomeMeta}      30
    Element Text Should Be         ${pageMeta.lblNomeMeta}      Carro
    Wait Until Element Is Visible  ${pageMeta.btnEditaImagemMeta}  30
    Click Element                  ${pageMeta.btnEditaImagemMeta} 
    Wait Until Element Is Visible  ${pageSubconta.btnImg2}         30
    Click Element                  ${pageSubconta.btnImg2}
    Wait Until Element Is Visible  ${pageLogin.btncontinue}        30
    Click Element                  ${pageLogin.btncontinue}
    Capture Page Screenshot
    Wait Until Element Is Visible  ${pageSubconta.btnVoltar}       30
    Click Element                  ${pageSubconta.btnVoltar}
    Wait Until Element Is Visible  ${pageSubconta.btnVoltar}       30
    Click Element                  ${pageSubconta.btnVoltar}
    Capture Page Screenshot


    # --------------------------------------------------------------------------------------
    # Excluir subconta cartão sem depósitos
    # --------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta2}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                  ${pageSubconta.btnMeta2}   
    Wait Until Element Is Visible  ${pageMeta.btnConfigMeta}        30
    Click Element                  ${pageMeta.btnConfigMeta} 
    Wait Until Element Is Visible  ${pageMeta.btnExcluir}           30
    Click Element                  ${pageMeta.btnExcluir}
    #Conferir a mensagem
    Wait Until Element Is Visible  ${pageMeta.btnConfirmaExclusao}  30
    Click Element                  ${pageMeta.btnConfirmaExclusao}
    Capture Page Screenshot


  