*** Keyword ***
Subconta_ct4
    
    # Cria uma subconta do tipo objetivo
    CriaSubconta      Novo objetivo        ${pageSubconta.btnObjetivoMeta}

    #--------------------------------------------------------------------------------------
    # Depositar em uma subconta meta pelo método de arrastar
    #--------------------------------------------------------------------------------------
    MetodoArrastar        ${pageHome.lblMinhaConta}        ${pageSubconta.imgSubconta1}

    #--------------------------------------------------------------------------------------
    # Verificar se ao depositar em uma subconta meta pelo método de arrastar aparece mensagem verde
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible      ${pageMeta.valorDeposito}             30
    Input Text                         ${pageMeta.valorDeposito}            20000
    Wait Until Element Is Visible      ${pageMeta.btnDepositar}             30
    Click Element                      ${pageMeta.btnDepositar} 

    #Verifica msg de confirmação verde
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando} 
    Wait Until Page Does Not Contain Element  ${pageHome.lblMsgAvisoVerde}    30
    Capture Page Screenshot

    #--------------------------------------------------------------------------------------
    # Resgatar em uma subconta meta pelo método de arrastar
    #--------------------------------------------------------------------------------------
    MetodoArrastar        ${pageSubconta.imgSubconta1}       ${pageHome.lblMinhaConta}    
    #--------------------------------------------------------------------------------------
    # Verificar se ao tentar resgatar em uma subconta meta pelo método de arrastar aparece mensagem motivacional
    #--------------------------------------------------------------------------------------
    Wait Until Element Is Visible      ${pageMeta.lblMsgResgatar}           30
    Element Text Should Be             ${pageMeta.lblMsgResgatar}  	        Para que nossos objetivos se tornem realidade, devemos ser fiéis a eles ;) 
    Element Text Should Be             ${pageMeta.lblMsgObjetivo}           Deseja realmente retirar o dinheiro do seu objetivo?
    Capture Page Screenshot


    #--------------------------------------------------------------------------------------
    # Verificar se ao tentar resgatar em uma subconta meta pelo método de arrastar aparece mensagem verde
    #--------------------------------------------------------------------------------------
    Click Element                      ${pageMeta.btnConfirma}
    Wait Until Element Is Visible      ${pageMeta.lblDescricaoResgate}      30
    Element Text Should Be             ${pageMeta.lblDescricaoResgate}  	Para que você não perca dinheiro, nós facilitamos a compra e a venda dos seus papéis.\n\n Por isso quando você faz um saque e um depósito no mesmo dia, nós mostramos apenas a diferença dessa transação no seu extrato.
    Click Element                      ${pageMeta.btncontinue} 
    Capture Page Screenshot
    Wait Until Element Is Visible      ${pageMeta.lblPergunta}              30
    Element Text Should Be             ${pageMeta.lblPergunta}  	        Qual valor você quer enviar para sua conta?
    Input Text                         ${pageMeta.valorDeposito}            1000
    Click Element                      ${pageMeta.btnResgatar}
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}  
    Wait Until Page Does Not Contain Element  ${pageHome.lblMsgAvisoVerde}    30
    Capture Page Screenshot
    Wait Until Element Is Visible      ${pageHome.btnMostrarSaldo}     30


    # Exclui a subconta criada
    ExcluirSubconta       ${pageSubconta.btnMeta}

    