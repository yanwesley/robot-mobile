*** Keyword ***
Subconta_ct6
    #--------------------------------------------------------------------------------------
    # Cria uma subconta do tipo objetivo 
    CriaSubconta      Novo objetivo        ${pageSubconta.btnObjetivoMeta}
    #--------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------
    # Definir meta para uma subconta meta
    # --------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  0.5
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                             ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible             ${pageMeta.btnAdicionarMeta}          30
    Click Element                             ${pageMeta.btnAdicionarMeta}
    Wait Until Element Is Visible             ${pageMeta.lblPerguntaMeta}           30
    Element Text Should Be                    ${pageMeta.lblPerguntaMeta}  	     Defina um valor para seu objetivo
    Input Text                                ${pageMeta.valor}                     1000000
    Click Element                             ${pageMeta.btnAdicionarPrazo}
    Wait Until Element Is Visible             ${pageMetaConfig.lblAno}              30
    Click Element                             ${pageMetaConfig.lblAno}
    Wait Until Element Is Visible             ${pageMetaConfig.lblSelecionaAno}     30
    Click Element                             ${pageMetaConfig.lblSelecionaAno}
    Wait Until Element Is Visible             ${pageMeta.lblData}                   30
    Click Element                             ${pageMeta.lblData}
    Click Element                             ${pageMeta.btnConfirmaPrazo}
    Wait Until Element Is Visible             ${pageMeta.btnDefinir}                30
    Click Element                             ${pageMeta.btnDefinir}
    # Verifica msg de confirmação verde
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}  
    Wait Until Element Is Visible             ${pageHome.lblMsgAvisoVerde}   30
    Capture Page Screenshot
    # Conferir se o valor da meta foi setado 
    Wait Until Element Is Visible             ${pageMeta.lblPerguntaMeta}           30
    ${Saldo}                                  GetValor                              ${pageMeta.lblPerguntaMeta}
    Log                                       ${Saldo}
    Should Be Equal                           ${Saldo}                              1000000
    Wait Until Element Is Visible             ${pageSubconta.btnVoltar}             30
    Click Element                             ${pageSubconta.btnVoltar}


    #--------------------------------------------------------------------------------------
    # Alterar meta de uma subconta alterando apenas o prazo
    #--------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20                                           
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  0.5
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                             ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible             ${pageMeta.btnConfigMeta}             30
    Click Element                             ${pageMeta.btnConfigMeta} 
    Wait Until Element Is Visible             ${pageMetaConfig.btnPrazo}            30
    Click Element                             ${pageMetaConfig.btnPrazo}
    Capture Page Screenshot
    Wait Until Element Is Visible             ${pageMetaConfig.lblAno}              30
    Click Element                             ${pageMetaConfig.lblAno}
    Wait Until Element Is Visible             ${pageMetaConfig.lblSelecionaAno}     30
    Click Element                             ${pageMetaConfig.lblSelecionaAno}
    Capture Page Screenshot
    Wait Until Element Is Visible             ${pageMetaConfig.lblNovoPrazo}        30
    Click Element                             ${pageMetaConfig.lblNovoPrazo}
    Click Element                             ${pageMeta.btnConfirmaPrazo}
    #--------------------------------------------------------------------------------------
    # Verificar se ao alterar o prazo da meta em uma subconta meta está aparecendo mensagem verde
    #--------------------------------------------------------------------------------------
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}  
    Wait Until Element Is Visible             ${pageHome.lblMsgAvisoVerde}           30
    Capture Page Screenshot

    # Verifica se o prazo foi alterado 
    Wait Until Element Is Visible             ${pageMetaConfig.btnPrazo}             30
    ${Valor}                                  Get Text                               ${pageMetaConfig.btnPrazo}
    Should Be Equal                           ${Valor}                               16/07/2025
    Wait Until Element Is Visible             ${pageSubconta.btnVoltar}              30
    Click Element                             ${pageSubconta.btnVoltar}
    Wait Until Element Is Visible             ${pageSubconta.btnVoltar}              30
    Click Element                             ${pageSubconta.btnVoltar}


    #--------------------------------------------------------------------------------------
    # Alterar meta de uma subconta alterando primeiro a meta e depois o prazo
    #--------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                             ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible             ${pageMeta.btnConfigMeta}        30
    Click Element                             ${pageMeta.btnConfigMeta} 
    Wait Until Element Is Visible             ${pageMetaConfig.btnValor}       30
    Click Element                             ${pageMetaConfig.btnValor}
    Wait Until Element Is Visible             ${pageMeta.lblPergunta}          30
    Element Text Should Be                    ${pageMeta.lblPergunta}  	       Defina um novo valor para seu objetivo
    Input Text                                ${pageMetaConfig.novoValor}      1500000
    Click Element                             ${pageMetaConfig.btnSalvar}

    #Verifica msg de confirmação verde
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}  
    Wait Until Element Is Visible             ${pageHome.lblMsgAvisoVerde}    30
    Capture Page Screenshot

    Wait Until Element Is Visible             ${pageMetaConfig.btnPrazo}            30
    Click Element                             ${pageMetaConfig.btnPrazo}
    Wait Until Element Is Visible             ${pageMetaConfig.lblAno}              30
    Click Element                             ${pageMetaConfig.lblAno}
    Wait Until Element Is Visible             ${pageMetaConfig.lblSelecionaAno}     30
    Click Element                             ${pageMetaConfig.lblSelecionaAno}
    Wait Until Element Is Visible             ${pageMeta.lblData}                   30
    Click Element                             ${pageMeta.lblData}
    Click Element                             ${pageMeta.btnConfirmaPrazo}

    #Verifica msg de confirmação verde
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}  
    Wait Until Element Is Visible             ${pageHome.lblMsgAvisoVerde}           30
    Capture Page Screenshot

    # Verifica se o prazo foi alterado 
    Wait Until Element Is Visible             ${pageMetaConfig.btnPrazo}             30
    ${Valor}                                  Get Text                               ${pageMetaConfig.btnPrazo}
    Should Be Equal                           ${Valor}                               05/07/2025

    Wait Until Element Is Visible             ${pageSubconta.btnVoltar}        30
    Click Element                             ${pageSubconta.btnVoltar}
    # Conferir se o valor da meta foi setado
    Wait Until Element Is Visible             ${pageMeta.lblPerguntaMeta}      30
    ${Valor}                                  GetValor                         ${pageMeta.lblPerguntaMeta}
    Log                                       ${Valor}
    Should Be Equal                           ${Valor}                         1500000                          
    Wait Until Element Is Visible             ${pageSubconta.btnVoltar}        30
    Click Element                             ${pageSubconta.btnVoltar}


    #--------------------------------------------------------------------------------------
    # Alterar meta de uma subconta alterando primeiro o prazo e depois a meta
    #--------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                             ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible             ${pageMeta.btnConfigMeta}              30
    Click Element                             ${pageMeta.btnConfigMeta} 
    Wait Until Element Is Visible             ${pageMetaConfig.btnPrazo}             30
    Click Element                             ${pageMetaConfig.btnPrazo}
    Wait Until Element Is Visible             ${pageMetaConfig.lblAno}              30
    Click Element                             ${pageMetaConfig.lblAno}
    Wait Until Element Is Visible             ${pageMetaConfig.lblSelecionaAno}     30
    Click Element                             ${pageMetaConfig.lblSelecionaAno}
    Wait Until Element Is Visible             ${pageMetaConfig.lblNovoPrazo}         30
    Click Element                             ${pageMetaConfig.lblNovoPrazo}
    Click Element                             ${pageMeta.btnConfirmaPrazo}

    #Verifica msg de confirmação verde
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}  
    Wait Until Element Is Visible             ${pageHome.lblMsgAvisoVerde}           30
    Capture Page Screenshot

    # Verifica se o prazo foi alterado 
    Wait Until Element Is Visible             ${pageMetaConfig.btnPrazo}             30
    ${Valor}                                  Get Text                               ${pageMetaConfig.btnPrazo}
    Should Be Equal                           ${Valor}                               16/07/2025

    Wait Until Element Is Visible             ${pageMetaConfig.btnValor}             30
    Click Element                             ${pageMetaConfig.btnValor}
    Wait Until Element Is Visible             ${pageMeta.lblPergunta}                30
    Element Text Should Be                    ${pageMeta.lblPergunta}  	             Defina um novo valor para seu objetivo
    Input Text                                ${pageMetaConfig.novoValor}            2000000
    Click Element                             ${pageMetaConfig.btnSalvar}

    #Verifica msg de confirmação verde
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}  
    Wait Until Element Is Visible             ${pageHome.lblMsgAvisoVerde}    30
    Capture Page Screenshot

    Wait Until Element Is Visible             ${pageSubconta.btnVoltar}        30
    Click Element                             ${pageSubconta.btnVoltar}
    # Conferir se o valor da meta foi setado
    Wait Until Element Is Visible             ${pageMeta.lblPerguntaMeta}      30
    ${Valor}                                  GetValor                         ${pageMeta.lblPerguntaMeta}
    Log                                       ${Valor}
    Should Be Equal                           ${Valor}                         2000000                          
    Wait Until Element Is Visible             ${pageSubconta.btnVoltar}        30
    Click Element                             ${pageSubconta.btnVoltar}

    
    #--------------------------------------------------------------------------------------
    # Ao abrir tela para alterar prazo da meta deverá trazer a data já salva (não a do dia de hoje)
    #--------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                             ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible             ${pageMeta.btnConfigMeta}              30
    Click Element                             ${pageMeta.btnConfigMeta} 
    Wait Until Element Is Visible             ${pageMetaConfig.btnPrazo}             30
    Click Element                             ${pageMetaConfig.btnPrazo}
    ${Valor}                                  Get Text                               ${pageMetaConfig.lblDataCalendario}
    Should Be Equal                           ${Valor}                               Wed, Jul 16
    Click Element                             ${pageMetaConfig.btnCancelCalendario}
    Wait Until Element Is Visible             ${pageSubconta.btnVoltar}        30
    Click Element                             ${pageSubconta.btnVoltar}
    Wait Until Element Is Visible             ${pageSubconta.btnVoltar}        30
    Click Element                             ${pageSubconta.btnVoltar}