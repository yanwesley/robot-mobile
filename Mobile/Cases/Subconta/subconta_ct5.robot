*** Keyword ***
Subconta_ct5
    # --------------------------------------------------------------------------------------
    # Cria uma subconta do tipo objetivo 
    # --------------------------------------------------------------------------------------
    CriaSubconta      Novo objetivo        ${pageSubconta.btnObjetivoMeta}

    # --------------------------------------------------------------------------------------
    # Definir meta para uma subconta meta
    # --------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                      ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible      ${pageMeta.btnAdicionarMeta}     30
    Click Element                      ${pageMeta.btnAdicionarMeta}
    Wait Until Element Is Visible      ${pageMeta.lblPerguntaMeta}      30
    Element Text Should Be             ${pageMeta.lblPerguntaMeta}  	Defina um valor para seu objetivo
    Input Text                         ${pageMeta.valor}                1000000
    Click Element                      ${pageMeta.btnAdicionarPrazo}
    Wait Until Element Is Visible             ${pageMetaConfig.lblAno}              30
    Click Element                             ${pageMetaConfig.lblAno}
    Wait Until Element Is Visible             ${pageMetaConfig.lblSelecionaAno}     30
    Click Element                             ${pageMetaConfig.lblSelecionaAno}
    Wait Until Element Is Visible      ${pageMeta.lblData}              30
    Click Element                      ${pageMeta.lblData}
    Click Element                      ${pageMeta.btnConfirmaPrazo}
    Wait Until Element Is Visible      ${pageMeta.btnDefinir}           30
    Click Element                      ${pageMeta.btnDefinir}
    # Verifica msg de confirmação verde
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}  
    Wait Until Element Is Visible             ${pageHome.lblMsgAvisoVerde}    30
    Capture Page Screenshot
    # Conferir se o valor da meta foi setado 
    Wait Until Element Is Visible      ${pageMeta.lblPerguntaMeta}      30
    ${Saldo}                           GetValor                         ${pageMeta.lblPerguntaMeta}
    Log                                ${Saldo}
    Should Be Equal                    ${Saldo}                         1000000
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}        30
    Click Element                      ${pageSubconta.btnVoltar}


    #--------------------------------------------------------------------------------------
    # Alterar meta de uma subconta alterando a meta para um valor maior
    #--------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                      ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible      ${pageMeta.btnConfigMeta}        30
    Click Element                      ${pageMeta.btnConfigMeta} 
    Wait Until Element Is Visible      ${pageMetaConfig.btnValor}       30
    Click Element                      ${pageMetaConfig.btnValor}
    Wait Until Element Is Visible      ${pageMeta.lblPergunta}          30
    Element Text Should Be             ${pageMeta.lblPergunta}  	    Defina um novo valor para seu objetivo
    Input Text                         ${pageMetaConfig.novoValor}      1500000
    Click Element                      ${pageMetaConfig.btnSalvar}

    #Verifica msg de confirmação verde
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}  
    Wait Until Element Is Visible             ${pageHome.lblMsgAvisoVerde}    30
    Capture Page Screenshot

    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}        30
    Click Element                      ${pageSubconta.btnVoltar}
    # Conferir se o valor da meta foi setado
    Wait Until Element Is Visible      ${pageMeta.lblPerguntaMeta}      30
    ${Valor}                           GetValor                         ${pageMeta.lblPerguntaMeta}
    Log                                ${Valor}
    Should Be Equal                    ${Valor}                         1500000                          
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}        30
    Click Element                      ${pageSubconta.btnVoltar}


    # --------------------------------------------------------------------------------------
    # Alterar meta de uma subconta para um valor menor e verificar mensagem motivacional
    # --------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                      ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible      ${pageMeta.btnConfigMeta}           30
    Click Element                      ${pageMeta.btnConfigMeta} 
    Wait Until Element Is Visible      ${pageMetaConfig.btnValor}          30
    Click Element                      ${pageMetaConfig.btnValor}
    Wait Until Element Is Visible      ${pageMeta.lblPergunta}             30
    Element Text Should Be             ${pageMeta.lblPergunta}  	       Defina um novo valor para seu objetivo
    Input Text                         ${pageMetaConfig.novoValor}         500000
    Capture Page Screenshot
    Click Element                      ${pageMetaConfig.btnSalvar}
    Capture Page Screenshot
    Sleep                              5s
    Capture Page Screenshot
    # Conferir msg motivacional
    Wait Until Element Is Visible      ${pageMetaConfig.lblMsgMetaMenor}   30
    Element Text Should Be             ${pageMetaConfig.lblMsgMetaMenor}   Você definiu um valor abaixo do inicial. Lembre-se que, por trás de um planejamento pode existir um grande propósito :) 
    Click Element                      ${pageMeta.btnConfirma}
    #Verifica msg de confirmação verde
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}  
    Wait Until Element Is Visible             ${pageHome.lblMsgAvisoVerde}    30
    Capture Page Screenshot
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}           30
    Click Element                      ${pageSubconta.btnVoltar}
    # Conferir se o valor da meta foi setado
    Wait Until Element Is Visible      ${pageMeta.lblPerguntaMeta}         30
    ${Valor}                           GetValor                            ${pageMeta.lblPerguntaMeta}
    Log                                ${Valor}
    Should Be Equal                    ${Valor}                            500000 
    Capture Page Screenshot
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}           30
    Click Element                      ${pageSubconta.btnVoltar}


    #--------------------------------------------------------------------------------------
    # Alterar meta de uma subconta alterando para um valor igual ao já estabelecido
    #--------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                      ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible      ${pageMeta.btnConfigMeta}        30
    Click Element                      ${pageMeta.btnConfigMeta} 
    Wait Until Element Is Visible      ${pageMetaConfig.btnValor}       30
    Click Element                      ${pageMetaConfig.btnValor}
    Wait Until Element Is Visible      ${pageMeta.lblPergunta}          30
    Element Text Should Be             ${pageMeta.lblPergunta}  	    Defina um novo valor para seu objetivo
    Input Text                         ${pageMetaConfig.novoValor}      500000
    Click Element                      ${pageMetaConfig.btnSalvar}
    Capture Page Screenshot
    
    #Verifica se a msg de confirmacao nao aparece
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}  
    Page Should Not Contain Element           ${pageHome.lblMsgAvisoVerde}    30
    Capture Page Screenshot

    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}        30
    Click Element                      ${pageSubconta.btnVoltar}
    # Conferir se o valor da meta foi setado
    Wait Until Element Is Visible      ${pageMeta.lblPerguntaMeta}      30
    ${Valor}                           GetValor                         ${pageMeta.lblPerguntaMeta}
    Log                                ${Valor}
    Should Be Equal                    ${Valor}                         500000                          
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}        30
    Click Element                      ${pageSubconta.btnVoltar}


    #--------------------------------------------------------------------------------------
    # Na mensagem de alterar meta ao clicar "não mostrar", a mensagem não deve aparecer em nenhuma subconta (cache)
    #--------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                      ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible      ${pageMeta.btnConfigMeta}           30
    Click Element                      ${pageMeta.btnConfigMeta} 
    Wait Until Element Is Visible      ${pageMetaConfig.btnValor}          30
    Click Element                      ${pageMetaConfig.btnValor}
    Wait Until Element Is Visible      ${pageMeta.lblPergunta}             30
    Element Text Should Be             ${pageMeta.lblPergunta}  	       Defina um novo valor para seu objetivo
    Input Text                         ${pageMetaConfig.novoValor}         200000
    Capture Page Screenshot
    Click Element                      ${pageMetaConfig.btnSalvar}
    Sleep                              5s
    Capture Page Screenshot
    # Conferir msg motivacional
    Wait Until Element Is Visible      ${pageMetaConfig.lblMsgMetaMenor}   30
    Element Text Should Be             ${pageMetaConfig.lblMsgMetaMenor}   Você definiu um valor abaixo do inicial. Lembre-se que, por trás de um planejamento pode existir um grande propósito :)    
    # Clica no botão não mostrar
    Click Element                      ${pageMeta.chkNaoMostrarMsg}
    Click Element                      ${pageBiometria.btnAgoraNao} 
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}           30
    Click Element                      ${pageSubconta.btnVoltar}
    Wait Until Element Is Visible      ${pageMetaConfig.btnValor}          30
    Click Element                      ${pageMetaConfig.btnValor}
    Wait Until Element Is Visible      ${pageMeta.lblPergunta}             30
    Element Text Should Be             ${pageMeta.lblPergunta}  	       Defina um novo valor para seu objetivo
    Input Text                         ${pageMetaConfig.novoValor}         100000
    Capture Page Screenshot
    Click Element                      ${pageMetaConfig.btnSalvar}
    Sleep                              5s
    Capture Page Screenshot
    # Conferir que msg motivacional não aparece
    Page Should Not Contain Element        ${pageMetaConfig.chkNaoMostrarMsg}   30
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}            30
    Click Element                      ${pageSubconta.btnVoltar}
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}            30
    Click Element                      ${pageSubconta.btnVoltar}


    #--------------------------------------------------------------------------------------
    # Ao clicar várias vezes no botão de alterar um valor menor da meta deverá aparecer apenas uma mensagem verde
    #--------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                      ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible      ${pageMeta.btnConfigMeta}           30
    Click Element                      ${pageMeta.btnConfigMeta} 
    Wait Until Element Is Visible      ${pageMetaConfig.btnValor}          30
    Click Element                      ${pageMetaConfig.btnValor}
    Wait Until Element Is Visible      ${pageMeta.lblPergunta}             30
    Element Text Should Be             ${pageMeta.lblPergunta}  	       Defina um novo valor para seu objetivo
    Input Text                         ${pageMetaConfig.novoValor}         500000
    Capture Page Screenshot
    Click Element                      ${pageMetaConfig.btnSalvar}
    Capture Page Screenshot
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando} 
    Capture Page Screenshot
    # Conferir msg motivacional
    Wait Until Element Is Visible      ${pageMetaConfig.lblMsgMetaMenor}   30
    Element Text Should Be             ${pageMetaConfig.lblMsgMetaMenor}   Você definiu um valor abaixo do inicial. Lembre-se que, por trás de um planejamento pode existir um grande propósito :) 
    Click Element                      ${pageMeta.btnConfirma}
    Click Element                      ${pageMeta.btnConfirma}
    Click Element                      ${pageMeta.btnConfirma}
    #Verifica msg de confirmação verde
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}  
    Wait Until Element Is Visible             ${pageHome.lblMsgAvisoVerde}    30
    Capture Page Screenshot
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}           30
    Click Element                      ${pageSubconta.btnVoltar}
    # Conferir se o valor da meta foi setado
    Wait Until Element Is Visible      ${pageMeta.lblPerguntaMeta}         30
    ${Valor}                           GetValor                            ${pageMeta.lblPerguntaMeta}
    Log                                ${Valor}
    Should Be Equal                    ${Valor}                            500000 
    Capture Page Screenshot
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}           30
    Click Element                      ${pageSubconta.btnVoltar}