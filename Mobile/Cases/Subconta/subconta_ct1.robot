*** Keyword ***
Subconta_ct1

    # Lembrar de alterar o token
    ${token}  GeraToken  09699387998
    Log       ${token} 
    ${HEADERS}      Create Dictionary       accept=application/json   Content-Type=application/json  User-Agent=8d31a8d5-f4a5-429b-943f-04a6d15bda46    Authorization=${token}  
    
    # #--------------------------------------------------------------------------------------
    # # Get request 
    # #-------------------------------------------------------------------------------------- 
    # Create Session    session    https://api.qa.bancobari.com.br/featureflag/v1/customerscopes  
    # Log       session 
    
    # ${RESPOSTA}     Get Request    session  ?customerId=09699387998   headers=${HEADERS}  
    # Should Be Equal As Strings      ${RESPOSTA.status_code}    200 

    #--------------------------------------------------------------------------------------
    # Testar flag de backend quando NÃO se tem subconta cartão ativa se aparece para criar subconta meta
    #--------------------------------------------------------------------------------------
    
    # Patch request 
    Close Application 

    Create Session    session    https://api.qa.bancobari.com.br/featureflag/v1/customerscopes 
    Log       session 
    ${PARAMS}=  catenate
        ...     {
        ...    "scopes": [
        ...        {
        ...        "code": "ACCOUNT_GOALS",
        ...        "isActive": false
        ...        }
        ...     ]
        ...     }
    Log      ${PARAMS}
    ${RESPOSTA}     Patch Request    session   09699387998   headers=${HEADERS}   data=${PARAMS}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200 

     
    OpenApp
    Login  09699387998  123Abc

    FOR    ${cont}    IN RANGE    0    10
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnCriarSubconta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    
    Should Be Equal  ${elemento}   ${False}


    #--------------------------------------------------------------------------------------
    # Testar flag de backend quando se tem subconta cartão ativa se aparece para criar subconta meta
    #--------------------------------------------------------------------------------------

    # Patch request 
    Close Application 

    Create Session    session    https://api.qa.bancobari.com.br/featureflag/v1/customerscopes 
    Log       session 
    ${PARAMS}=  catenate
        ...     {
        ...    "scopes": [
        ...        {
        ...        "code": "ACCOUNT_GOALS",
        ...        "isActive": true
        ...        }
        ...     ]
        ...     }
    Log      ${PARAMS}
    ${RESPOSTA}     Patch Request    session   09699387998   headers=${HEADERS}   data=${PARAMS}
    Should Be Equal As Strings      ${RESPOSTA.status_code}    200 
    
    OpenApp
    Login  09699387998  123Abc

    FOR    ${cont}    IN RANGE    0    10
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnCriarSubconta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    
    Should Be Equal  ${elemento}   ${True}