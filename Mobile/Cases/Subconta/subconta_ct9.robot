*** Keyword ***
Subconta_ct9
    # --------------------------------------------------------------------------------------
    # Criar subconta cartão
    # --------------------------------------------------------------------------------------
    CriaSubconta      Novo controle        ${pageSubcontaControle.btnCriarSubconta}
  
    #--------------------------------------------------------------------------------------
    # Ao não ter nenhum valor depósitado na subconta não deverá aparecer o botão de resgate
    #--------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                      ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible      ${pageMeta.btnDepositar}             30
    Page Should Not Contain Element    ${pageMeta.btnResgatar}
    Capture Page Screenshot


    #--------------------------------------------------------------------------------------
    # Depositar em uma subconta cartão pelo botão 
    #--------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                      ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible      ${pageMeta.btnMostrarSaldo}          30
    Click Element                      ${pageMeta.btnMostrarSaldo} 
    Capture Page Screenshot
    Click Element                      ${pageMeta.btnOcultarSaldo}
    Wait Until Element Is Visible      ${pageMeta.btnDepositar}             30
    Click Element                      ${pageMeta.btnDepositar} 
    Wait Until Element Is Visible      ${pageMeta.lblPergunta}              30
    #Element Text Should Be  ${pageMeta.lblPergunta}  	Qual valor você quer depositar em seu objetivo?
    Input Text                         ${pageMeta.valorDeposito}            20000
    Click Element                      ${pageMeta.btnDepositar} 

    # --------------------------------------------------------------------------------------
    # Verificar se ao depositar em uma subconta cartão pelo botão aparece mensagem verde
    # --------------------------------------------------------------------------------------
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}  
    Wait Until Element Is Visible             ${pageHome.lblMsgAvisoVerde}    30
    Capture Page Screenshot

    # Confere se o deposito aparece nas ultimas movimentações
    Wait Until Element Is Visible      ${pageMeta.lblUltimasMovimentacoes}  30
    Wait Until Element Is Visible      ${pageMeta.lblUltimoDeposito}        30
    Capture Page Screenshot
    Element Text Should Be             ${pageMeta.lblUltimoDeposito}  	    R$ 200,00
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}            30
    Click Element                      ${pageSubconta.btnVoltar}


    # --------------------------------------------------------------------------------------
    # Resgatar em uma subconta cartão pelo botão 
    # --------------------------------------------------------------------------------------
    Wait Until Element Is Visible      ${pageHome.btnMostrarSaldo}     30
    Click Element                      ${pageHome.btnMostrarSaldo}
    # Verifica o saldo inicial
    ${saldoContaInicial}               Get Text               ${pageHome.lblSaldo}
    ${saldoContaInicialNumber}=        ConvertReaisToNumber   ${saldoContaInicial}     
    Click Element                      ${pageHome.btnOcultarSaldo}     
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                      ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible      ${pageMeta.btnMostrarSaldo}          30
    Click Element                      ${pageMeta.btnMostrarSaldo} 
    Capture Page Screenshot
    Wait Until Element Is Visible      ${pageMeta.lblSaldo}          30
    ${saldoInicial}                    Get Text                             ${pageMeta.lblSaldo}
    Log                                ${saldoInicial}
    Click Element                      ${pageMeta.btnOcultarSaldo}
    Wait Until Element Is Visible      ${pageMeta.btnResgatar}              30
    Click Element                      ${pageMeta.btnResgatar} 
    Capture Page Screenshot
    Wait Until Element Is Visible      ${pageMeta.lblMsgResgatar}           30
    Element Text Should Be             ${pageMeta.lblMsgResgatar}  	        Ei, tem certeza que deseja retirar dinheiro do seu objetivo? 
    Element Text Should Be             ${pageMeta.lblMsgObjetivo}           Ao retirar dinheiro do objetivo o valor será enviado para sua conta, ok?
    
    Click Element                      ${pageMeta.btnConfirma}
    Wait Until Element Is Visible      ${pageMeta.lblDescricaoResgate}      30
    Element Text Should Be             ${pageMeta.lblDescricaoResgate}  	Para que você não perca dinheiro, nós facilitamos a compra e a venda dos seus papéis.\n\n Por isso quando você faz um saque e um depósito no mesmo dia, nós mostramos apenas a diferença dessa transação no seu extrato.
    Click Element                      ${pageMeta.btncontinue} 
    Capture Page Screenshot
    Wait Until Element Is Visible      ${pageMeta.lblPergunta}              30
    Element Text Should Be             ${pageMeta.lblPergunta}  	        Qual valor você quer enviar para sua conta?
    Input Text                         ${pageMeta.valorDeposito}            1000
    Click Element                      ${pageMeta.btnResgatar}
    # Verifica msg de confirmação verde
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}  
    Wait Until Element Is Visible             ${pageHome.lblMsgAvisoVerde}    30
    Capture Page Screenshot
    Wait Until Element Is Visible      ${pageMeta.btnDepositar}             30
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}            30
    Click Element                      ${pageSubconta.btnVoltar}
    Wait Until Element Is Visible      ${MainMenu.btnMenuPrincipal}         30
    
    #--------------------------------------------------------------------------------------
    # Realizar o resgate, ao abrir a tela novamente de resgate deve atualizar o saldo da conta
    #--------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                      ${pageSubconta.btnMeta} 
    Wait Until Element Is Visible      ${pageMeta.btnMostrarSaldo}          30
    Click Element                      ${pageMeta.btnMostrarSaldo} 
    Capture Page Screenshot
    ${saldoFinal}                      Get Text               ${pageMeta.lblSaldo}
    Log                                ${saldoFinal}
    Click Element                      ${pageMeta.btnOcultarSaldo}
    Wait Until Element Is Visible      ${pageSubconta.btnVoltar}            30
    Click Element                      ${pageSubconta.btnVoltar}
    #Confere se o valor foi descontado na subconta
    ${saldoInicialNumber}              ConvertReaisToNumber   ${saldoInicial}
    ${saldoFinalNumber}                ConvertReaisToNumber   ${saldoFinal}
    ${diferenca}=                      Evaluate               ${saldoInicialNumber} - ${saldoFinalNumber}
    Log                                ${diferenca}
    Should Be Equal                    ${diferenca}           ${10.0}
    # Confere se o valor foi adicionada ao saldo total
    Wait Until Element Is Visible      ${pageHome.lblSaldo}   30
    Wait Until Element Is Visible      ${pageHome.btnMostrarSaldo}     30
    Click Element                      ${pageHome.btnMostrarSaldo}
    ${saldoContaFinal}                 Get Text               ${pageHome.lblSaldo}
    ${saldoContaFinalNumber}=          ConvertReaisToNumber   ${saldoContaFinal}   
    ${diferenca}=                      Evaluate               ${saldoContaFinalNumber} - ${saldoContaInicialNumber}
    Log                                ${diferenca}
    Should Be Equal                    ${diferenca}           ${10.0}
    Click Element                      ${pageHome.btnOcultarSaldo}   


    # --------------------------------------------------------------------------------------
    # Excluir subconta cartão com depósitos
    # --------------------------------------------------------------------------------------
    Wait Until Element Is Visible      ${pageHome.btnMostrarSaldo}     30
    Click Element                      ${pageHome.btnMostrarSaldo}
    # Verifica o saldo inicial
    ${saldoContaInicial}               Get Text               ${pageHome.lblSaldo}
    ${saldoContaInicialNumber}=        ConvertReaisToNumber   ${saldoContaInicial} 
    Click Element                      ${pageHome.btnOcultarSaldo}    
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                     ${pageSubconta.btnMeta} 

    Wait Until Element Is Visible      ${pageMeta.btnMostrarSaldo}          30
    Click Element                      ${pageMeta.btnMostrarSaldo} 
    Capture Page Screenshot
    Wait Until Element Is Visible      ${pageMeta.lblSaldo}        30
    ${Meta}                            Get Text                         ${pageMeta.lblSaldo}
    ${MetaNum}                         ConvertReaisToNumber             ${Meta}
    Click Element                      ${pageMeta.btnOcultarSaldo}
    Wait Until Element Is Visible      ${pageMeta.btnConfigMeta}        30
    Click Element                      ${pageMeta.btnConfigMeta} 
    Wait Until Element Is Visible      ${pageMeta.btnExcluir}           30
    Click Element                      ${pageMeta.btnExcluir}
    #Conferir a mensagem
    Wait Until Element Is Visible      ${pageMeta.btnConfirmaExclusao}  30
    Click Element                      ${pageMeta.btnConfirmaExclusao}
    Capture Page Screenshot
    #Conferir se ele resgatou o valor do item e colocou no saldo total
    Wait Until Element Is Visible      ${pageHome.btnMostrarSaldo}          30
    Click Element                      ${pageHome.btnMostrarSaldo} 
    Wait Until Element Is Visible      ${pageHome.lblSaldo}  30
    ${saldoContaFinal}                 Get Text              ${pageHome.lblSaldo}
    ${saldoContaFinalNumber}=          ConvertReaisToNumber  ${saldoContaFinal}   
    ${diferenca}=                      Evaluate              ${saldoContaFinalNumber} - ${saldoContaInicialNumber}
    Log                                ${diferenca}
    Should Be Equal                    ${diferenca}          ${MetaNum}
    Click Element                      ${pageHome.btnOcultarSaldo} 


    
    
    
     
    
    