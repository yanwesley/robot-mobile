*** Keyword ***
Subconta_ct12
    
    # Cria uma subconta do tipo controle
    CriaSubconta      Novo controle        ${pageSubcontaControle.btnCriarSubconta}

    # --------------------------------------------------------------------------------------
    # seleciona o campo de configuração da subconta, habilita o campo de SMS e adiciona um número de telefone válido
    # --------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                   ${pageSubconta.btnMeta}   
    Wait Until Element Is Visible   ${pageMeta.btnConfigMeta}       30
    Click Element                   ${pageMeta.btnConfigMeta} 
    Wait Until Element Is Visible   ${pageSubcontaControle.btnSms}       30
    Click Element                   ${pageSubcontaControle.btnSms}       
    Wait Until Element Is Visible   ${pageSubcontaControle.lblTituloModal}       30
    Element Text Should Be          ${pageSubcontaControle.lblTituloModal}       Comprovante SMS
    Element Text Should Be          ${pageSubcontaControle.lblMsgModal}          Para habilitar o recebimento do comprovante você deve cadastrar um número de celular.
    Element Text Should Be          ${pageMeta.btnConfirma}                     Ok, cadastrar
    Click Element                   ${pageMeta.btnConfirma}    
    Wait Until Element Is Visible   ${pageMeta.lblPergunta}       30
    Element Text Should Be          ${pageMeta.lblPergunta}       Digite o número do celular
    Input Text                      ${pageSubcontaControle.numeroCelular}     41995947425
    Click Element                   ${pageSubcontaControle.btnCadastrarCelular} 
    Wait Until Element Is Visible   ${pageSubcontaControle.lblTelefoneCadastrado}       30
    Element Text Should Be          ${pageSubcontaControle.lblTelefoneCadastrado}       +55(41) 99594-7425
    Capture Page Screenshot
    Wait Until Element Is Visible  ${pageSubconta.btnVoltar}       30
    Click Element                  ${pageSubconta.btnVoltar}
    Wait Until Element Is Visible  ${pageSubconta.btnVoltar}       30
    Click Element                  ${pageSubconta.btnVoltar}

