*** Keyword ***
Subconta_ct14

    Subconta_ct12

    # --------------------------------------------------------------------------------------
    # desabilita o campo de SMS
    # --------------------------------------------------------------------------------------
    FOR    ${cont}    IN RANGE    0    20
        Swipe    15    600    15    200
        ${elemento}    Run Keyword And Return Status     Page Should Contain Element   ${pageSubconta.btnMeta}  1
        Run Keyword If    ${elemento}     Exit For Loop
    END
    Click Element                   ${pageSubconta.btnMeta}   
    Wait Until Element Is Visible   ${pageMeta.btnConfigMeta}       30
    Click Element                   ${pageMeta.btnConfigMeta} 
    Wait Until Element Is Visible   ${pageSubcontaControle.btnSms}       30
    Click Element                   ${pageSubcontaControle.btnSms} 
    Wait Until Element Is Visible   ${pageHome.lblMsgAvisoVerde}    30
    Element Text Should Be          ${pageHome.lblMsgAvisoVerde}    Comprovante desativado
    Capture Page Screenshot
    Wait Until Element Is Visible  ${pageSubconta.btnVoltar}       30
    Click Element                  ${pageSubconta.btnVoltar}
    Wait Until Element Is Visible  ${pageSubconta.btnVoltar}       30
    Click Element                  ${pageSubconta.btnVoltar}
