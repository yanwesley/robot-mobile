*** Settings ***
Resource  ${EXECDIR}/Resources/Settings.resource

*** Variables ***
@{asserts}


*** Test Cases ***
Regressao
    OpenApp
    Login  ${cpf}  ${senha}
    # #Start Screen Recording 

    #Efetua limpeza de todas subcontas 
    # LimpaDadosSubconta   ${cpf}
    #LimpaContatos  ${cpf}

    ${contents}    Get File     ${EXECDIR}/ListaTestes.txt   encoding=UTF-8
    @{Tests}=   Split To Lines  ${contents}

    FOR    ${test}   IN   @{Tests}
        ${progress1}  Run Keyword And Ignore Error  ${test}
        Append To List          ${asserts}           ${progress1[1]}
        Run Keyword If   "${progress1[0]}"=="FAIL"   Falha  ${cpf}  ${senha}
    END

    #Stop Screen Recording     filename=cenario4_subconta

    Set Test Variable       ${validacao}      ${EMPTY}
    FOR  ${assert}  IN  @{asserts}
        ${var}  Evaluate  "None"=="""${assert}"""
        Run Keyword Unless   ${var}  FalhaValidacao
    END
    Should Be Equal As Strings  ${validacao}  ${EMPTY}

*** Keyword ***
Falha
    [Arguments]  ${cpf}  ${senha}
    Close Application  
    # OpenApp
    # Login  ${cpf}  ${senha}
    #LimpaDadosSubconta

FalhaValidacao
    ${validacao}  Set Variable  Fail
    Set Test Variable       ${validacao}